$(document).ready(function(){
	
            $('.inline-cal').datetimepicker({
                inline: true,
                sideBySide: false,
				format: 'DD/MM/YYYY',
				icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-calendar-check-o',
        clear: 'fa fa-trash-o',
        close: 'fa fa-close'
    }
            });
       
	$(document).on('click','.appointment',function(){
		
		$(this).parent().next().slideToggle();
		
	})
})