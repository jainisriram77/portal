    'use strict';
angular
.module('patientportal', ['ui.router','ngCookies','ngResource','ngMessages','ngFlash','ui.bootstrap','patientportal.routes','angular-directive-select-usstates','ui.grid.cellNav','ui.grid','ui.grid.edit','ui.grid.expandable','ui.grid.selection', 'ui.grid.pinning','ui.grid.validate','patientportal.services', 'patientportal.controllers']
)
