  'use strict';
/*
 * angular .module('4ServSecurePortal.routes', ['ngRoute'])
 * .config(function($routeProvider) { $routeProvider.when('/', {templateUrl:
 * 'app/views/loginform.htm', controller: 'LoginController'});
 * $routeProvider.when('/showdashboard', {templateUrl: 'dashboard.htm',
 * controller: 'LandingPageController'}); $routeProvider.otherwise({redirectTo:
 * '/'}); });
 */

//ngRouter replacement with ui router
angular
.module('patientportal.routes', [ 'ui.router' ])
.config(
		function($stateProvider, $urlRouterProvider) {
			$urlRouterProvider.otherwise('/');
			$stateProvider
			.state('login', {

				url : '/',
				templateUrl : './views/login.view.html',
				controller : 'LoginController as login'

			})
			.state('loginhelp', {

				url : '/help',
				templateUrl : './views/loginhelp.view.html',
				controller : 'LoginHelpController as logincontrol'

			})
			.state('navigation', {
						url : '/navigation',
						views : {
							'' : {
								templateUrl : './views/navigation.view.html',
								controller : 'NavigationController as nav'
							},

							'navigation@navigation' : {
								templateUrl : './views/home.view.html',
								controller : 'HomeController as home'
							}
						}
					})
					.state(
							'home',
							{
								url : '/home',
								views : {
									'' : {
										templateUrl : './views/navigation.view.html',
										controller : 'NavigationController as nav'
									},

									'navigation@home' : {
										templateUrl : './views/home.view.html',
										controller : 'HomeController as home'
									}
								}

							})
					.state(
							'profile',
							{
								url : '/profile',
								views : {
									'' : {
										templateUrl : './views/navigation.view.html',
										controller : 'NavigationController as nav'
									},

									'navigation@profile' : {
										templateUrl : './views/profile.view.html',
										controller : 'ProfileInfoController as pc'
									}
								}

							})
							.state(
							'schedule',
							{
								url : '/schedule',
								views : {
									'' : {
										templateUrl : './views/navigation.view.html',
										controller : 'NavigationController as nav'
									},

									'navigation@schedule' : {
										templateUrl : './views/appointment.view.html',
										controller : 'AppointmentController as ac'
									}
								}

							})
							.state(
									'MyHealth',
									{
										url : '/MyHealth',
										views : {
											'' : {
												templateUrl : './views/navigation.view.html',
												controller : 'NavigationController as nav'
											},

											'navigation@MyHealth' : {
												templateUrl : './views/MyHealth.view.html',
												controller : 'MyHealthController as hc'
											}
										}

									})
									.state(
											'inbox',
											{
												url : '/inbox',
												views : {
													'' : {
														templateUrl : './views/navigation.view.html',
														controller : 'NavigationController as nav'
													},

													'navigation@inbox' : {
														templateUrl : './views/inbox.view.html',
														controller : 'InboxController as ic'
													}
												}

											})
											.state(
											'allergies',
											{
												url : '/allergies',
												views : {
													'' : {
														templateUrl : './views/navigation.view.html',
														controller : 'NavigationController as nav'
													},

													'navigation@allergies' : {
														templateUrl : './views/allergies.view.html',
														controller : 'AllergiesController as acc'
													}
												}

											})
											.state(
											'medication',
											{
												url : '/medication',
												views : {
													'' : {
														templateUrl : './views/navigation.view.html',
														controller : 'NavigationController as nav'
													},

													'navigation@medication' : {
														templateUrl : './views/medication.view.html',
														controller : 'MedicationController as mc'
													}
												}

											})
											.state(
											'document',
											{
												url : '/document',
												views : {
													'' : {
														templateUrl : './views/navigation.view.html',
														controller : 'NavigationController as nav'
													},

													'navigation@document' : {
														templateUrl : './views/documents.view.html',
														controller : 'DocumentController as dc'
													}
												}

											})
											.state(
											'results',
											{
												url : '/results',
												views : {
													'' : {
														templateUrl : './views/navigation.view.html',
														controller : 'NavigationController as nav'
													},

													'navigation@results' : {
														templateUrl : './views/results.view.html',
														controller : 'ResultsController as Rc'
													}
												}

											})
											
									
							
			}			
	);