'use strict';
angular
.module("patientportal.controllers")
.controller('AllergiesController',function($scope, $cookies, $rootScope, $state,Flash,AllergyService) {
	var vm=this;
	vm.id = $cookies.get('firstName');
	vm.allergyinfo=null;
	
	vm.id=$cookies.get('id');
	
	vm.getallergyinfo=function(){
		AllergyService.getAllergies(vm.id)
		.then(function(response){
			console.log(response);
		vm.allergyinfo=response;
		
		});
	}
	vm.getallergyinfo();

	
});