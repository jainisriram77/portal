'use strict';
angular
.module("patientportal.controllers")
.controller('ResultsController',function($scope, $cookies, $rootScope, $state,Flash,ResultService) {
	var vm=this;
	vm.id = $cookies.get('firstName');
	vm.results=null;
	
	vm.id=$cookies.get('id');
	
	vm.getresults=function(){
		ResultService.getResults(vm.id)
		.then(function(response){
			console.log(response);
		vm.results=response;
		
		});
	}
	vm.getresults();

	
});