
'use strict';
angular.module("patientportal.controllers").controller(
		'NavigationController', function($scope, $cookies, $rootScope, $state) {
			var vm = this;
			vm.firstname=$cookies.get('firstname');
			vm.lastname=$cookies.get('lastname');
			vm.navbarCollapsed = true;
			vm.userbarCollapsed = true;
			
			vm.gotohome=function(){
				$state.go("home");
			    }
		   
		    vm.gotoschedule=function(){
				$state.go("schedule");
		        }
		    vm.gotoMyHealth=function(){
				$state.go("MyHealth");
		        }
		    vm.gotodocument=function(){
				$state.go("document");
		        }
		    vm.gotoinsuranceinfo=function(){
				$state.go("insuranceinfo");
		        }
		    vm.gotomedication=function(){
				$state.go("medication");
		        }
		   vm.gotoprofile=function(){
			    $state.go("profile");
		   }
		   vm.gotoinbox=function(){
			    $state.go("inbox");
		   }
		   vm.gotoallergies=function(){
				$state.go("allergies");
			    }
		   vm.gotoresults=function(){
				$state.go("results");
			    }
			
		   
			
		});
