'use strict';
angular
.module("patientportal.controllers")
.controller('PhysiciannavigationController',function($scope, $cookies, $rootScope, $state,Flash) {
	var vm = this;
	vm.firstname=$cookies.get('firstname');
	vm.lastname=$cookies.get('lastname');
	vm.navbarCollapsed = true;
	vm.userbarCollapsed = true;
	
	
	vm.gotophysicianhome=function(){
		$state.go("physicianhome");
        }
	vm.gotoallappointments=function(){
		$state.go("allappointments");
        }
});