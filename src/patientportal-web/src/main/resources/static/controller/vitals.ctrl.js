'use strict';
angular
.module("patientportal.controllers")
.controller('VitalsController',function($scope, $cookies, $rootScope, $state,Flash,VitalService) {
	var vm=this;
	vm.id = $cookies.get('firstName');
	vm.vitalinfo=null;
	
	vm.id=$cookies.get('id');
	
	vm.getvitalinfo=function(){
		VitalService.getvitals(vm.id)
		.then(function(response){
			console.log(response);
		vm.vitalinfo=response;
		
		});
	}
	vm.getvitalinfo();

	
});