'use strict';
angular
.module("patientportal.controllers")
.controller('SignupController',function($scope,$cookies,$rootScope,$state,SignupService,Flash,$log) {
	//variables
	var vm=this;
	
	vm.id=$cookies.get('id');
	
	vm.Userinfo={
			userName:'',
			password:'',
			firstName:'',
			lastName:'',
			email:'',
			dob:'',
			userType:''
			};
	vm.createuser=function(){
		SignupService.createuser(vm.Userinfo).
	then(function(response){
		console.log(vm.Userinfo);
		Flash.create('success', 'Signup successfull', 10000, {class: 'custom-class', id: 'custom-id'}, true);
	return response;
	});
	}
	
});