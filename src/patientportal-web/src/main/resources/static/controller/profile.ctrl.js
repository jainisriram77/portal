'use strict';
angular
.module("patientportal.controllers")
.controller('ProfileInfoController',function($scope, $cookies, $rootScope, $state,Flash,ProfileService) {
	var vm=this;
	vm.id = $cookies.get('firstName');
	vm.profileinfo=null;
	vm.contactinfo=null;
	vm.id=$cookies.get('id');
	
	
	vm.getprofileinfo=function(){
		ProfileService.getProfile(vm.id)
		.then(function(response){
			console.log(response);
		vm.profileinfo=response;
		});
	}
	vm.getprofileinfo();

	
	vm.getcontactinfo=function(){
		ProfileService.getContactInfo(vm.id)
		.then(function(response){
			console.log(response);
			vm.contactinfo=response;
		});
	}
	vm.getcontactinfo();
});