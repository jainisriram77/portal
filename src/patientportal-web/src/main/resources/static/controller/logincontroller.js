
'use strict';
angular
.module("patientportal.controllers")
.controller('LoginController',function($scope,$cookies,$rootScope,$state,AuthService) {

    var vm = this;
	vm.loginDetails = [];
	vm.error=false;
	vm.errorMessage=null;
	
	vm.loginUser =  function(){
		AuthService.login(vm.loginDetails.userName, vm.loginDetails.password)
		.then(function(response){
			console.log(response);
			if(response.password){
		
				$cookies.put('firstname',response.firstName);
				$cookies.put('lastname',response.lastName);
				$cookies.put('username' , response.userName);
				$cookies.put('id',response.id);
				$state.go("navigation");
			}
		else{
				vm.error=true;
				vm.errorMessage="username or password is incorrect";
				}
		});
	};
});