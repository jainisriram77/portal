 'use strict';

angular
.module('patientportal.services')
.factory('InfoService', infoService);
function  infoService(HttpService, $log) {
	var data = {
			
			'getpatient': getpatient,
			
	};
	function getpatient(userType){
		return HttpService.get('users/'+ userType)
				.then(function(response){
					console.log(response);
					return response;
				});
	}
	return data;
}