'use strict';

angular
.module('patientportal.services')
.factory('AuthService', authService);

function authService(HttpService, $log) {
	var data = {
			'login': login,
			//'forgotPassword': forgotPassword
	};

	function login(username, password) {
		var data ={'userName': username, 'password' : password };
		return HttpService.post('api/userlogin',data)
		.then(function(response){
			console.log(response);
			return response;
		});
	}

	return data;
}