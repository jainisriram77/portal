 'use strict';

angular
.module('patientportal.services')
.factory('MedicationService', medicationService);
function  medicationService(HttpService, $log) {
	var data = {
			
			'getmedication': getmedication,
			
	};
	function getmedication(id){
		return HttpService.get('patientmedications/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}


return data;
}