 'use strict';

angular
.module('patientportal.services')
.factory('VitalService', vitalService);
function vitalService(HttpService, $log) {
	var data = {
			
			'getvitals': getvitals,
			
	};
	function getvitals(id){
		return HttpService.get('patientvitals/getbypatientid/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}


return data;
}