'use strict';

angular
.module('patientportal.services')
.factory('InsuranceService', insurance);
function insurance(HttpService, $log) {
	var data = {
			
			'getInsurance': getInsurance,
			
	};
	function getInsurance(id){
		return HttpService.get('patientinsurance/patientinsurance/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}


return data;
}