 'use strict';

angular
.module('patientportal.services')
.factory('AllergyService', allergiesService);
function allergiesService(HttpService, $log) {
	var data = {
			
			'getAllergies': getAllergies,
			
	};
	function getAllergies(id){
		return HttpService.get('patientallergies/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}


return data;
}