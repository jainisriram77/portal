'use strict';

angular
.module('patientportal.services')
.factory('PhysicianAppointmentService',physicianappointmentservice);
function physicianappointmentservice(HttpService, $log) {
	var data = {
			
			'getAllAppointments': getAllAppointments,
			'edit':edit,
			
			
	};
	function getAllAppointments(){
		return HttpService.get('appointments/')
				.then(function(response){
					console.log(response);
					return response;
				});
	}
	
	function edit(){
		return HttpService.put('appointments/appointmentupdate')
		.then(function(response){
			console.log(response);
			return response;
		}); 
	}


return data;
}