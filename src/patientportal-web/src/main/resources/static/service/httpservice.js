'use strict';

angular
.module('patientportal.services')
.factory('HttpService', httpService);

function httpService($http, SERVICE_BASE_URL, $log, Flash) {
	var data = {
	        'get': get,
	        'post': post,
	        'put':put
	        
	    };
	
	function get(url, params) {
        var requestUrl = SERVICE_BASE_URL + '/' + url;
        var i=0;
        
        angular.forEach(params, function(value, key){
        if(i==0){   
            requestUrl = requestUrl + '?' + key + '=' + value;
        }else{
            requestUrl = requestUrl + '&' + key + '=' + value;}
            i++;
        });
        return $http({
            'url': requestUrl,
            'method': 'GET',
            'headers': {
                'Content-Type': 'application/json'
            },
            'cache': true
        }).then(function(response){
            return response.data;
        }).catch(httpServiceError);
    }
	
	/*
	function post(url, data, params) {
        var requestUrl = SERVICE_BASE_URL + '/' + url;
        var i=0;
        angular.forEach(params, function(value, key){
        if(i==0){   
            requestUrl = requestUrl + '?' + key + '=' + value;
        }else{
            requestUrl = requestUrl + '&' + key + '=' + value;}
            i++;
        });
        console.log(requestUrl);
        return $http({
            'url': requestUrl,
            'method': 'POST',
            'data': data,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers' : '*'
            },
            'cache': true
        }).then(function(response){
        	console.log(response);
        	return response.data;
        }).catch(httpServiceError);
    } */
	
	function post(url, data, params, files, dataPartName) {
        var requestUrl = SERVICE_BASE_URL + '/' + url;
        var i=0;
        angular.forEach(params, function(value, key){
        if(i==0){   
            requestUrl = requestUrl + '?' + key + '=' + value;
        }else{
            requestUrl = requestUrl + '&' + key + '=' + value;}
            i++;
        });
        
        if(files){
        	 return $http({
 	            'url': requestUrl,
 	            'method': 'POST',
 	            'headers': {
 	                'Content-Type': undefined,
 	                'Access-Control-Allow-Headers' : '*'
 	            },
 	            'cache': true,
 	            'transformRequest': function(data){
	   					var formData = new FormData();
	   					
	   					if(data){
	   						formData.append(dataPartName, angular.toJson(data.model));
	   					}
	   					for (var i = 0; i < data.files.length; i++) {
	   						formData.append(data.files[i].filePartName, data.files[i].fileData);
	   					}
	   					return formData;
 	           		},
 	            'data': { 'model': data, 'files': files }
 	        }).then(function(response){
 	        	console.log(response);
 	        	return response.data;
 	        }).catch(httpServiceError);
        }else{
	        return $http({
	            'url': requestUrl,
	            'method': 'POST',
	            'data': data,
	            'headers': {
	                'Content-Type': 'application/json',
	                'Access-Control-Allow-Headers' : '*'
	            },
	            'cache': true
	        }).then(function(response){
	        	console.log(response);
	        	return response.data;
	        }).catch(httpServiceError);
        }
    }
	
	function put(url ,data){
        var requestUrl = SERVICE_BASE_URL + '/' + url;
        console.log(requestUrl);
        return $http({
            'url': requestUrl,
            'method': 'PUT',
            'data': data,
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Headers' : '*'
            },
            'cache': true
        }).then(function(response){
        	console.log(response);
        	return response.data;
        }).catch(httpServiceError);
    }
	
	return data;
	
	function httpServiceError(errorResponse) {
        $log.error('Request Failed for service...'+errorResponse);
        Flash.create('danger', 'operation failed with error: Http '+errorResponse.status, 5000, {class: 'custom-class', id: 'custom-id'}, true);
        return errorResponse;
    }
}