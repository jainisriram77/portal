'use strict';

angular
.module('patientportal.services')
.factory('DocumentService', documentService);
function  documentService(HttpService, $log) {
	var data = {
			
			'getdocuments': getdocuments,
			
	};
	function getdocuments(id){
		return HttpService.get('patientDocs/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}


return data;
}