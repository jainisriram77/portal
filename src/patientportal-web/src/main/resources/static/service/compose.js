 'use strict';

angular
.module('patientportal.services')
.factory('ComposeService', composeService);
function composeService(HttpService, $log) {
	var data = {
			
			'postMessage': postmessage,
			'sent': sent,
			
	};
	function postmessage(composeEmail){
		return HttpService.post('email/compose/',composeEmail)
				.then(function(response){
					console.log(response);
					return response;
				});
	}
	function sent(){
		return HttpService.get('email/')
				.then(function(response){
					console.log(response);
					return response;
				});
}
	

return data;
}