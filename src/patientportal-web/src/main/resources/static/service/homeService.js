'use strict';

angular
.module('patientportal.services')
.factory('HomeService', homeService);
function homeService(HttpService, $log) {
	var data = {
			
			'getUserProfile': getUserProfile,
			
	};
	function getUserProfile(id){
		return HttpService.get('users/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}


return data;
}