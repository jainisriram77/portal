'use strict';

angular
.module('patientportal.services')
.factory('ProfileService', profile);
function profile(HttpService, $log) {
	var data = {
			
			'getProfile': getProfile,
			'getContactInfo': getContactInfo,
			
	};
	function getProfile(id){
		return HttpService.get('patients/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}
	
	function getContactInfo(id){
		return HttpService.get('patientcontactinfo/'+ id)
		.then(function(response){
			console.log(response);
			return response;
		});			
		}


return data;
}