 'use strict';

angular
.module('patientportal.services')
.factory('ResultService', resultService);
function resultService(HttpService, $log) {
	var data = {
			
			'getResults': getResults,
			
	};
	function getResults(id){
		return HttpService.get('patientresult/'+ id)
				.then(function(response){
					console.log(response);
					return response;
				});
	}


return data;
}