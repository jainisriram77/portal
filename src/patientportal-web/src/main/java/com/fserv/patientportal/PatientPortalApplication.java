package com.fserv.patientportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.fserv.patientportal")
public class PatientPortalApplication{

    public static void main(String[] args) throws Exception{
        SpringApplication.run(PatientPortalApplication.class, args);
    }
}

