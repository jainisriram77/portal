CREATE SCHEMA if not exists  patientportal;

-- Table: patientportal.activity

-- DROP TABLE patientportal.activity;

CREATE TABLE patientportal.activity
(
  id integer NOT NULL,
  activity_type character varying(45),
  activity_date date,
  subject character varying(45),
  message character varying(45),
  "from" character varying(45),
  "to" character varying(45),
 
  CONSTRAINT activity_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.activity
  OWNER TO postgres;



-- Table: patientportal.document

-- DROP TABLE patientportal.document;

CREATE TABLE patientportal.document
(
  id character varying(45) NOT NULL,
  document_name character varying(45),
  document_type character varying(45),
  document_detail character varying(45),
  document_s3_url character varying(45),
  creation_date date,
  modified_date date,
  created_by character varying(45),
  source character varying(45),
  document_url character varying(45),
  CONSTRAINT document_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.document
  OWNER TO postgres;




-- Table: patientportal.health_provider

-- DROP TABLE patientportal.health_provider;

CREATE TABLE patientportal.health_provider
(
  health_provider_id integer NOT NULL,
  name character varying(45),
  CONSTRAINT health_provider_pkey PRIMARY KEY (health_provider_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.health_provider
  OWNER TO postgres;





-- Table: patientportal.physician

-- DROP TABLE patientportal.physician;

CREATE TABLE patientportal.physician
(
  physician_id integer NOT NULL,
  physician_npid integer,
  age integer,
  first_name character varying(45),
  last_name character varying(45),
  gender character varying(45),
  cell_phone_number integer,
  work_phone_number integer,
  home_phone_number integer,
  specialization character varying(45),
  email character varying(45),
  CONSTRAINT physician_pkey PRIMARY KEY (physician_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.physician
  OWNER TO postgres;




-- Table: patientportal.policy_information

-- DROP TABLE patientportal.policy_information;

CREATE TABLE patientportal.policy_information
(
  id integer NOT NULL,
  policy_number character varying(45),
  group_number character varying(45),
  policy_holder_ssn integer,
  policy_effective_date date,
  policy_termination_date date,
  policy_status character varying(45),
  co_pay integer,
  insurance_provider_name character varying(45),
  insurance_provider_address character varying(45),
  insurance_provider_zip integer,
  insurance_provider_state character varying(45),
  policy_holder_name character varying(45),
  policy_holder_relation character varying(45),
  policy_holder_dob date,
  policy_holder_address character varying(45),
  policy_holder_zip integer,
  policy_holder_state character varying(45),
  employer_name character varying(45),
  employer_address character varying(45),
  employer_zip integer,
  employer_state character varying(45),
  employer_city character varying(45),
  employer_country character varying(45),
  insurance_provider_city character varying(45),
  insurance_provider_country character varying(45),
  CONSTRAINT policy_information_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.policy_information
  OWNER TO postgres;





-- Table: patientportal.result_metric

-- DROP TABLE patientportal.result_metric;

CREATE TABLE patientportal.result_metric
(
  id integer NOT NULL,
  result_metric_code character varying(45),
  result_metric_name character varying(45),
  low_value character varying(45),
  normal_value character varying(45),
  high_value character varying(45),
  CONSTRAINT result_metric_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.result_metric
  OWNER TO postgres;






-- Table: patientportal.user

-- DROP TABLE patientportal.user;

CREATE TABLE patientportal."user"
(
id integer NOT NULL,
  user_name character varying(45),
  password character varying(45),
  first_name character varying(45),
  last_name character varying(45),
  email character varying(45),
  dob date,
  user_type character varying(45),
  CONSTRAINT "ID" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal."user"
  OWNER TO postgres;





-- Table: patientportal.user_activity

-- DROP TABLE patientportal.user_activity;

CREATE TABLE patientportal.user_activity
(
  user_id integer NOT NULL,
  activity_id integer NOT NULL,
  is_user_read character varying(45),
  CONSTRAINT user_activity_pkey PRIMARY KEY (activity_id, user_id),
  CONSTRAINT "ptUsrActivity_FK" FOREIGN KEY (activity_id)
      REFERENCES patientportal.activity (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptUsrActivity_Fk" FOREIGN KEY (user_id)
      REFERENCES patientportal."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.user_activity
  OWNER TO postgres;

-- Index: fki_user_activity_UserId_fk

-- DROP INDEX patientportal.fki_user_activity_UserId_fk;

CREATE INDEX fki_user_activity_UserId_fk
    ON patientportal.user_activity USING btree
    (USER_ID)
    TABLESPACE pg_default;






-- Table: patientportal.health_provider_location

-- DROP TABLE patientportal.health_provider_location;

CREATE TABLE patientportal.health_provider_location
(
  id integer NOT NULL,
  name character varying(45),
  code integer,
  address1 character varying(45),
  address2 character varying(45),
  city character varying(45),
  state character varying(45),
  zip character varying(45),
  email character varying(45),
  office_number integer,
  is_primary_location boolean,
  health_provider_id integer,
  CONSTRAINT health_provider_location_pkey PRIMARY KEY (id),
  CONSTRAINT fk_healthproloca_id FOREIGN KEY (health_provider_id)
      REFERENCES patientportal.health_provider (health_provider_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.health_provider_location
  OWNER TO postgres;

-- Index: fki_fk_healthproloca_id

-- DROP INDEX patientportal.fki_fk_healthproloca_id;

CREATE INDEX fki_fk_healthproloca_id
    ON patientportal.health_provider_location USING btree
    (HEALTH_PROVIDER_ID)
    TABLESPACE pg_default;





-- Table: patientportal.patient

-- DROP TABLE patientportal.patient;

CREATE TABLE patientportal.patient
(
  id integer NOT NULL,
  dob date,
  first_name character varying(45),
  last_name character varying(45),
  gender character varying(45),
  mrn character varying(45),
  user_id integer,
  phone_number integer,
  CONSTRAINT patient_pkey PRIMARY KEY (id),
  CONSTRAINT "ptuserId-Fk" FOREIGN KEY (user_id)
      REFERENCES patientportal."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient
  OWNER TO postgres;

-- Index: fki_ptuserId_Fk

-- DROP INDEX patientportal.fki_ptuserId_Fk;

CREATE INDEX fki_ptuserId_Fk
    ON patientportal.patient USING btree
    (USER_ID)
    TABLESPACE pg_default;





-- Table: patientportal.patient_allergy

-- DROP TABLE patientportal.patient_allergy;

CREATE TABLE patientportal.patient_allergy
(
  id integer NOT NULL,
  patient_id integer,
  allergy_description character varying(45),
  allergy_remarks character varying(45),
  CONSTRAINT patient_allergy_pkey PRIMARY KEY (id),
  CONSTRAINT fk_ptallergy_id FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptallergyFk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_allergy
  OWNER TO postgres;

-- Index: fki_fk_ptallergy_id

-- DROP INDEX patientportal.fki_fk_ptallergy_id;

CREATE INDEX fki_fk_ptallergy_id
    ON patientportal.patient_allergy USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;





-- Table: patientportal.patient_condition

-- DROP TABLE patientportal.patient_condition;

CREATE TABLE patientportal.patient_condition
(
  id integer NOT NULL,
  patient_id integer,
  condition_date date,
  condition_status character varying(45),
  condition_type character varying(45),
  condition_name character varying(45),
  condition_description character varying(45),
  source character varying(45),
  CONSTRAINT patient_condition_pkey PRIMARY KEY (id),
  CONSTRAINT "fk_ptCondition_id" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_condition
  OWNER TO postgres;

-- Index: fki_fk_ptCondition_id

-- DROP INDEX patientportal.fki_fk_ptCondition_id;

CREATE INDEX fki_fk_ptCondition_id
    ON patientportal.patient_condition USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;





-- Table: patientportal.patient_contact_information

-- DROP TABLE patientportal.patient_contact_information;

CREATE TABLE patientportal.patient_contact_information
(
  id integer NOT NULL,
  patient_id integer,
  contact_type character varying(45),
  address1 character varying(45),
  address2 character varying(45),
  city character varying(45),
  state character varying(45),
  country character varying(45),
  zip integer,
  phone_number integer,
  email character varying(45),
  CONSTRAINT patient_contact_information_pkey PRIMARY KEY (id),
  CONSTRAINT "fk_ptContact_id" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_contact_information
  OWNER TO postgres;

-- Index: fki_fk_ptContact_id

-- DROP INDEX patientportal.fki_fk_ptContact_id;

CREATE INDEX fki_fk_ptContact_id
    ON patientportal.patient_contact_information USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;





-- Table: patientportal.patient_emergency_contact

-- DROP TABLE patientportal.patient_emergency_contact;

CREATE TABLE patientportal.patient_emergency_contact
(
  id integer NOT NULL,
  patient_id integer,
  emergency_contact_name character varying(45),
  emergency_contact_number integer,
  emergency_email character varying(45),
  emergency_address character varying(45),
  emergency_contact_relation character varying(45),
  CONSTRAINT patient_emergency_contact_pkey PRIMARY KEY (id),
  CONSTRAINT "fk_ptEmerge_id" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_emergency_contact
  OWNER TO postgres;

-- Index: fki_fk_ptEmerge_id

-- DROP INDEX patientportal.fki_fk_ptEmerge_id;

CREATE INDEX fki_fk_ptEmerge_id
    ON patientportal.patient_emergency_contact USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;





-- Table: patientportal.patient_insurance_plan

-- DROP TABLE patientportal.patient_insurance_plan;

CREATE TABLE patientportal.patient_insurance_plan
(
  id integer NOT NULL,
  patient_id integer,
  policy_id integer,
  pt_insurance_plan_status character varying(45),
  CONSTRAINT patient_insurance_plan_pkey PRIMARY KEY (id),
  CONSTRAINT "ptInsurPlan_Fk" FOREIGN KEY (policy_id)
      REFERENCES patientportal.policy_information (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptInsuranceplan_Fk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_insurance_plan
  OWNER TO postgres;

-- Index: fki_ptInsurPlan_Fk

-- DROP INDEX patientportal.fki_ptInsurPlan_Fk;

CREATE INDEX fki_ptInsurPlan_Fk
    ON patientportal.patient_insurance_plan USING btree
    (POLICY_ID)
    TABLESPACE pg_default;

-- Index: fki_ptInsuranceplan_Fk

-- DROP INDEX patientportal.fki_ptInsuranceplan_Fk;

CREATE INDEX fki_ptInsuranceplan_Fk
    ON patientportal.patient_insurance_plan USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;






-- Table: patientportal.patient_medication

-- DROP TABLE patientportal.patient_medication;

CREATE TABLE patientportal.patient_medication
(
  id integer NOT NULL,
  patient_id integer,
  prescribed_date date,
  medication_name character varying(45),
  medication_description character varying(45),
  prescribed_physician_id integer,
  medication_start_date date,
  medication_end_date date,
  medication_type character varying(45),
  medication_status character varying(45),
  medication_directions character varying(45),
  CONSTRAINT patient_medication_pkey PRIMARY KEY (id),
  CONSTRAINT "ptMed_Fk" FOREIGN KEY (prescribed_physician_id)
      REFERENCES patientportal.physician (physician_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptMedication_Fk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_medication
  OWNER TO postgres;

-- Index: fki_ptMed_Fk

-- DROP INDEX patientportal.fki_ptMed_Fk;

CREATE INDEX fki_ptMed_Fk
    ON patientportal.patient_medication USING btree
    (PRESCRIBED_PHYSICIAN_ID)
    TABLESPACE pg_default;

-- Index: fki_ptMedication_Fk

-- DROP INDEX patientportal.fki_ptMedication_Fk;

CREATE INDEX fki_ptMedication_Fk
    ON patientportal.patient_medication USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;





-- Table: patientportal.appointment

-- DROP TABLE patientportal.appointment;

CREATE TABLE patientportal.appointment
(
  id integer NOT NULL,
  patient_id integer,
  physician_id integer,
  appointment_date date,
  appointment_time time without time zone,
  appointment_reason character varying(45),
  appointment_type character varying(45),
  health_provider_location_id integer,
  appointment_status character varying(45),
  CONSTRAINT appointment_pkey PRIMARY KEY (id),
  CONSTRAINT fk_appnt_health_id FOREIGN KEY (health_provider_location_id)
      REFERENCES patientportal.health_provider_location (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_appt_patient FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_appt_physi_id FOREIGN KEY (physician_id)
      REFERENCES patientportal.physician (physician_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.appointment
  OWNER TO postgres;

-- Index: fki_fk_appnt_health_id

-- DROP INDEX patientportal.fki_fk_appnt_health_id;

CREATE INDEX fki_fk_appnt_health_id
    ON patientportal.appointment USING btree
    (HEALTH_PROVIDER_LOCATION_ID)
    TABLESPACE pg_default;

-- Index: fki_fk_appt_patient

-- DROP INDEX patientportal.fki_fk_appt_patient;

CREATE INDEX fki_fk_appt_patient
    ON patientportal.appointment USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;

-- Index: fki_fk_appt_physi_id

-- DROP INDEX patientportal.fki_fk_appt_physi_id;

CREATE INDEX fki_fk_appt_physi_id
    ON patientportal.appointment USING btree
    (PHYSICIAN_ID)
    TABLESPACE pg_default;





-- Table: patientportal.patient_reports

-- DROP TABLE patientportal.patient_reports;

CREATE TABLE patientportal.patient_reports
(
  patient_id integer NOT NULL,
  document_id character varying(45) NOT NULL,
  report_type character varying(45),
  captured_date date,
  appointment_id integer,
  CONSTRAINT patient_reports_pkey PRIMARY KEY (document_id, patient_id),
  CONSTRAINT "ptReport_Fk" FOREIGN KEY (document_id)
      REFERENCES patientportal.document (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptReports_Fk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptReportsFk" FOREIGN KEY (appointment_id)
      REFERENCES patientportal.appointment (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_reports
  OWNER TO postgres;

-- Index: fki_ptReport_Fk

-- DROP INDEX patientportal.fki_ptReport_Fk;

CREATE INDEX fki_ptReport_Fk
    ON patientportal.patient_reports USING btree
    (DOCUMENT_ID COLLATE pg_catalog.default)
    TABLESPACE pg_default;

-- Index: fki_ptReports-Fk

-- DROP INDEX patientportal.fki_ptReports_Fk;

CREATE INDEX fki_ptReports_Fk
    ON patientportal.patient_reports USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;

-- Index: fki_ptReportsFk

-- DROP INDEX patientportal.fki_ptReportsFk;

CREATE INDEX fki_ptReportsFk
    ON patientportal.patient_reports USING btree
    (APPOINTMENT_ID)
    TABLESPACE pg_default;






-- Table: patientportal.patient_responsibile_party

-- DROP TABLE patientportal.patient_responsibile_party;

CREATE TABLE patientportal.patient_responsibile_party
(
  id integer NOT NULL,
  patient_id integer,
  responsible_party_name character varying(45),
  responsible_party_number integer,
  responsible_party_email character varying(45),
  responsible_party_address character varying(45),
  CONSTRAINT patient_responsibile_party_pkey PRIMARY KEY (id),
  CONSTRAINT "ptResponsible_Fk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_responsibile_party
  OWNER TO postgres;

-- Index: fki_ptResponsible_Fk

-- DROP INDEX patientportal.fki_ptResponsibile_Fk;

CREATE INDEX fki_ptResponsible_Fk
    ON patientportal.patient_responsibile_party USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;






-- Table: patientportal.patient_result

-- DROP TABLE patientportal.patient_result;

CREATE TABLE patientportal.patient_result
(
  id integer NOT NULL,
  patient_id integer,
  appointment_id integer,
  result_date date,
  result_metric_id integer,
  result_value character varying(45),
  result_details character varying(45),
  result_provider character varying(45),
  CONSTRAINT patient_result_pkey PRIMARY KEY (id),
  CONSTRAINT "ptResult-Fk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptResultFk" FOREIGN KEY (result_metric_id)
      REFERENCES patientportal.result_metric (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptResults-Fk" FOREIGN KEY (appointment_id)
      REFERENCES patientportal.appointment (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_result
  OWNER TO postgres;

-- Index: fki_ptResult_Fk

-- DROP INDEX patientportal.fki_ptResult_Fk;

CREATE INDEX fki_ptResult_Fk
    ON patientportal.patient_result USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;

-- Index: fki_ptResultFk

-- DROP INDEX patientportal.fki_ptResultFk;

CREATE INDEX fki_ptResultFk
    ON patientportal.patient_result USING btree
    (RESULT_METRIC_ID)
    TABLESPACE pg_default;

-- Index: fki_ptResults_Fk

-- DROP INDEX patientportal.fki_ptResults_Fk;

CREATE INDEX fki_ptResults_Fk
    ON patientportal.patient_result USING btree
    (APPOINTMENT_ID)
    TABLESPACE pg_default;






-- Table: patientportal.patient_vital

-- DROP TABLE patientportal.patient_vital;

CREATE TABLE patientportal.patient_vital
(
  id integer NOT NULL,
  patient_id integer,
  vital_name character varying(45),
  vital_value character varying(45),
  vital_date date,
  appointment_id integer,
  source character varying(45),
  CONSTRAINT patient_vital_pkey PRIMARY KEY (id),
  CONSTRAINT "ptVital_Fk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptVitals_fk" FOREIGN KEY (appointment_id)
      REFERENCES patientportal.appointment (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.patient_vital
  OWNER TO postgres;

-- Index: fki_ptVital_Fk

-- DROP INDEX patientportal.fki_ptVital_Fk;

CREATE INDEX fki_ptVital_Fk
    ON patientportal.patient_vital USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;

-- Index: fki_ptVitals_fk

-- DROP INDEX patientportal.fki_ptVitals_fk;

CREATE INDEX fki_ptVitals_fk
    ON patientportal.patient_vital USING btree
    (APPOINTMENT_ID)
    TABLESPACE pg_default;





-- Table: patientportal.user_preference

-- DROP TABLE patientportal.user_preference;

CREATE TABLE patientportal.user_preference
(
  id integer NOT NULL,
  user_id integer,
  patient_id integer,
  preference_name character varying(45),
  preference_value character varying(45),
  CONSTRAINT user_preference_pkey PRIMARY KEY (id),
  CONSTRAINT "ptUserPreference_Fk" FOREIGN KEY (patient_id)
      REFERENCES patientportal.patient (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "ptUsrPreference_Fk" FOREIGN KEY (user_id)
      REFERENCES patientportal."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE patientportal.user_preference
  OWNER TO postgres;


-- Index: fki_ptUserPreference_Fk

-- DROP INDEX patientportal.fki_ptUserPreference_Fk;

CREATE INDEX fki_ptUserPreference_Fk
    ON patientportal.user_preference USING btree
    (PATIENT_ID)
    TABLESPACE pg_default;

-- Index: fki_ptUsrPreference_Fk

-- DROP INDEX patientportal.fki_ptUsrPreference_Fk;

CREATE INDEX fki_ptUsrPreference_Fk
    ON patientportal.user_preference USING btree
    (USER_ID)
    TABLESPACE pg_default;
