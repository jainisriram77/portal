
INSERT INTO   patientportal."user"(
             id, user_name,password,first_name,last_name,email,dob,user_type)
    VALUES (1, 'raja','raja123','raja','katangur','raja@gmail.com','05-12-1980','doctor'),
           (2, 'krishna','krishna123','krishna','gullapalli','krishna@gmail.com','05-12-19887','patient'),
           (3, 'sriram','sriram123','sriram','sriram','sriram@gmail.com','05-12-1988','patient'),
           (4, 'rakesh','rakesh123','rakesh','reddy','rakesh@gmail.com','05-12-1989','doctor'),
           (5, 'immanuel','immanuel123','immanuel','govathoti','imma@gmail.com','05-12-1990','patient');

INSERT INTO   patientportal.patient(
             id, dob,first_name,last_name,gender,mrn,user_id,phone_number)
     VALUES (1,'05-12-1980','raja','katangur','male','not listed', 1,986757343),
            (2,'05-12-1989','krishna','gullapalli','male','not listed', 2,886757343),
            (3,'05-12-1988','sriram','sriram','male','not listed', 3,987573435),
            (4,'05-12-1991','rakesh','reddy','male','not listed', 4,675867543),
            (5,'05-12-1985','immanuel','govathoti','male','not listed',5,985656455);
             
INSERT INTO   patientportal.physician(
           physician_id, physician_npid,age,first_name,last_name,gender,cell_phone_number,work_phone_number,home_phone_number,specialization, email)
     VALUES (1,1,30,'raja','katangur','male',986757343,787876670,787876670,'FRCS','raja@gmail.com'),
            (2,2,31,'krishna','gullapalli','male',886757343,787876670,787876670,'MD','krishna@gmail.com'),
            (3,3,32,'sriram','sriram','male',987573435,787876670,787876670,'MD','sriram@gmail.com'),
            (4,4,33,'rakesh','reddy','male',675867543,787876670,787876670,'FRCS','rakesh@gmail.com'),
            (5,5,34,'immanuel','govathoti','male',985656455,787876670,787876670,'MD','immanuel@gmail.com');
            
INSERT INTO   patientportal.health_provider(
             health_provider_id,name)
    VALUES (1,'all state'),
           (2,'bcbi'),
           (3,'gyco'),
           (4,'New york'),
           (5,'bankers');
           
INSERT INTO   patientportal.health_provider_location(
           id,name,code,address1,address2,city,state,zip,email,office_number,is_primary_location,health_provider_id)
 VALUES   (1,'all state',101,'beckroad','wixom','detroit','mi','48393','allstate@allstate.com',875434233,true,1),
          (2,'bcbi',102,'beckroad','wixom','detroit','mi','48393','bcbi@insurence.com',875434233,false,2),
          (3,'gyco',103,'beckroad','wixom','detroit','mi','48393','gyco@insurence.com',875434233,true,3),
          (4,'newyork',104,'beckroad','wixom','detroit','mi','48393','newyork@insurence.com',875434233,false,4),
          (5,'bankers',105,'beckroad','wixom','detroit','mi','48393','bankers@insurence.com',875434233,true,5);

INSERT INTO   patientportal.appointment(
            id, patient_id, physician_id,appointment_date,appointment_time,appointment_reason,appointment_type,health_provider_location_id,appointment_status) 
    VALUES (1,1,1,'2016-05-19','12:40','bone checkup','regular',1,'done'),
           (2,2,2,'2016-05-16','08:20','heart checkup','regular',2,'done'),
           (3,3,3,'2016-05-18','09:10','lungs checkup','regular',3,'Patient arrived'),
           (4,4,4,'2016-05-19','04:50','health checkup','regular',4,'patient absent'),
           (5,5,5,'2016-05-19','05:30','bone checkup','emergency',5,'done');
           

INSERT INTO   patientportal.activity(
            id, activity_type, activity_date,subject,message,"from", "to") 
    VALUES (1,'dental checkup','2016-05-15','regular checkup','done','dentist','technician'),
           (2,'eye checkup','2016-05-16','required checkup','done','eye specialist','technician'),
           (3,'xray','2016-05-18','needed checkup','pending','technician','doctor'),
           (4,'CT scan','2016-05-19','required','done','doctor','lab technician'),
           (5,'bone checkup','2016-05-21','if required','not needed','doctor','technician');
           
   INSERT INTO   patientportal.user_activity(
            user_id,activity_id,is_user_read)
   VALUES (1,1,'yes'),
          (2,2,'yes'),
          (3,3,'yes'),
          (4,4,'yes'),
          (5,5,'yes');
             
             
  INSERT INTO   patientportal.user_preference( 
           id,user_id,patient_id,preference_name,preference_value)
    VALUES (1,1,1,'raja','100000'),
           (2,2,2,'krishna','120000'),
           (3,3,3,'sriram','130000'),
           (4,4,4,'rakesh','140000'),
           (5,5,5,'immanuel','150000');
   
   INSERT INTO   patientportal.policy_information(
           id,policy_number,group_number,policy_holder_ssn,policy_effective_date,policy_termination_date,policy_status,co_pay,insurance_provider_name,insurance_provider_address,insurance_provider_zip,insurance_provider_state,policy_holder_name, policy_holder_relation,policy_holder_dob,policy_holder_address,policy_holder_zip,policy_holder_state,employer_name,employer_address,employer_zip,employer_state,employer_city,employer_country,insurance_provider_city,insurance_provider_country)
    VALUES (1,'pa001','insurence001',92533443, '03-06-2016','04-06-2017','active',1,'allstate','wixom',48393,'MI','raja','no relation','12-12-1980','wixom',45345,'MI','4serv','detroit',54654,'MI','wixom','USA','wixom','USA'),
           (2,'pa002','insurence001',95645432, '03-06-2016','04-06-2017','active',2,'newyork','wixom',48393,'MI','krishna','no relation','12-12-1980','wixom',45345,'MI','4serv','detroit',54654,'MI','wixom','USA','wixom','USA'),
           (3,'pa003','insurence001',93434332, '03-06-2016','04-06-2017','active',3,'bcbi','wixom',48393,'MI','sriram','no relation','12-12-1980','wixom',45345,'MI','4serv','detroit',54654,'MI','wixom','USA','wixom','USA'),
           (4,'pa004','insurence001',95645433, '03-06-2016','04-06-2017','inactive',4,'health','wixom',48393,'MI','rakesh','no relation','12-12-1980','wixom',45345,'MI','4serv','detroit',54654,'MI','wixom','USA','wixom','USA'),
           (5,'pa005','insurence001',95432323, '03-06-2016','04-06-2017','active',5,'gyco','wixom',48393,'MI','immanuel','no relation','12-12-1980','wixom',45345,'MI','4serv','detroit',54654,'MI','wixom','USA','wixom','USA');
                  
   
   INSERT INTO  patientportal.patient_medication(
                 id,patient_id,prescribed_date,medication_name,medication_description,prescribed_physician_id,medication_start_date,medication_end_date,medication_type,medication_status,medication_directions)
    VALUES (1,1,'09-07-2016','raja','healthy',1,'07-07-2016','07-07-2017','general checkup','okay','mentioned in prescriton'),
           (2,2,'09-07-2016','krishna','healthy',2,'07-07-2016','07-07-2017','general checkup','okay','mentioned in prescriton'),
           (3,3,'09-07-2016','sriram','healthy',3,'07-07-2016','07-07-2017','general checkup','okay','mentioned in prescriton'),
           (4,4,'09-07-2016','rakesh','healthy',4,'07-07-2016','07-07-2017','general checkup','okay','mentioned in prescriton'),
           (5,5,'09-07-2016','immanuel','healthy',5,'07-07-2016','07-07-2017','general checkup','okay','mentioned in prescriton');
   
   INSERT INTO   patientportal.patient_condition(
          id,patient_id,condition_date,condition_status,condition_type,condition_name,condition_description,source)
    VALUES (1,1,'09-07-2016','good','regualr','general','normal','doctor'),
           (2,2,'09-07-2016','good','regualr','general','normal','doctor'),
           (3,3,'09-07-2016','good','regualr','general','normal','doctor'),
           (4,4,'09-07-2016','good','regualr','general','normal','doctor'),
           (5,5,'09-07-2016','good','regualr','general','normal','doctor');
           
   
   INSERT INTO   patientportal.patient_contact_information(
   id,patient_id,contact_type,address1,address2,city,state,country,zip,phone_number,email)
   VALUES  (1,1,'home','wixom','detroit','wixom','MI','USA',47564,575654545,'raja@gmail.com'),
          (2,2,'home','wixom','detroit','wixom','MI','USA',47564,575654545,'krishna@gmail.com'),
          (3,3,'home','wixom','detroit','wixom','MI','USA',47564,575654545,'sriram@gmail.com'),
          (4,4,'home','wixom','detroit','wixom','MI','USA',47564,575654545,'rakesh@gmail.com'),
          (5,5,'home','wixom','detroit','wixom','MI','USA',47564,575654545,'immanuel@gmail.com');
          
          
   INSERT INTO   patientportal.patient_emergency_contact(
          id,patient_id,emergency_contact_name,emergency_contact_number,emergency_email,emergency_contact_relation)
   VALUES (1,1,'rakesh',897655446,'rakesh@gmail.com','brother'),
          (2,2,'immanuel',897655446,'immanuel@gmail.com','friend'),
          (3,3,'krishna',897655446,'krishna@gmail.com','friend'),
          (4,4,'raja',897655446,'rakesh@gmail.com','brother'),
          (5,5,'sriram',897655446,'sriram@gmail.com','friend');
          
   
   INSERT INTO   patientportal.document(
          id,document_name,document_type,document_detail,document_s3_url,creation_date,modified_date,created_by,source,document_url)
  VALUES ('DOC001','','','','','06-12-2016','06-12-2016','','',''),
         ('DOC002','','','','','06-12-2016','06-12-2016','','',''),
         ('DOC003','','','','','06-12-2016','06-12-2016','','',''),
         ('DOC004','','','','','06-12-2016','06-12-2016','','',''),
         ('DOC005','','','','','06-12-2016','06-12-2016','','','');
         
   
   INSERT INTO   patientportal.patient_allergy(
         id,patient_id,allergy_description,allergy_remarks)
  VALUES (1,1,'',''),
         (2,1,'',''),
         (3,2,'',''),
         (4,2,'',''),
         (5,3,'',''),
         (6,3,'',''),
         (7,4,'',''),
         (8,4,'',''),
         (9,5,'',''),
         (10,5,'','');
         
   INSERT INTO   patientportal.patient_insurance_plan(
   id,patient_id,policy_id,pt_insurance_plan_status)
 VALUES (1,1,1,'active'),
        (2,2,2,'active'),
        (3,3,3,'active'),
        (4,4,4,'active'),
        (5,5,5,'active');
        
      
   INSERT INTO   patientportal.patient_vital(
       id,patient_id,vital_name,vital_value,vital_date,appointment_id,source)
 VALUES (1,1,'','','06-08-2016',1,''),
        (2,2,'','','06-08-2016',2,''),
        (3,3,'','','06-08-2016',3,''),
        (4,4,'','','06-08-2016',4,''),
        (5,5,'','','06-08-2016',5,'');
   
    INSERT INTO   patientportal.patient_reports(
     patient_id,document_id,report_type,captured_date,appointment_id)
VALUES (1,'DOC001','','05-05-2016',1),
       (2,'DOC002','','05-05-2016',2),
       (3,'DOC003','','05-05-2016',3),
       (4,'DOC004','','05-05-2016',4),
       (5,'DOC005','','05-05-2016',5) ;


      
   INSERT INTO   patientportal.result_metric(    
   id,result_metric_code,result_metric_name,low_value,normal_value,high_value)
VALUES (1,'','','','',''),
       (2,'','','','',''),
       (3,'','','','',''),
       (4,'','','','',''),
       (5,'','','','','');
       
   INSERT INTO   patientportal.patient_result(
    id,patient_id,appointment_id,result_date,result_metric_id,result_value,result_details,result_provider)
VALUES (1,1,1,'06-12-2016',1,'','',''),
       (2,2,2,'06-12-2016',2,'','',''),
       (3,3,3,'06-12-2016',3,'','',''),
       (4,4,4,'06-12-2016',4,'','',''),
       (5,5,5,'06-12-2016',5,'','','');
   
   
   INSERT INTO   patientportal.patient_responsibile_party(
        id,patient_id,responsible_party_name,responsible_party_number,responsible_party_email,responsible_party_address)
VALUES (1,1,'',987654334,'rakesh@gmail.com','wixom'),
       (2,2,'',987654334,'rakesh@gmail.com','wixom'),
       (3,3,'',987654334,'rakesh@gmail.com','wixom'),
       (4,4,'',987654334,'rakesh@gmail.com','wixom'),
       (5,5,'',987654334,'rakesh@gmail.com','wixom');

          
   
   
     
           
           
           
           
           
           
           
           