package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.UserDto;
import com.fserv.patientportal.service.UserService;

@Controller
@RequestMapping(value = "/users/")
public class UsersController extends BaseControllerImpl<Integer, UserDto> {

  @Autowired(required = true)
  public UsersController(UserService resourceService) {
    super(resourceService);
  }

}
