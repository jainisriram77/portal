package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the document database table.
 * 
 */
@Entity
@Table(name="document", schema = "patientportal")
@NamedQuery(name="Document.findAll", query="SELECT d FROM Document d")
public class Document implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private String id;

	@Column(name="created_by")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="creation_date")
	private Date creationDate;

	@Column(name="document_detail")
	private String documentDetail;

	@Column(name="document_name")
	private String documentName;

	@Column(name="document_s3_url")
	private String documentS3Url;

	@Column(name="document_type")
	private String documentType;

	@Column(name="document_url")
	private String documentUrl;

	@Temporal(TemporalType.DATE)
	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="source")
	private String source;

	//bi-directional many-to-one association to PatientReport
	@OneToMany(mappedBy="document")
	private List<PatientReport> patientReports;

	public Document() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDocumentDetail() {
		return this.documentDetail;
	}

	public void setDocumentDetail(String documentDetail) {
		this.documentDetail = documentDetail;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentS3Url() {
		return this.documentS3Url;
	}

	public void setDocumentS3Url(String documentS3Url) {
		this.documentS3Url = documentS3Url;
	}

	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentUrl() {
		return this.documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public List<PatientReport> getPatientReports() {
		return this.patientReports;
	}

	public void setPatientReports(List<PatientReport> patientReports) {
		this.patientReports = patientReports;
	}

	public PatientReport addPatientReport(PatientReport patientReport) {
		getPatientReports().add(patientReport);
		patientReport.setDocument(this);

		return patientReport;
	}

	public PatientReport removePatientReport(PatientReport patientReport) {
		getPatientReports().remove(patientReport);
		patientReport.setDocument(null);

		return patientReport;
	}

}