package com.fserv.patientportal.repository;

import java.util.List;

import com.fserv.patientportal.dto.PatientInsuranceSearchCriteriaDto;
import com.fserv.patientportal.model.PatientInsurancePlan;

public interface PatientInsurancePlanSearchRepository {

  public List<PatientInsurancePlan> searchPatinsrPlan(
      PatientInsuranceSearchCriteriaDto searchcriteria);
}
