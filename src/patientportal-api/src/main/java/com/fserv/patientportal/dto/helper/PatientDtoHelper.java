package com.fserv.patientportal.dto.helper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientDto;
import com.fserv.patientportal.model.Patient;

@Component
public class PatientDtoHelper implements DtoHelper<Patient, PatientDto> {

  @Override
  public List<PatientDto> buildDto(Iterable<Patient> models) {

    return null;
  }

  @Override
  public PatientDto buildDto(Patient model) {
    PatientDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new PatientDto();
      dtoToReturn.setMrn(model.getMrn());
      dtoToReturn.setGender(model.getGender());
      dtoToReturn.setFirstName(model.getFirstName());
      dtoToReturn.setLastName(model.getLastName());
      dtoToReturn.setDob(model.getDob());
      dtoToReturn.setId(model.getId());
      dtoToReturn.setUserId(model.getUserId());
      dtoToReturn.setPhoneNumber(model.getPhoneNumber());
    }
    return dtoToReturn;

  }

  @Override
  public Patient build(PatientDto dto) {
    Patient model = null;
    if (dto != null) {
      model = new Patient();
      model.setMrn(dto.getMrn());
      model.setGender(dto.getGender());
      model.setFirstName(dto.getFirstName());
      model.setLastName(dto.getLastName());
      model.setDob(dto.getDob());
      model.setId(dto.getId());
      model.setPhoneNumber(dto.getPhoneNumber());
      model.setUserId(dto.getUserId());
    }
    return model;
  }

  @Override
  public List<Patient> build(Iterable<PatientDto> dtos) {
    return null;
  }

}
