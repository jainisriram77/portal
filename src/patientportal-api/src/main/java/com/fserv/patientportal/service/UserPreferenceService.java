package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.UserPreferenceDto;

public interface UserPreferenceService extends BaseService<Integer, UserPreferenceDto> {

}
