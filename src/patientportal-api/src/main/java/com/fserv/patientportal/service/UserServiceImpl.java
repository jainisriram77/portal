package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.UserDto;
import com.fserv.patientportal.dto.helper.UserDtoHelper;
import com.fserv.patientportal.model.User;
import com.fserv.patientportal.repository.UserRepository;
import com.fserv.patientportal.service.BaseServiceImpl;
import com.fserv.patientportal.service.UserService;

@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl<User, Integer, UserDto>
    implements UserService {

  @Autowired
  public UserServiceImpl(UserRepository userRepository, UserDtoHelper userDtoHelper) {
    super(userRepository, userDtoHelper);
  }
}
