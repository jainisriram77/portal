package com.fserv.patientportal.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.AppointmentDto;
import com.fserv.patientportal.dto.AppointmentSearchDto;
import com.fserv.patientportal.dto.helper.AppointmentDtoHelper;
import com.fserv.patientportal.dto.helper.PatientAllergyDtoHelper;
import com.fserv.patientportal.model.Appointment;
import com.fserv.patientportal.repository.BaseRepository;
import com.fserv.patientportal.repository.PatientAllergyRepository;
import com.fserv.patientportal.repository.AppointmentRepository;
import com.fserv.patientportal.repository.AppointmentSearchRepo;

@Service
@Transactional
public class AppointmentServiceImpl extends BaseServiceImpl<Appointment, Integer, AppointmentDto>
    implements AppointmentService {
  @Autowired
  private AppointmentRepository appointmentRepository;
  @Autowired
  private AppointmentDtoHelper appointmentDtoHelper;
  @Autowired
  private AppointmentSearchRepo searchRepo;

  @Autowired
  public AppointmentServiceImpl(AppointmentRepository appointmentRepository,
		  AppointmentDtoHelper appointmentDtoHelper) {
	    super(appointmentRepository, appointmentDtoHelper);
	    }

  @Override
  public List<AppointmentDto> findAllwithpatientId(int patientId) {
    List<AppointmentDto> dtoList = new ArrayList<AppointmentDto>();
    Appointment patientAppointment = appointmentRepository.findAllwithId(patientId);
    dtoList.add(appointmentDtoHelper.buildDto(patientAppointment));
    return dtoList;

  }

  @Override
  public List<AppointmentDto> findAppointmentBypId(int physicianId) {
    List<AppointmentDto> dtoList = new ArrayList<AppointmentDto>();
    Appointment patientappointment = appointmentRepository.findPatient(physicianId);
    dtoList.add(appointmentDtoHelper.buildDto(patientappointment));
    return dtoList;
  }

  @Override
  public List<AppointmentDto> searchcriteria(AppointmentSearchDto dto) {
    List<Appointment> ptappointments = searchRepo.searchCriteria(dto);
    if (ptappointments != null) {
      List<AppointmentDto> dtoList = new ArrayList<AppointmentDto>();
      for (Appointment pat : ptappointments) {    	    
        dtoList.add(appointmentDtoHelper.buildDto(pat));
      }
      return dtoList;
    }
    return null;
  }

}
