package com.fserv.patientportal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.PatientReport;
import com.fserv.patientportal.model.PatientReportPK;

@Repository
@Transactional
public interface PatientReportRepository extends BaseRepository<PatientReport, PatientReportPK> {

  @Query("SELECT ptRe FROM PatientReport ptRe LEFT JOIN FETCH ptRe.document WHERE ptRe.id.documentId = :documentId AND ptRe.id.patientId = :patientId")
  public List<PatientReport> findptReports(@Param("patientId") int patientId, @Param("documentId") String ptDocsId);

}
