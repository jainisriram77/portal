package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.HealthProviderDto;

public interface HealthProviderService extends BaseService<Integer, HealthProviderDto>{

}
