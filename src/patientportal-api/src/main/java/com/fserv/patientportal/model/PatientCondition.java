package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the patient_condition database table.
 * 
 */
@Entity
@Table(name="patient_condition", schema = "patientportal")
@NamedQuery(name="PatientCondition.findAll", query="SELECT p FROM PatientCondition p")
public class PatientCondition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="condition_date")
	private Date conditionDate;

	@Column(name="condition_description")
	private String conditionDescription;

	@Column(name="condition_name")
	private String conditionName;

	@Column(name="condition_status")
	@Enumerated(EnumType.STRING)
	private ConditionStatus conditionStatus;

	@Column(name="condition_type")
	@Enumerated(EnumType.STRING)
	private ConditionType conditionType;

	@Column(name="patient_id")
	private Integer patientId;

	@Column(name="source")
	private String source;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	public PatientCondition() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getConditionDate() {
		return this.conditionDate;
	}

	public void setConditionDate(Date conditionDate) {
		this.conditionDate = conditionDate;
	}

	public String getConditionDescription() {
		return this.conditionDescription;
	}

	public void setConditionDescription(String conditionDescription) {
		this.conditionDescription = conditionDescription;
	}

	public String getConditionName() {
		return this.conditionName;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public ConditionStatus getConditionStatus() {
    return conditionStatus;
  }

  public void setConditionStatus(ConditionStatus conditionStatus) {
    this.conditionStatus = conditionStatus;
  }

  public ConditionType getConditionType() {
    return conditionType;
  }

  public void setConditionType(ConditionType conditionType) {
    this.conditionType = conditionType;
  }

  public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}