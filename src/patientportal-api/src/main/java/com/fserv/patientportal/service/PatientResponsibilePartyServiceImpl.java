package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientResponsibilePartyDto;
import com.fserv.patientportal.dto.helper.PatientResponsibilePartyDtoHelper;
import com.fserv.patientportal.model.PatientResponsibileParty;
import com.fserv.patientportal.repository.PatientResponsibilePartyRepository;

@Service
@Transactional
public class PatientResponsibilePartyServiceImpl
    extends BaseServiceImpl<PatientResponsibileParty, Integer, PatientResponsibilePartyDto>
    implements PatientResponsibilePartyService {

  @Autowired
  public PatientResponsibilePartyServiceImpl(
      PatientResponsibilePartyRepository patientResponsibilePartyRepository,
      PatientResponsibilePartyDtoHelper patientResponsibilePartyDtoHelper) {
    super(patientResponsibilePartyRepository, patientResponsibilePartyDtoHelper);
  }
}
