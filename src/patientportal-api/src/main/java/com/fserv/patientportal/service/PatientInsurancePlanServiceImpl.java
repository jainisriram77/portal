package com.fserv.patientportal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientInsurancePlanDto;
import com.fserv.patientportal.dto.PatientInsuranceSearchCriteriaDto;
import com.fserv.patientportal.dto.helper.PatientInsurancePlanDtoHelper;
import com.fserv.patientportal.dto.helper.PolicyInformationDtoHelper;
import com.fserv.patientportal.model.PatientInsurancePlan;
import com.fserv.patientportal.repository.PatientInsurancePlanRepository;
import com.fserv.patientportal.repository.PatientInsurancePlanSearchRepository;

@Service
@Transactional
public class PatientInsurancePlanServiceImpl
    extends BaseServiceImpl<PatientInsurancePlan, Integer, PatientInsurancePlanDto>
    implements PatientInsurancePlanService {

  @Autowired
  PolicyInformationDtoHelper polinfrmDtohel;

  @Autowired
  PatientInsurancePlanRepository patientInsurplanRepository;

  @Autowired
  PatientInsurancePlanDtoHelper patientInsurPlanDtoHelper;

  @Autowired
  private PatientInsurancePlanSearchRepository patientInsuplanSearchRepo;

  @Autowired
  public PatientInsurancePlanServiceImpl(PatientInsurancePlanRepository patientInsurplanRepository,
		  PatientInsurancePlanDtoHelper patientInsurPlanDtoHelper) {
	    super(patientInsurplanRepository, patientInsurPlanDtoHelper);
	    }

  @Override
  public List<PatientInsurancePlanDto> findPtInsurancePlans(int patientId) {
    List<PatientInsurancePlan> patientInsurPlan =
        patientInsurplanRepository.findPtInsurancePlans(patientId);
    return patientInsurPlanDtoHelper.buildDto(patientInsurPlan);
  }

  @Override
  public void deletePatientInsurPlan(int patientId) {}

  @Override
  public List<PatientInsurancePlanDto> searchPatientInsurplan(
      PatientInsuranceSearchCriteriaDto searchcriteria) {
    List<PatientInsurancePlan> patientInsur =
        patientInsuplanSearchRepo.searchPatinsrPlan(searchcriteria);
    return patientInsurPlanDtoHelper.buildDto(patientInsur);
  }

}
