package com.fserv.patientportal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientConditionDto;
import com.fserv.patientportal.dto.helper.PatientConditionDtoHelper;
import com.fserv.patientportal.model.ConditionType;
import com.fserv.patientportal.model.PatientCondition;
import com.fserv.patientportal.repository.PatientConditionRepository;

@Service
@Transactional
public class PatientConditionServiceImpl
    extends BaseServiceImpl<PatientCondition, Integer, PatientConditionDto>
    implements PatientConditionService {

  @Autowired
  PatientConditionRepository patientConditionRepository;

  @Autowired
  PatientConditionDtoHelper patientConditionDtoHelper;

  @Autowired
  public PatientConditionServiceImpl(PatientConditionRepository patientConditionRepository,
      PatientConditionDtoHelper patientConditionDtoHelper) {
    super(patientConditionRepository, patientConditionDtoHelper);

  }

  @Override
  public List<PatientConditionDto> findPatientBypIdAndconditionType(int patientId,
      ConditionType conditionType) {
    List<PatientCondition> patientConditions =
        patientConditionRepository.findPtConditionsByPtIdAndType(patientId, conditionType);
    return patientConditionDtoHelper.buildDto(patientConditions);
  }

  @Override
  public List<PatientConditionDto> findConditionsbypId(int patientId) {
    List<PatientCondition> patientCondition =
        patientConditionRepository.findPtConditionsByPtId(patientId);
    return patientConditionDtoHelper.buildDto(patientCondition);

  }


}
