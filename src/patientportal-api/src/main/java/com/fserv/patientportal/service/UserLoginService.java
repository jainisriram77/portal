package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.LoginRequestDto;
import com.fserv.patientportal.dto.UserDto;


public interface UserLoginService {

	public UserDto login(LoginRequestDto loginDto);

}
