package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.PatientContactInformationDto;
import com.fserv.patientportal.service.PatientContactInformationService;

@Controller
@RequestMapping(value = "/patientcontactinfo")
public class PatientContactInformationController 
	extends BaseControllerImpl<Integer, PatientContactInformationDto>{

	@Autowired(required = true)
	public PatientContactInformationController(PatientContactInformationService baseService) {
		super(baseService);
		// TODO Auto-generated constructor stub
	}

}
