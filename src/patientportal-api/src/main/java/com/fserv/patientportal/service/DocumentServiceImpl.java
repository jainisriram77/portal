package com.fserv.patientportal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.DocumentDto;
import com.fserv.patientportal.dto.DocumentSearchCriteriaDto;
import com.fserv.patientportal.dto.helper.AppointmentDtoHelper;
import com.fserv.patientportal.dto.helper.DocumentDtoHelper;
import com.fserv.patientportal.dto.helper.DtoHelper;
import com.fserv.patientportal.model.Document;
import com.fserv.patientportal.repository.AppointmentRepository;
import com.fserv.patientportal.repository.BaseRepository;
import com.fserv.patientportal.repository.DocumentRepository;
import com.fserv.patientportal.repository.DocumentSearchRepository;

@Service
@Transactional
public class DocumentServiceImpl extends BaseServiceImpl<Document, Integer, DocumentDto>
    implements DocumentService {

  @Autowired
  private DocumentRepository documentRepository;

  @Autowired
  private DocumentDtoHelper documentDtoHelper;

  @Autowired
  private DocumentSearchRepository docsSearchRepo;

  @Autowired
  public DocumentServiceImpl(DocumentRepository documentRepository,
		  DocumentDtoHelper documentDtoHelper) {
	    super(documentRepository, documentDtoHelper);
	    }

  @Override
  public List<DocumentDto> searchPtDocu(DocumentSearchCriteriaDto searchcriteria) {
    List<Document> ptDocs = docsSearchRepo.searchptDocu(searchcriteria);
    return documentDtoHelper.buildDto(ptDocs);
  }
}

