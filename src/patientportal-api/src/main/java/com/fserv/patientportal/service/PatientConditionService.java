package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.PatientConditionDto;
import com.fserv.patientportal.model.ConditionType;

public interface PatientConditionService extends BaseService<Integer, PatientConditionDto> {
  public List<PatientConditionDto> findPatientBypIdAndconditionType(int patientId,
      ConditionType conditionType);

  public List<PatientConditionDto> findConditionsbypId(int patientId);
}
