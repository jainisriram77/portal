package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.UserPreferenceDto;
import com.fserv.patientportal.dto.helper.UserPreferenceDtoHelper;
import com.fserv.patientportal.model.UserPreference;
import com.fserv.patientportal.repository.UserPreferenceRepository;

@Service
@Transactional
public class UserPreferenceServiceImpl extends
    BaseServiceImpl<UserPreference, Integer, UserPreferenceDto> implements UserPreferenceService {

  @Autowired
  public UserPreferenceServiceImpl(UserPreferenceRepository userPreferenceRepository,
      UserPreferenceDtoHelper userPreferenceDtoHelper) {
    super(userPreferenceRepository, userPreferenceDtoHelper);
  }

}
