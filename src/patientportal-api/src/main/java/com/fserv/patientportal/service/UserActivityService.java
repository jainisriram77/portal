package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.UserActivityDto;
import com.fserv.patientportal.dto.UserActivitySearchCriteriaDto;
import com.fserv.patientportal.model.UserActivityPK;

public interface UserActivityService extends BaseService<UserActivityPK, UserActivityDto> {

  List<UserActivityDto> searchUserActivities(UserActivitySearchCriteriaDto searchcriteria);

  List<UserActivityDto> findUserActivities(int userId);

}
