package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.UserActivityDto;
import com.fserv.patientportal.dto.UserActivitySearchCriteriaDto;
import com.fserv.patientportal.model.UserActivityPK;
import com.fserv.patientportal.service.UserActivityService;

@Controller
@RequestMapping(value = "/useractivity")
public class UserActivityController extends BaseControllerImpl<UserActivityPK, UserActivityDto> {

  @Autowired
  private UserActivityService userActivityService;

  @Autowired(required = true)
  public UserActivityController(UserActivityService resourceService) {
    super(resourceService);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/search/", produces = "application/json")
  public @ResponseBody List<UserActivityDto> searchUserByusIdAndActivityId(
      @RequestBody UserActivitySearchCriteriaDto searchcriteria) {
    List<UserActivityDto> useractivityDto = userActivityService.searchUserActivities(searchcriteria);
    return useractivityDto;
  }

  @RequestMapping(method = RequestMethod.GET, value = "useractivity/{usid}",
      produces = "application/json")
  public @ResponseBody List<UserActivityDto> findUserActivitysByUsrId(
      @PathVariable("usid") int userId) {
    List<UserActivityDto> activityDto = userActivityService.findUserActivities(userId);
    return activityDto;
  }
}
