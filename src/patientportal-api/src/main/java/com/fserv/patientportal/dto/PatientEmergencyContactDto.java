package com.fserv.patientportal.dto;

public class PatientEmergencyContactDto {

  private int id;
  private Integer patientId;
  private String emergencyContactName;
  private Integer emergencyContactNumber;
  private String emergencyEmail;
  private String emergencyAddress;
  private String emergencyContactRelation;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Integer getPatientId() {
    return patientId;
  }

  public void setPatientId(Integer patientId) {
    this.patientId = patientId;
  }

  public String getEmergencyContactName() {
    return emergencyContactName;
  }

  public void setEmergencyContactName(String emergencyContactName) {
    this.emergencyContactName = emergencyContactName;
  }

  public Integer getEmergencyContactNumber() {
    return emergencyContactNumber;
  }

  public void setEmergencyContactNumber(Integer emergencyContactNumber) {
    this.emergencyContactNumber = emergencyContactNumber;
  }

  public String getEmergencyEmail() {
    return emergencyEmail;
  }

  public void setEmergencyEmail(String emergencyEmail) {
    this.emergencyEmail = emergencyEmail;
  }

  public String getEmergencyAddress() {
    return emergencyAddress;
  }

  public void setEmergencyAddress(String emergencyAddress) {
    this.emergencyAddress = emergencyAddress;
  }

  public String getEmergencyContactRelation() {
    return emergencyContactRelation;
  }

  public void setEmergencyContactRelation(String emergencyContactRelation) {
    this.emergencyContactRelation = emergencyContactRelation;
  }
}
