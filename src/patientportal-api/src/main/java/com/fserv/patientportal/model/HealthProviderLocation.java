package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the health_provider_location database table.
 * 
 */
@Entity
@Table(name="health_provider_location", schema = "patientportal")
@NamedQuery(name="HealthProviderLocation.findAll", query="SELECT h FROM HealthProviderLocation h")
public class HealthProviderLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="address1")
	private String address1;

	@Column(name="address2")
	private String address2;

	@Column(name="city")
	private String city;

	@Column(name="code")
	private Integer code;

	@Column(name="email")
	private String email;

	@Column(name="health_provider_id")
	private Integer healthProviderId;

	@Column(name="is_primary_location")
	private Boolean isPrimaryLocation;

	@Column(name="name")
	private String name;

	@Column(name="office_number")
	private Integer officeNumber;

	@Column(name="state")
	private String state;

	@Column(name="zip")
	private String zip;

	//bi-directional many-to-one association to HealthProvider
	@ManyToOne
	@JoinColumn(name = "health_provider_id", insertable = false, updatable = false)
	private HealthProvider healthProvider;

	public HealthProviderLocation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getCode() {
		return this.code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getHealthProviderId() {
		return this.healthProviderId;
	}

	public void setHealthProviderId(Integer healthProviderId) {
		this.healthProviderId = healthProviderId;
	}

	public Boolean getIsPrimaryLocation() {
		return this.isPrimaryLocation;
	}

	public void setIsPrimaryLocation(Boolean isPrimaryLocation) {
		this.isPrimaryLocation = isPrimaryLocation;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOfficeNumber() {
		return this.officeNumber;
	}

	public void setOfficeNumber(Integer officeNumber) {
		this.officeNumber = officeNumber;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}



	public HealthProvider getHealthProvider() {
		return this.healthProvider;
	}

	public void setHealthProvider(HealthProvider healthProvider) {
		this.healthProvider = healthProvider;
	}

}