package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PolicyInformationDto;
import com.fserv.patientportal.model.PolicyInformation;

@Component
public class PolicyInformationDtoHelper
    implements DtoHelper<PolicyInformation, PolicyInformationDto> {

  @Override
  public List<PolicyInformationDto> buildDto(Iterable<PolicyInformation> models) {

    List<PolicyInformationDto> policyInfrDto = new ArrayList<PolicyInformationDto>();
    for (PolicyInformation policyInfr : models) {
      PolicyInformationDto dto = new PolicyInformationDto();

      dto.setCoPay(policyInfr.getCoPay());
      dto.setEmployerAddress(policyInfr.getEmployerAddress());
      dto.setEmployerCity(policyInfr.getEmployerCity());
      dto.setEmployerCountry(policyInfr.getEmployerCountry());
      dto.setEmployerName(policyInfr.getEmployerName());
      dto.setEmployerState(policyInfr.getEmployerState());
      dto.setEmployerZip(policyInfr.getEmployerZip());
      dto.setGroupNumber(policyInfr.getGroupNumber());
      dto.setId(policyInfr.getId());
      dto.setInsuranceProviderAddress(policyInfr.getInsuranceProviderAddress());
      dto.setInsuranceProviderCity(policyInfr.getInsuranceProviderCity());
      dto.setInsuranceProviderCountry(policyInfr.getInsuranceProviderCountry());
      dto.setInsuranceProviderName(policyInfr.getInsuranceProviderName());
      dto.setInsuranceProviderState(policyInfr.getInsuranceProviderState());
      dto.setInsuranceproviderZip(policyInfr.getInsuranceProviderZip());
      dto.setPolicyeffectiveDate(policyInfr.getPolicyEffectiveDate());
      dto.setPolicyHolderAddress(policyInfr.getPolicyHolderAddress());
      dto.setPolicyHolderDob(policyInfr.getPolicyHolderDob());
      dto.setPolicyHolderName(policyInfr.getPolicyHolderName());
      dto.setPolicyHolderRelation(policyInfr.getPolicyHolderRelation());
      dto.setPolicyholderSsn(policyInfr.getPolicyHolderSsn());
      dto.setPolicyHolderState(policyInfr.getPolicyHolderState());
      dto.setPolicyHolderZip(policyInfr.getPolicyHolderZip());
      dto.setPolicyNumber(policyInfr.getPolicyNumber());
      dto.setPolicyStatus(policyInfr.getPolicyStatus());
      dto.setPolicyTerminationDate(policyInfr.getPolicyTerminationDate());
      policyInfrDto.add(dto);
    }
    return policyInfrDto;
  }

  @Override
  public PolicyInformationDto buildDto(PolicyInformation model) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PolicyInformation build(PolicyInformationDto dto) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public List<PolicyInformation> build(Iterable<PolicyInformationDto> dtos) {
    // TODO Auto-generated method stub
    return null;
  }

}
