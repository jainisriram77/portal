package com.fserv.patientportal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientAllergyDto;
import com.fserv.patientportal.dto.SearchCriteriaAllergyDto;
import com.fserv.patientportal.dto.helper.PatientAllergyDtoHelper;
import com.fserv.patientportal.model.PatientAllergy;
import com.fserv.patientportal.repository.PatientAllergyRepository;
import com.fserv.patientportal.repository.PatientAllergySearchRepository;

@Service
@Transactional
public class PatientAllergyServiceImpl extends
    BaseServiceImpl<PatientAllergy, Integer, PatientAllergyDto> implements PatientAllergyService {

  @Autowired
  PatientAllergyDtoHelper patientAllergyDtoHelper;

  @Autowired
  PatientAllergyRepository patientAllergyRepository;

  @Autowired
  PatientAllergySearchRepository patientRepoAllergyCustom;

  @Autowired
  public PatientAllergyServiceImpl(PatientAllergyRepository patientAllergyRepository,
      PatientAllergyDtoHelper patientAllergyDtoHelper) {
    super(patientAllergyRepository, patientAllergyDtoHelper);

  }

  @Override
  public List<PatientAllergyDto> findAllergiesbypId(int patientId) {
    List<PatientAllergy> patientAllergy = patientAllergyRepository.findAllergiesbypId(patientId);
    return patientAllergyDtoHelper.buildDto(patientAllergy);
  }

  @Override
  public List<PatientAllergyDto> search(SearchCriteriaAllergyDto searchAllergyDto) {
    List<PatientAllergy> pAllergy = patientRepoAllergyCustom.search(searchAllergyDto);
    return patientAllergyDtoHelper.buildDto(pAllergy);
  }
}
