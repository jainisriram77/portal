package com.fserv.patientportal.repository;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fserv.patientportal.model.HealthProvider;

@Repository
@Transactional
public interface HealthProviderRepository extends BaseRepository<HealthProvider, Integer>{

}
