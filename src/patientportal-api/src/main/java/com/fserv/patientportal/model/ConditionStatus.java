package com.fserv.patientportal.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ConditionStatus {
  ACTIVE, INACTIVE, OTHER;

    @JsonValue
    public String getValue() {
      return name();
    }

    @JsonCreator
    public static ConditionStatus create(String val) {
      ConditionStatus[] aStatuses = ConditionStatus.values();
      for (ConditionStatus aStatus : aStatuses) {

        if (aStatus.name().equalsIgnoreCase(val)) {
          return aStatus;
        }
      }
      return OTHER;
    }

  }
