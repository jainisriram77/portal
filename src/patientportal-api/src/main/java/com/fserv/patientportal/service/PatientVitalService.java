package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.PatientVitalDto;

public interface PatientVitalService extends BaseService<Integer, PatientVitalDto> {

}
