 package com.fserv.patientportal.controller;

 import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.PatientReportDto;
import com.fserv.patientportal.dto.PatientReportSearchCriteriaDto;
import com.fserv.patientportal.model.PatientReportPK;
import com.fserv.patientportal.service.PatientReportService;

 @Controller
 @RequestMapping(value="/patientreports")
 public class PatientReportController extends BaseControllerImpl<PatientReportPK, PatientReportDto>{


 @Autowired
 private PatientReportService ptReportService;

 @Autowired(required=true)
 public PatientReportController(PatientReportService baseService) {
 super(baseService);
 }

 @RequestMapping(method = RequestMethod.POST, value = "/search/", produces = "application/json")
 public @ResponseBody List<PatientReportDto> searchUserByusIdAndActivityId(
     @RequestBody PatientReportSearchCriteriaDto searchcriteria) {
   List<PatientReportDto> patientReptDto= ptReportService.searchPatientReport(searchcriteria);
   return patientReptDto;
 }
 
 @RequestMapping (value = "/{ptResultId}/{ptId}", method = RequestMethod.GET, produces="application/json")
 public @ResponseBody List<PatientReportDto> findPatientReportByptIdAndDocsId(@PathVariable("patientId")
 int patientId,@PathVariable("documentId") String ptDocsId ){
 List<PatientReportDto> ptReportDto=ptReportService.findPtreportByDocsIdAndpId(patientId, ptDocsId);
 return ptReportDto;
 }
 }
