package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.AppointmentDto;
import com.fserv.patientportal.dto.AppointmentSearchDto;
import com.fserv.patientportal.service.AppointmentService;

@Controller
@RequestMapping(value = "/appointments")
public class AppointmentController extends BaseControllerImpl<Integer, AppointmentDto> {

  @Autowired
  AppointmentService appointmentService;

  @Autowired(required = true)
  public AppointmentController(AppointmentService resourceService) {
    super(resourceService);
  }

  // there will be more that one apointments for a patient
  @RequestMapping(value = "/getbypatientid/{patientId}", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<AppointmentDto> getwithpatientId(
      @PathVariable(value = "patientId") int patientId) {
    List<AppointmentDto> dto = appointmentService.findAllwithpatientId(patientId);
    return dto;
  }

  // there will be more that one apointments for a physician
  @RequestMapping(value = "/getbyphysicianid/{physicianId}", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<AppointmentDto> findBypId(
      @PathVariable("physicianId") int physicianId) {
    List<AppointmentDto> patientappointmentDto =
        appointmentService.findAppointmentBypId(physicianId);
    return patientappointmentDto;
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody List<AppointmentDto> search(@RequestBody AppointmentSearchDto dto) {
    List<AppointmentDto> dt = appointmentService.searchcriteria(dto);
    return dt;
  }
  
  

}
