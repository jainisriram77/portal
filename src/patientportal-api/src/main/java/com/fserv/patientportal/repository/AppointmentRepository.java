package com.fserv.patientportal.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.Appointment;

@Repository
@Transactional
public interface AppointmentRepository extends BaseRepository<Appointment, Integer> {

  @Query("select a from Appointment a where a.patient.id=:patientId")
  public Appointment findAllwithId(@Param("patientId") int patientId);


  @Query("select a from Appointment a where a.physician.id = :physicianId")
  public Appointment findPatient(@Param("physicianId") int physicianId);


}
