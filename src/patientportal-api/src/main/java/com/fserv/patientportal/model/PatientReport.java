package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the patient_reports database table.
 * 
 */
@Entity
@Table(name="patient_reports", schema = "patientportal")
@NamedQuery(name="PatientReport.findAll", query="SELECT p FROM PatientReport p")
public class PatientReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PatientReportPK id;

	@Column(name="appointment_id")
	private Integer appointmentId;

	@Temporal(TemporalType.DATE)
	@Column(name="captured_date")
	private Date capturedDate;

	@Column(name="report_type")
	private String reportType;

	//bi-directional many-to-one association to Appointment
	@ManyToOne
	@JoinColumn(name = "appointment_id",insertable = false, updatable = false)
	private Appointment appointment;

	//bi-directional many-to-one association to Document
	@ManyToOne
	@JoinColumn(name = "document_id",insertable = false, updatable = false)
	private Document document;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	public PatientReport() {
	}

	public PatientReportPK getId() {
		return this.id;
	}

	public void setId(PatientReportPK id) {
		this.id = id;
	}

	public Integer getAppointmentId() {
		return this.appointmentId;
	}

	public void setAppointmentId(Integer appointmentId) {
		this.appointmentId = appointmentId;
	}

	public Date getCapturedDate() {
		return this.capturedDate;
	}

	public void setCapturedDate(Date capturedDate) {
		this.capturedDate = capturedDate;
	}

	public String getReportType() {
		return this.reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public Appointment getAppointment() {
		return this.appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}