package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the patient_vital database table.
 * 
 */
@Entity
@Table(name="patient_vital", schema = "patientportal")
@NamedQuery(name="PatientVital.findAll", query="SELECT p FROM PatientVital p")
public class PatientVital implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="appointment_id")
	private Integer appointmentId;

	@Column(name="patient_id")
	private Integer patientId;

	@Column(name="source")
	private String source;

	@Temporal(TemporalType.DATE)
	@Column(name="vital_date")
	private Date vitalDate;

	@Column(name="vital_name")
	private String vitalName;

	@Column(name="vital_value")
	private String vitalValue;
	
	@ManyToOne
	@JoinColumn(name = "appointment_id",insertable = false, updatable = false)
    private Appointment appointment;
	
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
    private Patient patient;

	public PatientVital() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAppointmentId() {
		return this.appointmentId;
	}

	public void setAppointmentId(Integer appointmentId) {
		this.appointmentId = appointmentId;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getVitalDate() {
		return this.vitalDate;
	}

	public void setVitalDate(Date vitalDate) {
		this.vitalDate = vitalDate;
	}

	public String getVitalName() {
		return this.vitalName;
	}

	public void setVitalName(String vitalName) {
		this.vitalName = vitalName;
	}

	public String getVitalValue() {
		return this.vitalValue;
	}

	public void setVitalValue(String vitalValue) {
		this.vitalValue = vitalValue;
	}

	public Appointment getAppointment() {
      return this.appointment;
  }

  public void setAppointment(Appointment appointment) {
      this.appointment = appointment;
  }

  public Patient getPatient(){
    return this.patient;
  }
  
  public void setPatient(Patient patient){
    this.patient = patient;
  }
    

}