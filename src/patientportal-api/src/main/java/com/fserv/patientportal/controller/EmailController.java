package com.fserv.patientportal.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.AppointmentDto;
import com.fserv.patientportal.dto.ComposeEmailDto;
import com.fserv.patientportal.service.AmazonSESService;
import com.fserv.patientportal.service.AppointmentService;
import com.fserv.patientportal.service.ComposeEmailService;

@Controller
@RequestMapping(value = "/email")
public class EmailController extends BaseControllerImpl<Integer, ComposeEmailDto>{

	@Autowired
	AmazonSESService sesService;
	@Autowired
	ComposeEmailService composeEmailService;
	
	@Autowired(required = true)
	  public EmailController(ComposeEmailService composeEmailService) {
	    super(composeEmailService);
	  }
	
	@RequestMapping(value = "/getbyId/{Id}", method = RequestMethod.GET,
		      produces = "application/json")
		  public @ResponseBody List<ComposeEmailDto> findComposeEmailbyId(
		      @PathVariable(value ="Id") int Id) {
		    List<ComposeEmailDto> composeEmailDto = composeEmailService.findComposeEmailbyId(Id);
		    return composeEmailDto;
		  }
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/compose/", produces = "application/json")
	public @ResponseBody ResponseEntity<ComposeEmailDto> sendEmail(
			@RequestBody ComposeEmailDto sendDto) {
		try {
			sesService.sendEmail(sendDto.getSubject(), sendDto.getBody(),
					sendDto.getToEmail());
			
			return new ResponseEntity<ComposeEmailDto>(sendDto, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<ComposeEmailDto>(HttpStatus.BAD_REQUEST);
	}

	
	
	
	
}
