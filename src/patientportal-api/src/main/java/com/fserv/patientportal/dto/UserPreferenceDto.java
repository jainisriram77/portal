package com.fserv.patientportal.dto;

public class UserPreferenceDto {

  private int id;
  private int userId;
  private String preferenceName;
  private String preferenceValue;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getPreferenceName() {
    return preferenceName;
  }

  public void setPreferenceName(String preferenceName) {
    this.preferenceName = preferenceName;
  }

  public String getPreferenceValue() {
    return preferenceValue;
  }

  public void setPreferenceValue(String preferenceValue) {
    this.preferenceValue = preferenceValue;
  }
}
