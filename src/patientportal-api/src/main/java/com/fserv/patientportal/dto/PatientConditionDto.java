package com.fserv.patientportal.dto;

import java.util.Date;

import com.fserv.patientportal.model.ConditionStatus;
import com.fserv.patientportal.model.ConditionType;

public class PatientConditionDto {

  private int id;
  private Date conditionDate;
  private String conditionDescription;
  private String conditionName;

  private ConditionStatus conditionStatus;
  private ConditionType conditionType;

  private String source;
  private int patientId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getConditionDate() {
    return conditionDate;
  }

  public void setConditionDate(Date conditionDate) {
    this.conditionDate = conditionDate;
  }

  public String getConditionDescription() {
    return conditionDescription;
  }

  public void setConditionDescription(String conditionDescription) {
    this.conditionDescription = conditionDescription;
  }

  public String getConditionName() {
    return conditionName;
  }

  public void setConditionName(String conditionName) {
    this.conditionName = conditionName;
  }

  public ConditionStatus getConditionStatus() {
    return conditionStatus;
  }

  public void setConditionStatus(ConditionStatus conditionStatus) {
    this.conditionStatus = conditionStatus;
  }

  public ConditionType getConditionType() {
    return conditionType;
  }

  public void setConditionType(ConditionType conditionType) {
    this.conditionType = conditionType;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public int getPatientId() {
    return patientId;
  }

  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }
}
