package com.fserv.patientportal.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.Physician;

@Repository
@Transactional
public interface PhysicianRepository extends BaseRepository<Physician, Integer> {

}
