package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the activity database table.
 * 
 */
@Entity
@Table(name="activity", schema = "patientportal")
@NamedQuery(name="Activity.findAll", query="SELECT a FROM Activity a")
public class Activity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="activity_date")
	private Date activityDate;

	@Column(name="activity_type")
	@Enumerated(EnumType.STRING)
	private ActivityType activityType;

	@Column(name="from")
	private String from;

	@Column(name="message")
	private String message;

	@Column(name="subject")
	private String subject;

	@Column(name="to")
	private String to;

	//bi-directional many-to-one association to UserActivity
	@OneToMany(mappedBy="activity")
	private List<UserActivity> userActivities;

	public Activity() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getActivityDate() {
		return this.activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public ActivityType getActivityType() {
    return activityType;
  }

  public void setActivityType(ActivityType activityType) {
    this.activityType = activityType;
  }

  public String getFrom() {
		return this.from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTo() {
		return this.to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public List<UserActivity> getUserActivities() {
		return this.userActivities;
	}

	public void setUserActivities(List<UserActivity> userActivities) {
		this.userActivities = userActivities;
	}

//	public UserActivity addUserActivity(UserActivity userActivity) {
//		getUserActivities().add(userActivity);
//		userActivity.setActivity(this);
//
//		return userActivity;
//	}
//
//	public UserActivity removeUserActivity(UserActivity userActivity) {
//		getUserActivities().remove(userActivity);
//		userActivity.setActivity(null);
//
//		return userActivity;
//	}

}