package com.fserv.patientportal.dto;

import com.fserv.patientportal.model.Patient;

public class PatientResponsibilePartyDto {
  private int patientResponsibilePartyId;
  private String responsibilePartyAddress;
  private String responsibilePartyEmail;
  private String responsibilePartyName;
  private int responsibilePartyNumber;
  private Patient patient;

  public int getPatientResponsibilePartyId() {
    return patientResponsibilePartyId;
  }

  public void setPatientResponsibilePartyId(int patientResponsibilePartyId) {
    this.patientResponsibilePartyId = patientResponsibilePartyId;
  }

  public String getResponsibilePartyAddress() {
    return responsibilePartyAddress;
  }

  public void setResponsibilePartyAddress(String responsibilePartyAddress) {
    this.responsibilePartyAddress = responsibilePartyAddress;
  }

  public String getResponsibilePartyEmail() {
    return responsibilePartyEmail;
  }

  public void setResponsibilePartyEmail(String responsibilePartyEmail) {
    this.responsibilePartyEmail = responsibilePartyEmail;
  }

  public String getResponsibilePartyName() {
    return responsibilePartyName;
  }

  public void setResponsibilePartyName(String responsibilePartyName) {
    this.responsibilePartyName = responsibilePartyName;
  }

  public int getResponsibilePartyNumber() {
    return responsibilePartyNumber;
  }

  public void setResponsibilePartyNumber(int responsibilePartyNumber) {
    this.responsibilePartyNumber = responsibilePartyNumber;
  }

  public Patient getPatient() {
    return patient;
  }

  public void setPatient(Patient patient) {
    this.patient = patient;
  }

}
