package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.PhysicianDto;
import com.fserv.patientportal.service.PhysicianService;


@Controller
@RequestMapping(value = "/physiciancontroller")
public class PhysicianController extends BaseControllerImpl<Integer, PhysicianDto> {

  @Autowired
  private PhysicianService physicianservice;

  @Autowired(required = true)
  public PhysicianController(PhysicianService resourceService) {
    super(resourceService);
  }

}
