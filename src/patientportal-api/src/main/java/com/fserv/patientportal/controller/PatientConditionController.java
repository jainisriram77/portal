package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.PatientConditionDto;
import com.fserv.patientportal.model.ConditionType;
import com.fserv.patientportal.service.PatientConditionService;

@Controller
@RequestMapping(value = "/patientconditions/")
public class PatientConditionController extends BaseControllerImpl<Integer, PatientConditionDto> {

  @Autowired
  private PatientConditionService patientConditionService;


  @Autowired(required = true)
  public PatientConditionController(PatientConditionService resourceService) {
    super(resourceService);
  }


  @RequestMapping(value = "/{patientId}/{conditionType}", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<PatientConditionDto> findPtConditionsByPtIdAndConditionType(
      @PathVariable("patientId") int patientId,
      @PathVariable("conditionType") ConditionType conditionType) {
    List<PatientConditionDto> patientConditionDto =
        patientConditionService.findPatientBypIdAndconditionType(patientId, conditionType);
    return patientConditionDto;
  }


  @RequestMapping(value = "/getbyptId/{patientId}", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<PatientConditionDto> findPtConditionsByPtId(
      @PathVariable("patientId") int patientId) {
    List<PatientConditionDto> patientConditionDto =
        patientConditionService.findConditionsbypId(patientId);
    return patientConditionDto;
  }

}


