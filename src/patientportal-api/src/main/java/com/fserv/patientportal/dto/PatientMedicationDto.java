package com.fserv.patientportal.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fserv.patientportal.model.MedicationType;

public class PatientMedicationDto {
  private int id;
  private String medicationDescription;
  private String medicationDirections;
  @JsonFormat(pattern = "yyyy-mm-dd")
  private Date medicationEndDate;
  private String medicationName;
  @JsonFormat(pattern = "yyyy-mm-dd")
  private Date medicationStartDate;
  private String medicationStatus;
  private MedicationType medicationType;
  @JsonFormat(pattern = "yyyy-mm-dd")
  private Date prescribedDate;
  private int patientId;
  private int physicianId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getMedicationDescription() {
    return medicationDescription;
  }

  public void setMedicationDescription(String medicationDescription) {
    this.medicationDescription = medicationDescription;
  }

  public String getMedicationDirections() {
    return medicationDirections;
  }

  public void setMedicationDirections(String medicationDirections) {
    this.medicationDirections = medicationDirections;
  }

  public Date getMedicationEndDate() {
    return medicationEndDate;
  }

  public void setMedicationEndDate(Date medicationEndDate) {
    this.medicationEndDate = medicationEndDate;
  }

  public String getMedicationName() {
    return medicationName;
  }

  public void setMedicationName(String medicationName) {
    this.medicationName = medicationName;
  }

  public Date getMedicationStartDate() {
    return medicationStartDate;
  }

  public void setMedicationStartDate(Date medicationStartDate) {
    this.medicationStartDate = medicationStartDate;
  }

  public String getMedicationStatus() {
    return medicationStatus;
  }

  public void setMedicationStatus(String medicationStatus) {
    this.medicationStatus = medicationStatus;
  }

  public MedicationType getMedicationType() {
    return medicationType;
  }

  public void setMedicationType(MedicationType medicationType) {
    this.medicationType = medicationType;
  }

  public Date getPrescribedDate() {
    return prescribedDate;
  }

  public void setPrescribedDate(Date prescribedDate) {
    this.prescribedDate = prescribedDate;
  }

  public int getPatientId() {
    return patientId;
  }

  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }

  public int getPhysicianId() {
    return physicianId;
  }

  public void setPhysicianId(int physicianId) {
    this.physicianId = physicianId;
  }

}
