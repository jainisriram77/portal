package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientConditionDto;
import com.fserv.patientportal.model.Patient;
import com.fserv.patientportal.model.PatientCondition;


@Component
public class PatientConditionDtoHelper implements DtoHelper<PatientCondition, PatientConditionDto> {
  @Override
  public List<PatientConditionDto> buildDto(Iterable<PatientCondition> models) {
    List<PatientConditionDto> userDtoList = null;
    if (models != null) {
      userDtoList = new ArrayList<PatientConditionDto>();
      for (PatientCondition usr : models) {
        userDtoList.add(buildDto(usr));
      }
    }
    return userDtoList;
  }

  @Override
  public PatientConditionDto buildDto(PatientCondition model) {
    PatientConditionDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new PatientConditionDto();
      dtoToReturn.setId(model.getId());
      dtoToReturn.setConditionDate(model.getConditionDate());
      dtoToReturn.setConditionDescription(model.getConditionDescription());
      dtoToReturn.setConditionName(model.getConditionName());
      dtoToReturn.setConditionStatus(model.getConditionStatus());
      dtoToReturn.setConditionType(model.getConditionType());
      dtoToReturn.setPatientId(model.getPatientId());
      dtoToReturn.setSource(model.getSource());
    }
    return dtoToReturn;
  }

  @Override
  public PatientCondition build(PatientConditionDto dto) {
    PatientCondition model = null;
    if (dto != null) {
      model = new PatientCondition();
      model.setId(dto.getId());
      model.setConditionDate(dto.getConditionDate());
      model.setConditionDescription(dto.getConditionDescription());
      model.setConditionName(dto.getConditionName());
      model.setConditionStatus(dto.getConditionStatus());
      model.setConditionType(dto.getConditionType());
      model.setSource(dto.getSource());
      Patient pc = new Patient();
      pc.setId(dto.getPatientId());
      model.setPatient(pc);
    }
    return model;
  }

  @Override
  public List<PatientCondition> build(Iterable<PatientConditionDto> dtos) {
    List<PatientCondition> models = null;
    if (dtos != null) {
      models = new ArrayList<PatientCondition>();
      for (PatientConditionDto usrdto : dtos) {
        models.add(build(usrdto));
      }
    }
    return models;
  }
}
