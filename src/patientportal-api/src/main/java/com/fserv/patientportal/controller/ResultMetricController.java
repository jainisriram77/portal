package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.ResultMetricDto;
import com.fserv.patientportal.service.ResultMetricService;

@Controller
@RequestMapping(value = "resultmetrics")
public class ResultMetricController extends BaseControllerImpl<Integer, ResultMetricDto> {

  @Autowired(required = true)
  public ResultMetricController(ResultMetricService baseService) {
    super(baseService);
    // TODO Auto-generated constructor stub
  }

}
