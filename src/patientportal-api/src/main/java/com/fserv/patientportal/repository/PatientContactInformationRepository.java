package com.fserv.patientportal.repository;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fserv.patientportal.model.PatientContactInformation;


@Repository
@Transactional
public interface PatientContactInformationRepository 
	extends BaseRepository<PatientContactInformation, Integer> {

}
