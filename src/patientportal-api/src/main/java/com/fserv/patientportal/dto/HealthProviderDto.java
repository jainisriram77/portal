package com.fserv.patientportal.dto;

public class HealthProviderDto {

  private int healthProviderId;

  private String name;

  public int getHealthProviderId() {
    return healthProviderId;
  }

  public void setHealthProviderId(int healthProviderId) {
    this.healthProviderId = healthProviderId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


}
