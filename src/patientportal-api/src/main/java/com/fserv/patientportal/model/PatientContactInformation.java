
package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the patient_contact_information database table.
 * 
 */
@Entity
@Table(name="patient_contact_information", schema = "patientportal")
@NamedQuery(name="PatientContactInformation.findAll", query="SELECT p FROM PatientContactInformation p")
public class PatientContactInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="address1")
	private String address1;

	@Column(name="address2")
	private String address2;

	@Column(name="city")
	private String city;

	@Column(name="contact_type")
	private String contactType;

	@Column(name="country")
	private String country;

	@Column(name="email")
	private String email;

	@Column(name="patient_id")
	private Integer patientId;

	@Column(name="phone_number")
	private Integer phoneNumber;

	@Column(name="state")
	private String state;

	@Column(name="zip")
	private Integer zip;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	public PatientContactInformation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContactType() {
		return this.contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Integer getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getZip() {
		return this.zip;
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}