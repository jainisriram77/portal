package com.fserv.patientportal.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fserv.patientportal.dto.PatientInsuranceSearchCriteriaDto;
import com.fserv.patientportal.model.PatientInsurancePlan;

@Repository
@Transactional
public class PatientInsurancePlanSearchRepositoryImpl
    implements PatientInsurancePlanSearchRepository {

  @PersistenceContext
  private EntityManager entityMgr;

  @SuppressWarnings("unchecked")
  @Override
  public List<PatientInsurancePlan> searchPatinsrPlan(
      PatientInsuranceSearchCriteriaDto searchcriteria) {

    StringBuilder query = new StringBuilder();

    query.append("SELECT ptinp FROM PatientInsurancePlan AS ptinp ");
    // query.append("LEFT JOIN FETCH polinfrm PolicyInformation ON( polinfrm.ID
    // =ptinp.policyInformation.id)");
    query.append("LEFT JOIN FETCH ptinp.policyInformation AS polinfrm ");
    // query.append("LEFT JOIN FETCH pt Patient ON(pt.id= ptinp.patient.id");
    query.append("LEFT JOIN FETCH ptinp.patient AS pt ");
    query.append("WHERE 1=1 ");

    if ((!StringUtils.isEmpty(searchcriteria.getPatientId()))) {
      query.append("AND pt.id=:patientid ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getInsuranceProviderName()))) {
      query.append("AND polinfrm.insuranceProviderName=:insuranceProviderName ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getPolicyHolderName()))) {
      query.append("AND polinfrm.policyHolderName=:policyHolderName ");
    }
    if (searchcriteria.getPolicyholderSsn() != 0) {
      query.append("AND polinfrm.policyHolderSsn=:policyHolderSsn ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getPolicyStatus()))) {
      query.append("AND polinfrm.policyStatus=:policyStatus ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getInsuranceProviderState()))) {
      query.append("AND polinfrm.insuranceProviderState=:insuranceProviderState ");
    }

    Query query1 = entityMgr.createQuery(query.toString());

    if ((!StringUtils.isEmpty(searchcriteria.getPatientId())))
      query1.setParameter("patientid", searchcriteria.getPatientId());

    if ((!StringUtils.isEmpty(searchcriteria.getInsuranceProviderName())))
      query1.setParameter("insuranceProviderName", searchcriteria.getInsuranceProviderName());

    if ((!StringUtils.isEmpty(searchcriteria.getPolicyHolderName())))
      query1.setParameter("policyHolderName", searchcriteria.getPolicyHolderName());

    if (searchcriteria.getPolicyholderSsn() != 0)
      query1.setParameter("policyHolderSsn", searchcriteria.getPolicyholderSsn());

    if ((!StringUtils.isEmpty(searchcriteria.getPolicyStatus())))
      query1.setParameter("policyStatus", searchcriteria.getPolicyStatus());

    if ((!StringUtils.isEmpty(searchcriteria.getInsuranceProviderState())))
      query1.setParameter("insuranceProviderState", searchcriteria.getInsuranceProviderState());

    List<PatientInsurancePlan> resultList = query1.getResultList();
    return resultList;
  }

}
