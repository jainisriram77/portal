package com.fserv.patientportal.repository;

import java.util.List;

import com.fserv.patientportal.dto.UserActivitySearchCriteriaDto;
import com.fserv.patientportal.model.UserActivity;

public interface UserActivitySearchRepository {

  public List<UserActivity> searchUserActivities(
      UserActivitySearchCriteriaDto usrActivitySearchCriteria);
}
