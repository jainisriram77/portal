package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientInsurancePlanDto;
import com.fserv.patientportal.dto.PolicyInformationDto;
import com.fserv.patientportal.model.PatientInsurancePlan;
import com.fserv.patientportal.model.PolicyInformation;

@Component
public class PatientInsurancePlanDtoHelper
    implements DtoHelper<PatientInsurancePlan, PatientInsurancePlanDto> {


  @Override
  public List<PatientInsurancePlanDto> buildDto(Iterable<PatientInsurancePlan> models) {
    List<PatientInsurancePlanDto> patInsurplanDtoList = new ArrayList<PatientInsurancePlanDto>();
    for (PatientInsurancePlan ptInsurplan : models) {
      PatientInsurancePlanDto patientInsurancePlanDto = new PatientInsurancePlanDto();
      patientInsurancePlanDto.setId(ptInsurplan.getId());
      patientInsurancePlanDto.setPatientId(ptInsurplan.getPatientId());
      patientInsurancePlanDto.setPtInsurancePlanStatus(ptInsurplan.getPtInsurancePlanStatus());

      PolicyInformationDto policyInfoDto = new PolicyInformationDto();
      PolicyInformation polinfrm = ptInsurplan.getPolicyInformation();
      policyInfoDto.setCoPay(polinfrm.getCoPay());
      policyInfoDto.setEmployerAddress(polinfrm.getEmployerAddress());
      policyInfoDto.setEmployerCity(polinfrm.getEmployerCity());
      policyInfoDto.setEmployerCountry(polinfrm.getEmployerCountry());
      policyInfoDto.setEmployerName(polinfrm.getEmployerName());
      policyInfoDto.setEmployerState(polinfrm.getEmployerState());
      policyInfoDto.setEmployerZip(polinfrm.getEmployerZip());
      policyInfoDto.setGroupNumber(polinfrm.getGroupNumber());
      policyInfoDto.setId(polinfrm.getId());
      policyInfoDto.setInsuranceProviderAddress(polinfrm.getInsuranceProviderAddress());
      policyInfoDto.setInsuranceProviderCity(polinfrm.getInsuranceProviderCity());
      policyInfoDto.setInsuranceProviderCountry(polinfrm.getInsuranceProviderCountry());
      policyInfoDto.setInsuranceProviderName(polinfrm.getInsuranceProviderName());
      policyInfoDto.setInsuranceProviderState(polinfrm.getInsuranceProviderState());
      policyInfoDto.setInsuranceproviderZip(polinfrm.getInsuranceProviderZip());
      policyInfoDto.setPolicyeffectiveDate(polinfrm.getPolicyEffectiveDate());
      policyInfoDto.setPolicyHolderAddress(polinfrm.getPolicyHolderAddress());
      policyInfoDto.setPolicyHolderDob(polinfrm.getPolicyHolderDob());
      policyInfoDto.setPolicyHolderName(polinfrm.getPolicyHolderName());
      policyInfoDto.setPolicyHolderRelation(polinfrm.getPolicyHolderRelation());
      policyInfoDto.setPolicyholderSsn(polinfrm.getPolicyHolderSsn());
      policyInfoDto.setPolicyHolderState(polinfrm.getPolicyHolderState());
      policyInfoDto.setPolicyHolderZip(polinfrm.getPolicyHolderZip());
      policyInfoDto.setPolicyNumber(polinfrm.getPolicyNumber());
      policyInfoDto.setPolicyStatus(polinfrm.getPolicyStatus());
      policyInfoDto.setPolicyTerminationDate(polinfrm.getPolicyTerminationDate());
      patientInsurancePlanDto.setPolicyinformationDto(policyInfoDto);
      patInsurplanDtoList.add(patientInsurancePlanDto);
    }
    return patInsurplanDtoList;
  }

  @Override
  public PatientInsurancePlan build(PatientInsurancePlanDto dto) {
    PatientInsurancePlan patinsurplan = new PatientInsurancePlan();
    patinsurplan.setId(dto.getId());
    patinsurplan.setPtInsurancePlanStatus(dto.getPtInsurancePlanStatus());
    return patinsurplan;
  }

  @Override
  public List<PatientInsurancePlan> build(Iterable<PatientInsurancePlanDto> dtos) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PatientInsurancePlanDto buildDto(PatientInsurancePlan model) {
    // TODO Auto-generated method stub
    return null;
  }
}
