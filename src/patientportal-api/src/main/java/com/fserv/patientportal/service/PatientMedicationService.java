package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.PatientMedicationDto;
import com.fserv.patientportal.model.MedicationType;

public interface PatientMedicationService extends BaseService<Integer, PatientMedicationDto> {

  List<PatientMedicationDto> findPtMedicationsByPtIdAndMedicationType(int patientId,
      MedicationType medicationType);

}
