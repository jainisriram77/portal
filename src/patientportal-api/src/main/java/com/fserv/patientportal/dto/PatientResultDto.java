package com.fserv.patientportal.dto;

import java.util.Date;

public class PatientResultDto {

  private int id;
  private Date resultDate;
  private String resultDetails;
  private String resultProvider;
  private String resultValue;
  private int patientAppointmentId;
  private int patientId;
  private int resultMetricId;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getResultDate() {
    return resultDate;
  }

  public void setResultDate(Date resultDate) {
    this.resultDate = resultDate;
  }

  public String getResultDetails() {
    return resultDetails;
  }

  public void setResultDetails(String resultDetails) {
    this.resultDetails = resultDetails;
  }

  public String getResultProvider() {
    return resultProvider;
  }

  public void setResultProvider(String resultProvider) {
    this.resultProvider = resultProvider;
  }

  public String getResultValue() {
    return resultValue;
  }

  public void setResultValue(String resultValue) {
    this.resultValue = resultValue;
  }

  public int getPatientAppointmentId() {
    return patientAppointmentId;
  }

  public void setPatientAppointmentId(int patientAppointmentId) {
    this.patientAppointmentId = patientAppointmentId;
  }

  public int getPatientId() {
    return patientId;
  }

  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }

  public int getResultMetricId() {
    return resultMetricId;
  }

  public void setResultMetricId(int resultMetricId) {
    this.resultMetricId = resultMetricId;
  }
}
