package com.fserv.patientportal.dto;

public class SearchCriteriaAllergyDto {

  private String allergyDescription;
  private String ptName;
  private Integer patientId;

  public String getAllergyDescription() {
    return allergyDescription;
  }

  public void setAllergyDescription(String allergyDescription) {
    this.allergyDescription = allergyDescription;
  }

  public String getPtName() {
    return ptName;
  }

  public void setPtName(String ptName) {
    this.ptName = ptName;
  }

  public Integer getPatientId() {
    return patientId;
  }

  public void setPatientId(Integer patientId) {
    this.patientId = patientId;
  }

}
