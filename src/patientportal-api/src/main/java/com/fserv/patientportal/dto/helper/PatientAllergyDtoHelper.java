package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientAllergyDto;
import com.fserv.patientportal.model.Patient;
import com.fserv.patientportal.model.PatientAllergy;

@Component
public class PatientAllergyDtoHelper implements DtoHelper<PatientAllergy, PatientAllergyDto> {
  @Override
  public List<PatientAllergyDto> buildDto(Iterable<PatientAllergy> models) {
    List<PatientAllergyDto> userDtoList = null;
    if (models != null) {
      userDtoList = new ArrayList<PatientAllergyDto>();
      for (PatientAllergy usr : models) {
        userDtoList.add(buildDto(usr));
      }
    }
    return userDtoList;
  }

  @Override
  public PatientAllergyDto buildDto(PatientAllergy model) {
    PatientAllergyDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new PatientAllergyDto();
      dtoToReturn.setId(model.getId());
      dtoToReturn.setAllergyDescription(model.getAllergyDescription());
      dtoToReturn.setAllergyRemarks(model.getAllergyRemarks());
      dtoToReturn.setPatientId(model.getPatientId());

    }
    return dtoToReturn;
  }

  @Override
  public PatientAllergy build(PatientAllergyDto dto) {
    PatientAllergy model = null;
    if (dto != null) {
      model = new PatientAllergy();
      model.setId(dto.getId());
      model.setAllergyDescription(dto.getAllergyDescription());
      model.setAllergyRemarks(dto.getAllergyRemarks());

      Patient pc = new Patient();
      pc.setId(dto.getPatientId());
      model.setPatient(pc);
    }
    return model;
  }

  @Override
  public List<PatientAllergy> build(Iterable<PatientAllergyDto> dtos) {
    List<PatientAllergy> models = null;
    if (dtos != null) {
      models = new ArrayList<PatientAllergy>();
      for (PatientAllergyDto usrdto : dtos) {
        models.add(build(usrdto));
      }
    }
    return models;
  }
}
