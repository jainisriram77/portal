package com.fserv.patientportal.repository;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fserv.patientportal.model.PatientVital;


@Repository
@Transactional
public interface PatientVitalRepository extends BaseRepository<PatientVital, Integer> {

}
