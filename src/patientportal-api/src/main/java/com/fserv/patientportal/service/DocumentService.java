package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.DocumentDto;
import com.fserv.patientportal.dto.DocumentSearchCriteriaDto;

public interface DocumentService extends BaseService<Integer, DocumentDto> {

  List<DocumentDto> searchPtDocu(DocumentSearchCriteriaDto searchcriteria);


}
