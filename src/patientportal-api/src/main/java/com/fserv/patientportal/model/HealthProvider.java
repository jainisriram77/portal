package com.fserv.patientportal.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the health_provider database table.
 * 
 */
@Entity
@Table(name="health_provider", schema = "patientportal")
@NamedQuery(name="HealthProvider.findAll", query="SELECT h FROM HealthProvider h")
public class HealthProvider implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="health_provider_id")
	private Integer healthProviderId;

	@Column(name="name")
	private String name;
	
	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="healthProvider")
	private List<Appointment> appointments;

	//bi-directional many-to-one association to HealthProviderLocation
	@OneToMany(mappedBy="healthProvider")
	private List<HealthProviderLocation> healthProviderLocations;

	public HealthProvider() {
	}

	public Integer getHealthProviderId() {
		return this.healthProviderId;
	}

	public void setHealthProviderId(Integer healthProviderId) {
		this.healthProviderId = healthProviderId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<HealthProviderLocation> getHealthProviderLocations() {
		return this.healthProviderLocations;
	}

	public void setHealthProviderLocations(List<HealthProviderLocation> healthProviderLocations) {
		this.healthProviderLocations = healthProviderLocations;
	}

	public HealthProviderLocation addHealthProviderLocation(HealthProviderLocation healthProviderLocation) {
		getHealthProviderLocations().add(healthProviderLocation);
		healthProviderLocation.setHealthProvider(this);

		return healthProviderLocation;
	}

	public HealthProviderLocation removeHealthProviderLocation(HealthProviderLocation healthProviderLocation) {
		getHealthProviderLocations().remove(healthProviderLocation);
		healthProviderLocation.setHealthProvider(null);

		return healthProviderLocation;
	}
	
	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setHealthProvider(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setHealthProvider(null);

		return appointment;
	}

}