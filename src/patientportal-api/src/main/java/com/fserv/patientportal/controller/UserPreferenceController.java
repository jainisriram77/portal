package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.UserPreferenceDto;
import com.fserv.patientportal.service.UserPreferenceService;

@Controller
@RequestMapping(value = "/userpreferences/")
public class UserPreferenceController extends BaseControllerImpl<Integer, UserPreferenceDto> {

  @Autowired(required = true)
  public UserPreferenceController(UserPreferenceService resourceService) {
    super(resourceService);
  }

}
