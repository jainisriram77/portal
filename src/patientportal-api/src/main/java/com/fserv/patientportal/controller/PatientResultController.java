package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.PatientResultDto;
import com.fserv.patientportal.service.PatientResultService;


@Controller
@RequestMapping(value = "/patientresult")
public class PatientResultController extends BaseControllerImpl<Integer, PatientResultDto> {


  @Autowired
  private PatientResultService patientresultservice;

  @Autowired(required = true)
  public PatientResultController(PatientResultService resourceService) {
    super(resourceService);
  }

  // @RequestMapping (value = "/{ptResultId}/{ptId}", method = RequestMethod.GET, produces =
  // "application/json")
  // public @ResponseBody PatientResultDto
  // findPatientResultByptresultIdAndpId(@PathVariable("patientId") int
  // patientId,@PathVariable("patientresultId") int resultId ){
  // PatientResultDto
  // patientResultDto=patientresultservice.findPatientResultByptresultIdAndpId(patientId, resultId);
  // return patientResultDto;
  // }
  // @RequestMapping (value = RequestMappingConstants.DELETE_PATIENTRESULT, method =
  // RequestMethod.DELETE, produces = "application/json")
  // public void deletePatientResult(@PathVariable("patientResultId") int patientResultId,
  // @PathVariable("patientId") int patientId){
  // patientresultservice.deletePatientResult(patientResultId, patientId);
  // }

  @RequestMapping(value = "/patientResult", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<PatientResultDto> findAllPatientResults(
      @PathVariable("patientId") int patientId) {
    List<PatientResultDto> patientResultDto = patientresultservice.findPatientResults(patientId);
    return patientResultDto;

  }
}
