package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.HealthProviderDto;
import com.fserv.patientportal.service.BaseService;
import com.fserv.patientportal.service.HealthProviderService;

@Controller
@RequestMapping(value = "/healthprovider")
public class HealthProviderController extends BaseControllerImpl<Integer, HealthProviderDto> {

	@Autowired(required = true)
	public HealthProviderController(HealthProviderService resourceService) {
		super(resourceService);
		// TODO Auto-generated constructor stub
	}

}
