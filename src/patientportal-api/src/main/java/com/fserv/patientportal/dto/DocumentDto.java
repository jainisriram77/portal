
package com.fserv.patientportal.dto;

import java.util.Date;

public class DocumentDto {

  private String id;
  private String documentName;
  private String documentType;
  private String documentDetail;
  private String documentS3URL;
  private Date creationDate;
  private Date modifiedDate;
  private String createdBy;
  private String source;
  private String documentURL;

  public String getId() {
    return id;
  }

  public void setId(String string) {
    this.id = string;
  }

  public String getDocumentName() {
    return documentName;
  }

  public void setDocumentName(String documentName) {
    this.documentName = documentName;
  }

  public String getDocumentType() {
    return documentType;
  }

  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }

  public String getDocumentDetail() {
    return documentDetail;
  }

  public void setDocumentDetail(String documentDetail) {
    this.documentDetail = documentDetail;
  }

  public String getDocumentS3URL() {
    return documentS3URL;
  }

  public void setDocumentS3URL(String documentS3URL) {
    this.documentS3URL = documentS3URL;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getDocumentURL() {
    return documentURL;
  }

  public void setDocumentURL(String documentURL) {
    this.documentURL = documentURL;
  }
}
