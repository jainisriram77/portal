package com.fserv.patientportal.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.fserv.patientportal.dto.SearchCriteriaAllergyDto;
import com.fserv.patientportal.model.PatientAllergy;

@Repository
@Transactional
public class PatientAllergySearchRepositoryImpl implements PatientAllergySearchRepository {

  @PersistenceContext
  private EntityManager entityMgr;

  @SuppressWarnings("unchecked")
  @Override
  public List<PatientAllergy> search(SearchCriteriaAllergyDto searchAllergyDto) {
    StringBuilder query = new StringBuilder();
    query.append("SELECT pa FROM PatientAllergy pa ");
    query.append("WHERE 1=1 ");
    if ((!StringUtils.isEmpty(searchAllergyDto.getAllergyDescription()))) {
      query.append(" AND pa.allergyDescription = :allergyDescription ");
    }

    if ((!StringUtils.isEmpty(searchAllergyDto.getPatientId()))) {
      query.append("AND pa.patient.id = :patientId");
    }

    Query query1 = entityMgr.createQuery(query.toString());
    //
    // if ((searchAllergyDto.getPatientAllergyDto().getPatientId()) != 0)
    // query1.setParameter("patientId", searchAllergyDto.getPatientAllergyDto().getPatientId());
    // if (!StringUtils.isEmpty(searchAllergyDto.getPatientAllergyDto().getAllergyDescription()))
    // query1.setParameter("allergyDescription",
    // searchAllergyDto.getPatientAllergyDto().getAllergyDescription());
    // if (!StringUtils.isEmpty(searchAllergyDto.getPatientAllergyDto().getAllergyRemarks()))
    // query1.setParameter("allergyRemarks",
    // searchAllergyDto.getPatientAllergyDto().getAllergyRemarks());
    List<PatientAllergy> resultList = query1.getResultList();
    return resultList;
  }
}
