package com.fserv.patientportal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.UserActivity;
import com.fserv.patientportal.model.UserActivityPK;

@Repository
@Transactional
public interface UserActivityRepository extends BaseRepository<UserActivity, UserActivityPK> {

  @Query("SELECT us FROM UserActivity us LEFT JOIN FETCH us.activity WHERE us.id.userId = :userId")
  public List<UserActivity> findUserActivities(@Param("userId") int userId);
}
