package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the patient_allergy database table.
 * 
 */
@Entity
@Table(name="patient_allergy", schema = "patientportal")
@NamedQuery(name="PatientAllergy.findAll", query="SELECT p FROM PatientAllergy p")
public class PatientAllergy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="allergy_description")
	private String allergyDescription;

	@Column(name="allergy_remarks")
	private String allergyRemarks;

	@Column(name="patient_id")
	private Integer patientId;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	//bi-directional many-to-one association to Patient
	
	public PatientAllergy() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAllergyDescription() {
		return this.allergyDescription;
	}

	public void setAllergyDescription(String allergyDescription) {
		this.allergyDescription = allergyDescription;
	}

	public String getAllergyRemarks() {
		return this.allergyRemarks;
	}

	public void setAllergyRemarks(String allergyRemarks) {
		this.allergyRemarks = allergyRemarks;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

  public Patient getPatient() {
    return patient;
  }

  public void setPatient(Patient patient) {
    this.patient = patient;
  }

	
}