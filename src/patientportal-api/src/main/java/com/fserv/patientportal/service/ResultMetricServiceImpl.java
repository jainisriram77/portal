package com.fserv.patientportal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.dto.ResultMetricDto;
import com.fserv.patientportal.dto.helper.ResultMetricDtoHelper;
import com.fserv.patientportal.model.ResultMetric;
import com.fserv.patientportal.repository.ResultMetricRepository;

@Service
@Transactional
public class ResultMetricServiceImpl extends BaseServiceImpl<ResultMetric, Integer, ResultMetricDto>
    implements ResultMetricService {

  @Autowired
  public ResultMetricServiceImpl(ResultMetricRepository repository,
      ResultMetricDtoHelper dtoHelper) {
    super(repository, dtoHelper);
  }

}
