package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_preference database table.
 * 
 */
@Entity
@Table(name="user_preference", schema = "patientportal")
@NamedQuery(name="UserPreference.findAll", query="SELECT u FROM UserPreference u")
public class UserPreference implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="patient_id")
	private Integer patientId;

	@Column(name="preference_name")
	private String preferenceName;

	@Column(name="preference_value")
	private String preferenceValue;

	@Column(name="user_id")
	private Integer userId;

	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
    private Patient patient;

	@ManyToOne
	@JoinColumn(name = "user_id",insertable = false, updatable = false)
    private User user;

	public UserPreference() {
    }

	public Patient getPatient() {
    return patient;
  }

  public void setPatient(Patient patient) {
    this.patient = patient;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public String getPreferenceName() {
		return this.preferenceName;
	}

	public void setPreferenceName(String preferenceName) {
		this.preferenceName = preferenceName;
	}

	public String getPreferenceValue() {
		return this.preferenceValue;
	}

	public void setPreferenceValue(String preferenceValue) {
		this.preferenceValue = preferenceValue;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}