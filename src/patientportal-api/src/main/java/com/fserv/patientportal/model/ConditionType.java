package com.fserv.patientportal.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ConditionType {
  SURGICAL, FAMILY_HISTORY, VACCINATIONS, DEFAULT;
  
  @JsonValue
  public String getValue() {
    return name();
  }

  @JsonCreator
  public static ConditionType create(String val) {
    ConditionType[] conditionTypes = ConditionType.values();
    for (ConditionType conType : conditionTypes) {

      if (conType.name().equalsIgnoreCase(val)) {
        return conType;
      }
    }
    return DEFAULT;
}}
