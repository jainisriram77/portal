package com.fserv.patientportal.repository;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fserv.patientportal.model.PatientResponsibileParty;


@Repository
@Transactional
public interface PatientResponsibilePartyRepository
    extends BaseRepository<PatientResponsibileParty, Integer> {



}
