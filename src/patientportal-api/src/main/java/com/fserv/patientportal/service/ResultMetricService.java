package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.ResultMetricDto;

public interface ResultMetricService extends BaseService<Integer, ResultMetricDto> {

}
