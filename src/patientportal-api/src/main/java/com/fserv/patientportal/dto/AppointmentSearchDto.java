package com.fserv.patientportal.dto;

import java.util.Date;

import com.fserv.patientportal.model.AppointmentStatus;
import com.fserv.patientportal.model.AppointmentType;

public class AppointmentSearchDto {

	private Date appointmentDate;

	// Change to enum
	private AppointmentStatus appointmentStatus;
	// Change to enum
	private AppointmentType appointmentType;

	private Integer patientId;
	private Integer physicianId;
	private Integer healthProviderId;

	public Date getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public AppointmentStatus getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public AppointmentType getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(AppointmentType appointmentType) {
		this.appointmentType = appointmentType;
	}
	
	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Integer getPhysicianId() {
		return physicianId;
	}

	public void setPhysicianId(Integer physicianId) {
		this.physicianId = physicianId;
	}

	public Integer getHealthProviderId() {
		return healthProviderId;
	}

	public void setHealthProviderId(Integer healthProviderId) {
		this.healthProviderId = healthProviderId;
	}
}
