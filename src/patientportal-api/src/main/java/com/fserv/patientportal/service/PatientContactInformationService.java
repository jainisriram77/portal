package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.PatientContactInformationDto;

public interface PatientContactInformationService 
	extends BaseService<Integer, PatientContactInformationDto>{

}
