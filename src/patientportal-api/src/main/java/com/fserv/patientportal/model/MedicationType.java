package com.fserv.patientportal.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum MedicationType {
  PRIMARY, SECONDARY, TERTIARY, OTHERPRIMARY, OTHERSECONDARY, OTHERTERTIARY, OTHER;

  @JsonValue
  public String getValue() {
    return name();
  }

  @JsonCreator
  public static MedicationType create(String val) {
    MedicationType[] mTypes = MedicationType.values();
    for (MedicationType mType : mTypes) {

      if (mType.name().equalsIgnoreCase(val)) {
        return mType;
      }
    }
    return OTHER;
  }
}
