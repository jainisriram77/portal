package com.fserv.patientportal.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the physician database table.
 * 
 */
@Entity
@Table(name = "physician", schema = "patientportal")
@NamedQuery(name="Physician.findAll", query="SELECT p FROM Physician p")
public class Physician implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="physician_id")
	private Integer physicianId;

	@Column(name="age")
	private Integer age;

	@Column(name="cell_phone_number")
	private Integer cellPhoneNumber;

	@Column(name="email")
	private String email;

	@Column(name="first_name")
	private String firstName;

	@Column(name="gender")
	private String gender;

	@Column(name="home_phone_number")
	private Integer homePhoneNumber;

	@Column(name="last_name")
	private String lastName;

	@Column(name="physician_npid")
	private Integer physicianNpid;

	@Column(name="specialization")
	private String specialization;

	@Column(name="work_phone_number")
	private Integer workPhoneNumber;

	//bi-directional many-to-one association to Appointment
    @OneToMany(mappedBy="physician")
    private List<Appointment> appointments;

    //bi-directional many-to-one association to PatientMedication
    @OneToMany(mappedBy="physician")
    private List<PatientMedication> patientMedications;
    
	public Physician() {
	}

	public Integer getPhysicianId() {
		return this.physicianId;
	}

	public void setPhysicianId(Integer physicianId) {
		this.physicianId = physicianId;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getCellPhoneNumber() {
		return this.cellPhoneNumber;
	}

	public void setCellPhoneNumber(Integer cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getHomePhoneNumber() {
		return this.homePhoneNumber;
	}

	public void setHomePhoneNumber(Integer homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getPhysicianNpid() {
		return this.physicianNpid;
	}

	public void setPhysicianNpid(Integer physicianNpid) {
		this.physicianNpid = physicianNpid;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public Integer getWorkPhoneNumber() {
		return this.workPhoneNumber;
	}

	public void setWorkPhoneNumber(Integer workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}

}