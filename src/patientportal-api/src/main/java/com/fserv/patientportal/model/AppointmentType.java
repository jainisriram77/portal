package com.fserv.patientportal.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AppointmentType {
  PRIMARY, SECONDARY, TERTIARY, OTHERPRIMARY, OTHERSECONDARY, OTHERTERTIARY, OTHER;

  @JsonValue
  public String getValue() {
    return name();
  }

  @JsonCreator
  public static AppointmentType create(String val) {
    AppointmentType[] aTypes = AppointmentType.values();
    for (AppointmentType aType : aTypes) {

      if (aType.name().equalsIgnoreCase(val)) {
        return aType;
      }
    }
    return OTHER;
  }

}
