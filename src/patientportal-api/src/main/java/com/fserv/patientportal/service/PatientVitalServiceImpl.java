package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientVitalDto;
import com.fserv.patientportal.dto.helper.PatientVitalDtoHelper;
import com.fserv.patientportal.model.PatientVital;
import com.fserv.patientportal.repository.PatientVitalRepository;

@Service
@Transactional
public class PatientVitalServiceImpl extends BaseServiceImpl<PatientVital, Integer, PatientVitalDto>
    implements PatientVitalService {


  @Autowired
  PatientVitalRepository patientVitalRepository;

  @Autowired
  PatientVitalDtoHelper patientVitaldtoHelper;

  @Autowired
  public PatientVitalServiceImpl(PatientVitalRepository patientVitalRepository,
      PatientVitalDtoHelper patientVitaldtoHelper) {
    super(patientVitalRepository, patientVitaldtoHelper);
  }

}
