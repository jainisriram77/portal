package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.ActivityDto;
import com.fserv.patientportal.dto.UserActivityDto;
import com.fserv.patientportal.dto.UserDto;
import com.fserv.patientportal.model.Activity;
import com.fserv.patientportal.model.User;
import com.fserv.patientportal.model.UserActivity;
import com.fserv.patientportal.model.UserActivityPK;

@Component
public class UserActivityDtoHelper implements DtoHelper<UserActivity, UserActivityDto> {

  @Override
  public List<UserActivityDto> buildDto(Iterable<UserActivity> models) {
    List<UserActivityDto> userActivityDtoList = new ArrayList<UserActivityDto>();
    for (UserActivity useractivity : models) {
      UserActivityDto dto = new UserActivityDto();
      dto.setUserId(useractivity.getId().getUserId());
      dto.setActivityId(useractivity.getId().getActivityId());
      dto.setIsUserRead(useractivity.getIsUserRead());
      UserDto usr = new UserDto();
      User user = useractivity.getUser();
      usr.setDob(user.getDob());
      usr.setEmail(user.getEmail());
      usr.setFirstName(user.getFirstName());
      usr.setLastName(user.getLastName());
      usr.setId(user.getId());
      usr.setPassword(user.getPassword());
      usr.setUserName(user.getUserName());
      usr.setUserType(user.getUserType());
      ActivityDto actv = new ActivityDto();
      Activity activity = useractivity.getActivity();
      actv.setActivityDate(activity.getActivityDate());
      actv.setActivityType(activity.getActivityType());
      actv.setFrom(activity.getFrom());
      actv.setId(activity.getId());
      actv.setMessage(activity.getMessage());
      actv.setTo(activity.getTo());
      actv.setSubject(activity.getSubject());
      userActivityDtoList.add(dto);
    }
    return userActivityDtoList;
  }

  @Override
  public UserActivityDto buildDto(UserActivity model) {
    return null;
  }

  @Override
  public UserActivity build(UserActivityDto dto) {
    UserActivity pActivity = new UserActivity();
    UserActivityPK pk = new UserActivityPK();
    pk.setUserId(dto.getUserId());
    pk.setActivityId(dto.getActivityId());

    pActivity.setId(pk);
    return pActivity;
  }

  @Override
  public List<UserActivity> build(Iterable<UserActivityDto> dtos) {
    return null;
  }
}
