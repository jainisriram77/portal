package com.fserv.patientportal.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the result_metric database table.
 * 
 */
@Entity
@Table(name="result_metric", schema = "patientportal")
@NamedQuery(name="ResultMetric.findAll", query="SELECT r FROM ResultMetric r")
public class ResultMetric implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="high_value")
	private String highValue;

	@Column(name="low_value")
	private String lowValue;

	@Column(name="normal_value")
	private String normalValue;

	@Column(name="result_metric_code")
	private String resultMetricCode;

	@Column(name="result_metric_name")
	private String resultMetricName;

	//bi-directional many-to-one association to PatientResult
	@OneToMany(mappedBy="resultMetric")
    private List<PatientResult> patientResult;

	
	public ResultMetric() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHighValue() {
		return this.highValue;
	}

	public void setHighValue(String highValue) {
		this.highValue = highValue;
	}

	public String getLowValue() {
		return this.lowValue;
	}

	public void setLowValue(String lowValue) {
		this.lowValue = lowValue;
	}

	public String getNormalValue() {
		return this.normalValue;
	}

	public void setNormalValue(String normalValue) {
		this.normalValue = normalValue;
	}

	public String getResultMetricCode() {
		return this.resultMetricCode;
	}

	public void setResultMetricCode(String resultMetricCode) {
		this.resultMetricCode = resultMetricCode;
	}

	public String getResultMetricName() {
		return this.resultMetricName;
	}

	public void setResultMetricName(String resultMetricName) {
		this.resultMetricName = resultMetricName;
	}

	public List<PatientResult> getPatientResult() {
      return this.patientResult;
  }

  public void setPatientResult(List<PatientResult> patientResult) {
      this.patientResult = patientResult;
  }

  public PatientResult addPatientResult(PatientResult patientResult) {
      getPatientResult().add(patientResult);
      patientResult.setResultMetric(this);

      return patientResult;
  }

  public PatientResult removePatientResult(PatientResult patientResult) {
      getPatientResult().remove(patientResult);
      patientResult.setResultMetric(null);

      return patientResult;
  }
}