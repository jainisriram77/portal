package com.fserv.patientportal.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum ActivityType {
  EMAIL, SMS, DEFAULT;

  @JsonValue
  public String getValue() {
    return name();
  }

  @JsonCreator
  public static ActivityType create(String val) {
    ActivityType[] activityTypes = ActivityType.values();
    for (ActivityType actType : activityTypes) {

      if (actType.name().equalsIgnoreCase(val)) {
        return actType;
      }
    }
    return DEFAULT;
  }
}
