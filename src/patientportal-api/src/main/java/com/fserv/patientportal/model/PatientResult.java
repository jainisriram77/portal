package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the patient_result database table.
 * 
 */
@Entity
@Table(name="patient_result", schema = "patientportal")
@NamedQuery(name="PatientResult.findAll", query="SELECT p FROM PatientResult p")
public class PatientResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="appointment_id")
	private Integer appointmentId;

	@Column(name="patient_id")
	private Integer patientId;

	@Temporal(TemporalType.DATE)
	@Column(name="result_date")
	private Date resultDate;

	@Column(name="result_details")
	private String resultDetails;

	@Column(name="result_metric_id")
	private Integer resultMetricId;

	@Column(name="result_provider")
	private String resultProvider;

	@Column(name="result_value")
	private String resultValue;

	//bi-directional many-to-one association to Appointment
	@ManyToOne
	@JoinColumn(name = "appointment_id",insertable = false, updatable = false)
	private Appointment appointment;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	//bi-directional many-to-one association to ResultMetric
	@ManyToOne
	@JoinColumn(name = "result_metric_id",insertable = false, updatable = false)
	private ResultMetric resultMetric;


	public PatientResult() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAppointmentId() {
		return this.appointmentId;
	}

	public void setAppointmentId(Integer appointmentId) {
		this.appointmentId = appointmentId;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Date getResultDate() {
		return this.resultDate;
	}

	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}

	public String getResultDetails() {
		return this.resultDetails;
	}

	public void setResultDetails(String resultDetails) {
		this.resultDetails = resultDetails;
	}

	public Integer getResultMetricId() {
		return this.resultMetricId;
	}

	public void setResultMetricId(Integer resultMetricId) {
		this.resultMetricId = resultMetricId;
	}

	public String getResultProvider() {
		return this.resultProvider;
	}

	public void setResultProvider(String resultProvider) {
		this.resultProvider = resultProvider;
	}

	public String getResultValue() {
		return this.resultValue;
	}

	public void setResultValue(String resultValue) {
		this.resultValue = resultValue;
	}

	public Appointment getAppointment() {
		return this.appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public ResultMetric getResultMetric() {
		return this.resultMetric;
	}

	public void setResultMetric(ResultMetric resultMetric) {
		this.resultMetric = resultMetric;
	}

}