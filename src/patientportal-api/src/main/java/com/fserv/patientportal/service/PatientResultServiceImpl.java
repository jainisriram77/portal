package com.fserv.patientportal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientResultDto;
import com.fserv.patientportal.dto.helper.PatientResultDtoHelper;
import com.fserv.patientportal.model.PatientResult;
import com.fserv.patientportal.repository.PatientResultRepository;

@Service
public class PatientResultServiceImpl extends
    BaseServiceImpl<PatientResult, Integer, PatientResultDto> implements PatientResultService {

  @Autowired
  PatientResultRepository patientResultRepository;

  @Autowired
  PatientResultDtoHelper patientResultDtoHelper;

  @Autowired
  public PatientResultServiceImpl(PatientResultRepository patientResultRepository,
		  PatientResultDtoHelper patientResultDtoHelper) {
	    super(patientResultRepository, patientResultDtoHelper);
	    }

  @Override
  public List<PatientResultDto> findPatientResults(int patientId) {
    List<PatientResult> patientResult = patientResultRepository.findPatientResults(patientId);

    return patientResultDtoHelper.buildDto(patientResult);
  }


  // @Override
  // public PatientResultDto findPatientResultByptresultIdAndpId(int patientId, int resultId) {
  // PatientResult patientResult = patientResultRepository.findPatientResult(patientId, resultId);
  // return patientResultDtoHelper.buildDto(patientResult);
  // }
  //
  // @Override
  // public void deletePatientResult(int patientResultId, int patientId) {
  // if (patientResultId != 0) {
  // patientResultRepository.delete(patientResultId);
  // } else if (patientId != 0) {
  // patientResultRepository.delete(patientId);
  // }

}


