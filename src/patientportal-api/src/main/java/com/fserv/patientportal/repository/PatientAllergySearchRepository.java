package com.fserv.patientportal.repository;

import java.util.List;

import com.fserv.patientportal.dto.SearchCriteriaAllergyDto;
import com.fserv.patientportal.model.PatientAllergy;

public interface PatientAllergySearchRepository {

  public List<PatientAllergy> search(SearchCriteriaAllergyDto searchAllergyDto);

}
