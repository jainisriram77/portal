package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.PatientVitalDto;
import com.fserv.patientportal.service.PatientVitalService;

@Controller
@RequestMapping(value = "/patientvitals")
public class PatientVitalController extends BaseControllerImpl<Integer, PatientVitalDto> {


  @Autowired(required = true)
  public PatientVitalController(PatientVitalService resourceService) {
    super(resourceService);
  }

  // @RequestMapping (value = "/usingpId/{patientId}", method = RequestMethod.GET, produces =
  // "application/json")
  // public @ResponseBody List<PatientVitalDto> findConditionsbypId(@PathVariable("patientId")int
  // patientId){
  // List<PatientConditionDto> patientConditionDto =
  // patientConditionService.findConditionsbypId(patientId);
  // return patientConditionDto;
  // }

}
