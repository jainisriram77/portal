package com.fserv.patientportal.dto;

public class UserActivityDto {

  private int userId;
  private UserDto userDto;

  private int activityId;
  private ActivityDto activityDto;

  private String isUserRead;

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public UserDto getUserDto() {
    return userDto;
  }

  public void setUserDto(UserDto userDto) {
    this.userDto = userDto;
  }

  public int getActivityId() {
    return activityId;
  }

  public void setActivityId(int activityId) {
    this.activityId = activityId;
  }

  public ActivityDto getActivityDto() {
    return activityDto;
  }

  public void setActivityDto(ActivityDto activityDto) {
    this.activityDto = activityDto;
  }

  public String getIsUserRead() {
    return isUserRead;
  }

  public void setIsUserRead(String isUserRead) {
    this.isUserRead = isUserRead;
  }

  



}
