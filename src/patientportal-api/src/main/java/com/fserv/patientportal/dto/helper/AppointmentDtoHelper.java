package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.AppointmentDto;
import com.fserv.patientportal.model.Appointment;

@Component
public class AppointmentDtoHelper implements DtoHelper<Appointment, AppointmentDto> {

  @Autowired
  private PatientDtoHelper ptDtoHelper;
  @Autowired
  private PhysicianDtoHelper phDtoHelper;
  @Autowired
  private HealthProviderDtoHelper hplDtoHelper;

  @Override
  public List<AppointmentDto> buildDto(Iterable<Appointment> models) {
    List<AppointmentDto> patientappointmentdtolist = new ArrayList<AppointmentDto>();
    for (Appointment appointment : models) {
      AppointmentDto dto = buildDto(appointment);
      patientappointmentdtolist.add(dto);
    }
    return patientappointmentdtolist;
  }

  @Override
  public AppointmentDto buildDto(Appointment model) {
    AppointmentDto dtore = null;
    if (model != null) {
      dtore = new AppointmentDto();
      dtore.setId(model.getId());
      dtore.setAppointmentDate(model.getAppointmentDate());
      dtore.setAppointmentReason(model.getAppointmentReason());
      dtore.setAppointmentStatus(model.getAppointmentStatus());
      dtore.setAppointmentTime(model.getAppointmentTime());
      dtore.setAppointmentType(model.getAppointmentType());

      dtore.setPatientDto(ptDtoHelper.buildDto(model.getPatient()));
      dtore.setPhysicianDto(phDtoHelper.buildDto(model.getPhysician()));
      dtore.setHealthProviderDto(hplDtoHelper.buildDto(model.getHealthProvider()));

      // dtoToReturn.setPatientReports(model.getPatientReports());
      // dtoToReturn.setPatientResults(model.getPatientResults());
      // dtoToReturn.setPatientVitals(model.getPatientVitals());
    }
    return dtore;
  }

  @Override
  public Appointment build(AppointmentDto dto) {
    Appointment model = null;
    if (dto != null) {
      model = new Appointment();
      model.setAppointmentDate(dto.getAppointmentDate());
      model.setAppointmentReason(dto.getAppointmentReason());
      model.setAppointmentStatus(dto.getAppointmentStatus());
      model.setAppointmentTime(dto.getAppointmentTime());
      model.setAppointmentType(dto.getAppointmentType());
      model.setId(dto.getId());

      model.setPatient(ptDtoHelper.build(dto.getPatientDto()));
      model.setPhysician(phDtoHelper.build(dto.getPhysicianDto()));
      model.setHealthProvider(hplDtoHelper.build(dto.getHealthProviderDto()));

      // model.setPatientReports(dto.getPatientReports());
      // model.setPatientResults(dto.getPatientResults());
      // model.setPatientVitals(dto.getPatientVitals());
    }
    return model;
  }

  @Override
  public List<Appointment> build(Iterable<AppointmentDto> dtos) {
    List<Appointment> modelList = new ArrayList<Appointment>();
    for (AppointmentDto pat : dtos) {
      Appointment model = build(pat);
      modelList.add(model);
    }
    return modelList;
  }

}
