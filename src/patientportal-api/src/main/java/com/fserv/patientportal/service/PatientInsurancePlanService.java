package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.PatientInsurancePlanDto;
import com.fserv.patientportal.dto.PatientInsuranceSearchCriteriaDto;

public interface PatientInsurancePlanService extends BaseService<Integer, PatientInsurancePlanDto> {

  List<PatientInsurancePlanDto> findPtInsurancePlans(int patientId);

  void deletePatientInsurPlan(int patientId);

  List<PatientInsurancePlanDto> searchPatientInsurplan(
      PatientInsuranceSearchCriteriaDto searchcriteria);

}
