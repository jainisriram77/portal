package com.fserv.patientportal.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AppointmentStatus {
  CONFIRMED, PENDING, CANCELLED, OTHER;

  @JsonValue
  public String getValue() {
    return name();
  }

  @JsonCreator
  public static AppointmentStatus create(String val) {
    AppointmentStatus[] aStatuses = AppointmentStatus.values();
    for (AppointmentStatus aStatus : aStatuses) {

      if (aStatus.name().equalsIgnoreCase(val)) {
        return aStatus;
      }
    }
    return OTHER;
  }

}
