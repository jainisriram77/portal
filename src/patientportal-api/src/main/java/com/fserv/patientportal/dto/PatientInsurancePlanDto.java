package com.fserv.patientportal.dto;

public class PatientInsurancePlanDto {
  private int Id;
  private int patientId;
  private PolicyInformationDto policyinformationDto;
  private int policyId;
  private String ptInsurancePlanStatus;

  public int getId() {
    return Id;
  }

  public void setId(int id) {
    Id = id;
  }

  public int getPatientId() {
    return patientId;
  }

  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }

  public PolicyInformationDto getPolicyinformationDto() {
    return policyinformationDto;
  }

  public void setPolicyinformationDto(PolicyInformationDto policyinformationDto) {
    this.policyinformationDto = policyinformationDto;
  }

  public int getPolicyId() {
    return policyId;
  }

  public void setPolicyId(int policyId) {
    this.policyId = policyId;
  }

  public String getPtInsurancePlanStatus() {
    return ptInsurancePlanStatus;
  }

  public void setPtInsurancePlanStatus(String ptInsurancePlanStatus) {
    this.ptInsurancePlanStatus = ptInsurancePlanStatus;
  }



}
