package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientMedicationDto;
import com.fserv.patientportal.model.Patient;
import com.fserv.patientportal.model.PatientMedication;
import com.fserv.patientportal.model.Physician;

@Component
public class PatientMedicationDtoHelper
    implements DtoHelper<PatientMedication, PatientMedicationDto> {

  @Override
  public List<PatientMedicationDto> buildDto(Iterable<PatientMedication> models) {
    List<PatientMedicationDto> patientMedicationDtoList = new ArrayList<PatientMedicationDto>();
    for (PatientMedication medication : models) {
      PatientMedicationDto dto = new PatientMedicationDto();
      dto.setMedicationDescription(medication.getMedicationDescription());
      dto.setMedicationDirections(medication.getMedicationDirections());
      dto.setMedicationEndDate(medication.getMedicationEndDate());
      dto.setMedicationName(medication.getMedicationName());
      dto.setMedicationStartDate(medication.getMedicationStartDate());
      dto.setMedicationStatus(medication.getMedicationStatus());
      dto.setMedicationType(medication.getMedicationType());
      dto.setPrescribedDate(medication.getPrescribedDate());
      Patient p = new Patient();
      p = (medication.getPatient());
      dto.setPatientId(p.getId());
      Physician ph = new Physician();
      ph = (medication.getPhysician());
      dto.setPhysicianId(ph.getPhysicianId());
      patientMedicationDtoList.add(dto);
    }
    return patientMedicationDtoList;
  }

  @Override
  public PatientMedicationDto buildDto(PatientMedication model) {
    PatientMedicationDto patientMedicationDto = null;
    if (model != null) {
      patientMedicationDto = new PatientMedicationDto();
      patientMedicationDto.setMedicationDescription(model.getMedicationDescription());
      patientMedicationDto.setMedicationDirections(model.getMedicationDirections());
      patientMedicationDto.setMedicationEndDate(model.getMedicationEndDate());
      patientMedicationDto.setMedicationName(model.getMedicationName());
      patientMedicationDto.setMedicationStartDate(model.getMedicationStartDate());
      patientMedicationDto.setMedicationStatus(model.getMedicationStatus());
      patientMedicationDto.setMedicationType(model.getMedicationType());
      patientMedicationDto.setPrescribedDate(model.getPrescribedDate());
      Patient p = new Patient();
      p = (model.getPatient());
      patientMedicationDto.setPatientId(p.getId());
      Physician ph = new Physician();
      ph = (model.getPhysician());
      patientMedicationDto.setPhysicianId(ph.getPhysicianId());
    }
    return patientMedicationDto;
  }

  @Override
  public PatientMedication build(PatientMedicationDto dto) {
    PatientMedication patientMedication = null;
    if (dto != null) {
      patientMedication = new PatientMedication();
      patientMedication.setMedicationDescription(dto.getMedicationDescription());
      patientMedication.setMedicationDirections(dto.getMedicationDirections());
      patientMedication.setMedicationEndDate(dto.getMedicationEndDate());
      patientMedication.setMedicationName(dto.getMedicationName());
      patientMedication.setMedicationStartDate(dto.getMedicationStartDate());
      patientMedication.setMedicationStatus(dto.getMedicationStatus());
      patientMedication.setMedicationType(dto.getMedicationType());
      patientMedication.setPrescribedDate(dto.getPrescribedDate());
      Patient p = new Patient();
      p.setId(dto.getPatientId());
      patientMedication.setPatient(p);
      Physician ph = new Physician();
      ph.setPhysicianId(dto.getPhysicianId());
      patientMedication.setPhysician(ph);
    }
    return patientMedication;
  }

  @Override
  public List<PatientMedication> build(Iterable<PatientMedicationDto> dtos) {
    // TODO Auto-generated method stub
    return null;
  }


}
