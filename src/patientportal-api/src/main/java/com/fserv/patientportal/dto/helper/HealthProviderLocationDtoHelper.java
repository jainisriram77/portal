package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.HealthProviderLocationDto;
import com.fserv.patientportal.model.HealthProviderLocation;

@Component
public class HealthProviderLocationDtoHelper implements
		DtoHelper<HealthProviderLocation, HealthProviderLocationDto> {

	@Autowired
	private HealthProviderDtoHelper hpDtoHelper;

	@Override
	public List<HealthProviderLocationDto> buildDto(
			Iterable<HealthProviderLocation> models) {
		List<HealthProviderLocationDto> hplList = new ArrayList<HealthProviderLocationDto>();
		for (HealthProviderLocation hpl : models) {
			HealthProviderLocationDto dto = buildDto(hpl);
			if (dto != null) {
				hplList.add(dto);
			}
		}
		return hplList;
	}

	@Override
	public HealthProviderLocationDto buildDto(HealthProviderLocation model) {
		HealthProviderLocationDto dto = null;
		if (model != null) {
			dto = new HealthProviderLocationDto();
			dto.setAddress1(model.getAddress1());
			dto.setAddress2(model.getAddress2());
			dto.setCity(model.getCity());
			dto.setCode(model.getCode());
			dto.setEmail(model.getEmail());
			dto.setHealthProviderDto(hpDtoHelper.buildDto(model					.getHealthProvider()));
			dto.setId(model.getId());
			dto.setIsPrimaryLocation(model.getIsPrimaryLocation());
			dto.setName(model.getName());
			dto.setOfficeNumber(model.getOfficeNumber());
			dto.setState(model.getState());
			dto.setZip(model.getZip());
		}
		return dto;
	}

	@Override
	public HealthProviderLocation build(HealthProviderLocationDto dto) {
		return null;
	}

	@Override
	public List<HealthProviderLocation> build(
			Iterable<HealthProviderLocationDto> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
