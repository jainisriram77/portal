package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the patient_responsibile_party database table.
 * 
 */
@Entity
@Table(name="patient_responsibile_party", schema = "patientportal")
@NamedQuery(name="PatientResponsibileParty.findAll", query="SELECT p FROM PatientResponsibileParty p")
public class PatientResponsibileParty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="patient_id")
	private Integer patientId;

	@Column(name="responsible_party_address")
	private String responsibilePartyAddress;

	@Column(name="responsible_party_email")
	private String responsibilePartyEmail;

	@Column(name="responsible_party_name")
	private String responsibilePartyName;

	@Column(name="responsible_party_number")
	private Integer responsibilePartyNumber;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	public PatientResponsibileParty() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public String getResponsibilePartyAddress() {
		return this.responsibilePartyAddress;
	}

	public void setResponsibilePartyAddress(String responsibilePartyAddress) {
		this.responsibilePartyAddress = responsibilePartyAddress;
	}

	public String getResponsibilePartyEmail() {
		return this.responsibilePartyEmail;
	}

	public void setResponsibilePartyEmail(String responsibilePartyEmail) {
		this.responsibilePartyEmail = responsibilePartyEmail;
	}

	public String getResponsibilePartyName() {
		return this.responsibilePartyName;
	}

	public void setResponsibilePartyName(String responsibilePartyName) {
		this.responsibilePartyName = responsibilePartyName;
	}

	public Integer getResponsibilePartyNumber() {
		return this.responsibilePartyNumber;
	}

	public void setResponsibilePartyNumber(Integer responsibilePartyNumber) {
		this.responsibilePartyNumber = responsibilePartyNumber;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}