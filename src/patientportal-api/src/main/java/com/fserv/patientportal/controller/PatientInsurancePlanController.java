package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.PatientInsurancePlanDto;
import com.fserv.patientportal.dto.PatientInsuranceSearchCriteriaDto;
import com.fserv.patientportal.service.PatientInsurancePlanService;

@Controller
@RequestMapping(value = "/patientinsurance")
public class PatientInsurancePlanController
    extends BaseControllerImpl<Integer, PatientInsurancePlanDto> {

  @Autowired
  private PatientInsurancePlanService patientInsurPlanservice;

  @Autowired(required = true)
  public PatientInsurancePlanController(PatientInsurancePlanService resourceService) {
    super(resourceService);
  }

  @RequestMapping(value = "/patientinsurance/{patientId}", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<PatientInsurancePlanDto> findPtInsurplanByPtId(
      @PathVariable("patientId") int patientId) {
    List<PatientInsurancePlanDto> patientinsuplanDto =
        patientInsurPlanservice.findPtInsurancePlans(patientId);
    return patientinsuplanDto;
  }

  @RequestMapping(value = "/patientinsurance/{patientId}", method = RequestMethod.DELETE,
      produces = "application/json")
  public void deletePatientInsurPlan(@PathVariable("patientId") int patientId) {
    patientInsurPlanservice.deletePatientInsurPlan(patientId);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/search/", produces = "application/json")
  public @ResponseBody List<PatientInsurancePlanDto> searchpatientInsurplanbyptId(
      @RequestBody PatientInsuranceSearchCriteriaDto searchcriteria) {
    List<PatientInsurancePlanDto> ptInsurplanDto =
        patientInsurPlanservice.searchPatientInsurplan(searchcriteria);
    return ptInsurplanDto;
  }
}
