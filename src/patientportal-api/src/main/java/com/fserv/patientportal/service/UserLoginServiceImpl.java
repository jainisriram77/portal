package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.LoginRequestDto;
import com.fserv.patientportal.dto.UserDto;
import com.fserv.patientportal.dto.helper.UserDtoHelper;
import com.fserv.patientportal.model.User;
import com.fserv.patientportal.repository.UserRepository;

@Service
@Transactional

public class UserLoginServiceImpl implements UserLoginService {

	@Autowired
	UserRepository userRepo;
	@Autowired
	UserDtoHelper userDtoHelper;
	
	@Override
	public UserDto login(LoginRequestDto loginDto) {
		User user = userRepo.findByUsername(loginDto.getUserName(),loginDto.getPassword());
		if ((user !=null)&&(user.getPassword().contentEquals(loginDto.getPassword()))) {
		      return userDtoHelper.buildDto(user);		    }		
		
		else
		return null;
	}

}
