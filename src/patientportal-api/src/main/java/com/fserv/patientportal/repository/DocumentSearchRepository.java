package com.fserv.patientportal.repository;

import java.util.List;

import com.fserv.patientportal.dto.DocumentSearchCriteriaDto;
import com.fserv.patientportal.model.Document;

public interface DocumentSearchRepository {

  public List<Document> searchptDocu(DocumentSearchCriteriaDto searchcriteria);


}
