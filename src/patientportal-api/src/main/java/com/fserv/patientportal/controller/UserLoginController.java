package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.constants.Constants;
import com.fserv.patientportal.constants.RequestMappingConstants;
import com.fserv.patientportal.dto.LoginRequestDto;
import com.fserv.patientportal.dto.UserDto;
import com.fserv.patientportal.service.UserLoginService;

@Controller
public class UserLoginController {
	@Autowired
	  UserLoginService userLoginService;

	  @RequestMapping(value = RequestMappingConstants.USER_LOGIN, method = RequestMethod.POST,
	      consumes = Constants.JSON_FORMAT, produces = Constants.JSON_FORMAT)
	  public @ResponseBody ResponseEntity<UserDto> login(@RequestBody LoginRequestDto loginDto) {
	    UserDto userDto = userLoginService.login(loginDto);
	    if ((null == userDto))
	      return new ResponseEntity<UserDto>(HttpStatus.NON_AUTHORITATIVE_INFORMATION);

	    return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);

	  }

}
