package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fserv.patientportal.dto.PatientAllergyDto;
import com.fserv.patientportal.dto.SearchCriteriaAllergyDto;
import com.fserv.patientportal.service.PatientAllergyService;

@RestController
@Controller
@RequestMapping(value = "/patientallergies/")
public class PatientAllergyController extends BaseControllerImpl<Integer, PatientAllergyDto> {

  @Autowired
  private PatientAllergyService patientAllergyService;


  @Autowired(required = true)
  public PatientAllergyController(PatientAllergyService resourceService) {
    super(resourceService);
  }


  @RequestMapping(value = "/getbyptId/{patientId}", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<PatientAllergyDto> findAllergiesByPtId(
      @PathVariable("patientId") int patientId) {
    List<PatientAllergyDto> patientAllergyDto = patientAllergyService.findAllergiesbypId(patientId);
    return patientAllergyDto;
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST, produces = "application/json")
  public @ResponseBody List<PatientAllergyDto> findAllergiesDynamic(
      @RequestBody SearchCriteriaAllergyDto searchDto) {
    List<PatientAllergyDto> pAllergyDto = patientAllergyService.search(searchDto);
    return pAllergyDto;
  }


}
