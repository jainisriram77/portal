package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.PatientResponsibilePartyDto;
import com.fserv.patientportal.service.PatientResponsibilePartyService;

@Controller
@RequestMapping("/patientresponsibileparty")
public class PatientResponsibilePartyController
    extends BaseControllerImpl<Integer, PatientResponsibilePartyDto> {

  @Autowired(required = true)
  public PatientResponsibilePartyController(PatientResponsibilePartyService resourceService) {
    super(resourceService);

  }
}
