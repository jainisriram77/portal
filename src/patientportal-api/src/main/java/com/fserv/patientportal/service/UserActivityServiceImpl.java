package com.fserv.patientportal.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.UserActivityDto;
import com.fserv.patientportal.dto.UserActivitySearchCriteriaDto;
import com.fserv.patientportal.dto.helper.ActivityDtoHelper;
import com.fserv.patientportal.dto.helper.UserActivityDtoHelper;
import com.fserv.patientportal.model.UserActivity;
import com.fserv.patientportal.model.UserActivityPK;
import com.fserv.patientportal.repository.UserActivityRepository;
import com.fserv.patientportal.repository.UserActivitySearchRepository;

@Service
@Transactional
public class UserActivityServiceImpl extends
    BaseServiceImpl<UserActivity, UserActivityPK, UserActivityDto> implements UserActivityService {

  @Autowired
  private ActivityDtoHelper activityHelper;

  @Autowired
  private UserActivityDtoHelper usrActivityHelper;

  @Autowired
  private UserActivitySearchRepository usrActvSearchRepo;

  @Autowired
  public UserActivityServiceImpl(UserActivityRepository repository,
      UserActivityDtoHelper dtoHelper) {
    super(repository, dtoHelper);
  }

  @Override
  public List<UserActivityDto> findUserActivities(int userId) {
    List<UserActivity> usrActivitys =((UserActivityRepository) repository).findUserActivities(userId);
    return usrActivityHelper.buildDto(usrActivitys);
  }

  @Override
  public List<UserActivityDto> searchUserActivities(UserActivitySearchCriteriaDto searchcriteria) {
    List<UserActivity> activities = usrActvSearchRepo.searchUserActivities(searchcriteria);
    return usrActivityHelper.buildDto(activities);
  }

}
