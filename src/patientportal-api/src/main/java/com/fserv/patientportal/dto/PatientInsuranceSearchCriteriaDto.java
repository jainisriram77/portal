package com.fserv.patientportal.dto;

import java.util.Date;

public class PatientInsuranceSearchCriteriaDto {

  private int patientId;
  private String insuranceProviderName;
  private String insuranceProviderAddress;
  private String insuranceproviderZip;
  private String insuranceProviderState;
  private String policyHolderName;
  private String policyHolderRelation;
  private Date policyHolderDob;
  private String policyHolderAddress;
  private String policyHolderZip;
  private String policyHolderState;
  private String policyNumber;
  private String groupNumber;
  private int policyholderSsn;
  private Date policyeffectiveDate;
  private Date policyTerminationDate;


  public int getPatientId() {
    return patientId;
  }

  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }

  public String getInsuranceProviderName() {
    return insuranceProviderName;
  }

  public void setInsuranceProviderName(String insuranceProviderName) {
    this.insuranceProviderName = insuranceProviderName;
  }

  public String getInsuranceProviderAddress() {
    return insuranceProviderAddress;
  }

  public void setInsuranceProviderAddress(String insuranceProviderAddress) {
    this.insuranceProviderAddress = insuranceProviderAddress;
  }

  public String getInsuranceproviderZip() {
    return insuranceproviderZip;
  }

  public void setInsuranceproviderZip(String insuranceproviderZip) {
    this.insuranceproviderZip = insuranceproviderZip;
  }

  public String getInsuranceProviderState() {
    return insuranceProviderState;
  }

  public void setInsuranceProviderState(String insuranceProviderState) {
    this.insuranceProviderState = insuranceProviderState;
  }

  public String getPolicyHolderName() {
    return policyHolderName;
  }

  public void setPolicyHolderName(String policyHolderName) {
    this.policyHolderName = policyHolderName;
  }

  public String getPolicyHolderRelation() {
    return policyHolderRelation;
  }

  public void setPolicyHolderRelation(String policyHolderRelation) {
    this.policyHolderRelation = policyHolderRelation;
  }

  public Date getPolicyHolderDob() {
    return policyHolderDob;
  }

  public void setPolicyHolderDob(Date policyHolderDob) {
    this.policyHolderDob = policyHolderDob;
  }

  public String getPolicyHolderAddress() {
    return policyHolderAddress;
  }

  public void setPolicyHolderAddress(String policyHolderAddress) {
    this.policyHolderAddress = policyHolderAddress;
  }

  public String getPolicyHolderZip() {
    return policyHolderZip;
  }

  public void setPolicyHolderZip(String policyHolderZip) {
    this.policyHolderZip = policyHolderZip;
  }

  public String getPolicyHolderState() {
    return policyHolderState;
  }

  public void setPolicyHolderState(String policyHolderState) {
    this.policyHolderState = policyHolderState;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public String getGroupNumber() {
    return groupNumber;
  }

  public void setGroupNumber(String groupNumber) {
    this.groupNumber = groupNumber;
  }

  public int getPolicyholderSsn() {
    return policyholderSsn;
  }

  public void setPolicyholderSsn(int policyholderSsn) {
    this.policyholderSsn = policyholderSsn;
  }

  public Date getPolicyeffectiveDate() {
    return policyeffectiveDate;
  }

  public void setPolicyeffectiveDate(Date policyeffectiveDate) {
    this.policyeffectiveDate = policyeffectiveDate;
  }

  public Date getPolicyTerminationDate() {
    return policyTerminationDate;
  }

  public void setPolicyTerminationDate(Date policyTerminationDate) {
    this.policyTerminationDate = policyTerminationDate;
  }

  public String getPolicyStatus() {
    return policyStatus;
  }

  public void setPolicyStatus(String policyStatus) {
    this.policyStatus = policyStatus;
  }

  public int getCoPay() {
    return coPay;
  }

  public void setCoPay(int coPay) {
    this.coPay = coPay;
  }

  private String policyStatus;
  private int coPay;
}
