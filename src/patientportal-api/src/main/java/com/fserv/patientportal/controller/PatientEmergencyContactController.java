package com.fserv.patientportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fserv.patientportal.dto.PatientEmergencyContactDto;
import com.fserv.patientportal.service.PatientEmergencyContactService;

@Controller
@RequestMapping(value = "/patientemergencycontact")
public class PatientEmergencyContactController
    extends BaseControllerImpl<Integer, PatientEmergencyContactDto> {

  @Autowired(required = true)
  public PatientEmergencyContactController(PatientEmergencyContactService resourceService) {
    super(resourceService);
  }

}
