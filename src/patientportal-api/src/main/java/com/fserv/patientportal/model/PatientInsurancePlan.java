package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the patient_insurance_plan database table.
 * 
 */
@Entity
@Table(name="patient_insurance_plan", schema = "patientportal")
@NamedQuery(name="PatientInsurancePlan.findAll", query="SELECT p FROM PatientInsurancePlan p")
public class PatientInsurancePlan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="patient_id")
	private Integer patientId;

	@Column(name="policy_id")
	private Integer policyId;

	@Column(name="pt_insurance_plan_status")
	private String ptInsurancePlanStatus;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	//bi-directional many-to-one association to PolicyInformation
	@ManyToOne
	@JoinColumn(name = "policy_id",insertable = false, updatable = false)
	private PolicyInformation policyInformation;

	public PatientInsurancePlan() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Integer getPolicyId() {
		return this.policyId;
	}

	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}

	public String getPtInsurancePlanStatus() {
		return this.ptInsurancePlanStatus;
	}

	public void setPtInsurancePlanStatus(String ptInsurancePlanStatus) {
		this.ptInsurancePlanStatus = ptInsurancePlanStatus;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public PolicyInformation getPolicyInformation() {
		return this.policyInformation;
	}

	public void setPolicyInformation(PolicyInformation policyInformation) {
		this.policyInformation = policyInformation;
	}

}