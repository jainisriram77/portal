package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.HealthProviderDto;
import com.fserv.patientportal.model.HealthProvider;

@Component
public class HealthProviderDtoHelper implements DtoHelper<HealthProvider, HealthProviderDto> {

  @Override
  public List<HealthProviderDto> buildDto(Iterable<HealthProvider> models) {
    List<HealthProviderDto> hpDtoList = new ArrayList<HealthProviderDto>();
    for (HealthProvider hp : models) {
      HealthProviderDto dto = new HealthProviderDto();
      dto.setHealthProviderId(hp.getHealthProviderId());
      dto.setName(hp.getName());
      hpDtoList.add(dto);
    }
    return hpDtoList;
  }

  @Override
  public HealthProviderDto buildDto(HealthProvider model) {
    HealthProviderDto dto = null;
    if (model != null) {
      dto = new HealthProviderDto();
      dto.setHealthProviderId(model.getHealthProviderId());
      dto.setName(model.getName());
    }
    return dto;
  }

  @Override
  public HealthProvider build(HealthProviderDto dto) {
    HealthProvider model = null;
    if (dto != null) {
      model = new HealthProvider();
      model.setHealthProviderId(dto.getHealthProviderId());
      model.setName(dto.getName());
    }
    return model;
  }

  @Override
  public List<HealthProvider> build(Iterable<HealthProviderDto> dtos) {
    List<HealthProvider> modelList = new ArrayList<HealthProvider>();
    for (HealthProviderDto hp : dtos) {
      HealthProvider model = new HealthProvider();
      model.setHealthProviderId(hp.getHealthProviderId());
      model.setName(hp.getName());
      modelList.add(model);
    }

    return modelList;
  }

}
