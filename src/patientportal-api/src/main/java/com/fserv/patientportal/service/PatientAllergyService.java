package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.PatientAllergyDto;
import com.fserv.patientportal.dto.SearchCriteriaAllergyDto;

public interface PatientAllergyService extends BaseService<Integer, PatientAllergyDto> {


  public List<PatientAllergyDto> findAllergiesbypId(int patientId);

  public List<PatientAllergyDto> search(SearchCriteriaAllergyDto searchAllergyDto);
}
