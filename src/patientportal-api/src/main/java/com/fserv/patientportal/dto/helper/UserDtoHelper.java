package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.UserDto;
import com.fserv.patientportal.model.User;

@Component
public class UserDtoHelper implements DtoHelper<User, UserDto> {

  @Override
  public List<UserDto> buildDto(Iterable<User> models) {
    List<UserDto> userDtoList = null;
    if (models != null) {
      userDtoList = new ArrayList<UserDto>();
      for (User usr : models) {
        userDtoList.add(buildDto(usr));
      }
    }
    return userDtoList;
  }

  @Override
  public UserDto buildDto(User model) {
    UserDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new UserDto();
      dtoToReturn.setDob(model.getDob());
      dtoToReturn.setEmail(model.getEmail());
      dtoToReturn.setFirstName(model.getFirstName());
      dtoToReturn.setLastName(model.getLastName());
      dtoToReturn.setPassword(model.getPassword());
      dtoToReturn.setId(model.getId());
      dtoToReturn.setUserName(model.getUserName());
      dtoToReturn.setUserType(model.getUserType());
    }
    return dtoToReturn;
  }

  @Override
  public User build(UserDto dto) {
    User model = null;
    if (dto != null) {
      model = new User();
      model.setDob(dto.getDob());
      model.setEmail(dto.getEmail());
      model.setFirstName(dto.getFirstName());
      model.setLastName(dto.getLastName());
      model.setPassword(dto.getPassword());
      model.setId(dto.getId());
      model.setUserName(dto.getUserName());
      model.setUserType(dto.getUserType());
    }
    return model;
  }

  @Override
  public List<User> build(Iterable<UserDto> dtos) {
    List<User> models = null;
    if (dtos != null) {
      models = new ArrayList<User>();
      for (UserDto usrdto : dtos) {
        models.add(build(usrdto));
      }
    }
    return models;
  }
}
