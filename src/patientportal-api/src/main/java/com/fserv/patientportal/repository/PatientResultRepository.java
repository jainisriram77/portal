package com.fserv.patientportal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.PatientResult;

@Repository
@Transactional
public interface PatientResultRepository extends BaseRepository<PatientResult, Integer> {

  @Query("SELECT p FROM PatientResult p WHERE p.patient.id=:patientId")
  public List<PatientResult> findPatientResults(@Param("patientId") Integer patientId);

}
