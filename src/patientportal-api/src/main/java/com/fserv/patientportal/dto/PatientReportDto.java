package com.fserv.patientportal.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PatientReportDto {

  private int patientId;
  @JsonFormat(pattern = "yyyy-mm-dd")
  private Date capturedDate;
  private String reportType;
  private int AppointmentId;
  private AppointmentDto appointmentDto;
  private String documentId;
  private DocumentDto documentDto;
  public int getPatientId() {
    return patientId;
  }
  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }
  public Date getCapturedDate() {
    return capturedDate;
  }
  public void setCapturedDate(Date capturedDate) {
    this.capturedDate = capturedDate;
  }
  public String getReportType() {
    return reportType;
  }
  public void setReportType(String reportType) {
    this.reportType = reportType;
  }
  public int getAppointmentId() {
    return AppointmentId;
  }
  public void setAppointmentId(int appointmentId) {
    AppointmentId = appointmentId;
  }
  public AppointmentDto getAppointmentDto() {
    return appointmentDto;
  }
  public void setAppointmentDto(AppointmentDto appointmentDto) {
    this.appointmentDto = appointmentDto;
  }
  public String getDocumentId() {
    return documentId;
  }
  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }
  public DocumentDto getDocumentDto() {
    return documentDto;
  }
  public void setDocumentDto(DocumentDto documentDto) {
    this.documentDto = documentDto;
  }

  
  
}
