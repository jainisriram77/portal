package com.fserv.patientportal.repository;

import java.util.List;

import com.fserv.patientportal.dto.AppointmentSearchDto;
import com.fserv.patientportal.model.Appointment;

public interface AppointmentSearchRepo {
  public List<Appointment> searchCriteria(AppointmentSearchDto dto);

}
