package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the patient_emergency_contact database table.
 * 
 */
@Entity
@Table(name="patient_emergency_contact", schema = "patientportal")
@NamedQuery(name="PatientEmergencyContact.findAll", query="SELECT p FROM PatientEmergencyContact p")
public class PatientEmergencyContact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="emergency_address")
	private String emergencyAddress;

	@Column(name="emergency_contact_name")
	private String emergencyContactName;

	@Column(name="emergency_contact_number")
	private Integer emergencyContactNumber;

	@Column(name="emergency_contact_relation")
	private String emergencyContactRelation;

	@Column(name="emergency_email")
	private String emergencyEmail;

	@Column(name="patient_id")
	private Integer patientId;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	public PatientEmergencyContact() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmergencyAddress() {
		return this.emergencyAddress;
	}

	public void setEmergencyAddress(String emergencyAddress) {
		this.emergencyAddress = emergencyAddress;
	}

	public String getEmergencyContactName() {
		return this.emergencyContactName;
	}

	public void setEmergencyContactName(String emergencyContactName) {
		this.emergencyContactName = emergencyContactName;
	}

	public Integer getEmergencyContactNumber() {
		return this.emergencyContactNumber;
	}

	public void setEmergencyContactNumber(Integer emergencyContactNumber) {
		this.emergencyContactNumber = emergencyContactNumber;
	}

	public String getEmergencyContactRelation() {
		return this.emergencyContactRelation;
	}

	public void setEmergencyContactRelation(String emergencyContactRelation) {
		this.emergencyContactRelation = emergencyContactRelation;
	}

	public String getEmergencyEmail() {
		return this.emergencyEmail;
	}

	public void setEmergencyEmail(String emergencyEmail) {
		this.emergencyEmail = emergencyEmail;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

}