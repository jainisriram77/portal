package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the policy_information database table.
 * 
 */
@Entity
@Table(name="policy_information", schema = "patientportal")
@NamedQuery(name="PolicyInformation.findAll", query="SELECT p FROM PolicyInformation p")
public class PolicyInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="co_pay")
	private Integer coPay;

	@Column(name="employer_address")
	private String employerAddress;

	@Column(name="employer_city")
	private String employerCity;

	@Column(name="employer_country")
	private String employerCountry;

	@Column(name="employer_name")
	private String employerName;

	@Column(name="employer_state")
	private String employerState;

	@Column(name="employer_zip")
	private Integer employerZip;

	@Column(name="group_number")
	private String groupNumber;

	@Column(name="insurance_provider_address")
	private String insuranceProviderAddress;

	@Column(name="insurance_provider_city")
	private String insuranceProviderCity;

	@Column(name="insurance_provider_country")
	private String insuranceProviderCountry;

	@Column(name="\"INSURANCE_PROVIDER_NAME\"")
	private String insuranceProviderName;

	@Column(name="insurance_provider_state")
	private String insuranceProviderState;

	@Column(name="insurance_provider_zip")
	private Integer insuranceProviderZip;

	@Temporal(TemporalType.DATE)
	@Column(name="policy_effective_date")
	private Date policyEffectiveDate;

	@Column(name="policy_holder_address")
	private String policyHolderAddress;

	@Temporal(TemporalType.DATE)
	@Column(name="policy_holder_dob")
	private Date policyHolderDob;

	@Column(name="policy_holder_name")
	private String policyHolderName;

	@Column(name="policy_holder_relation")
	private String policyHolderRelation;

	@Column(name="policy_holder_ssn")
	private Integer policyHolderSsn;

	@Column(name="policy_holder_state")
	private String policyHolderState;

	@Column(name="policy_holder_zip")
	private Integer policyHolderZip;

	@Column(name="policy_number")
	private String policyNumber;

	@Column(name="policy_status")
	private String policyStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="policy_termination_date")
	private Date policyTerminationDate;

	@OneToMany(mappedBy="policyInformation")
    private List<PatientInsurancePlan> patientInsurancePlans;
	
	public PolicyInformation() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCoPay() {
		return this.coPay;
	}

	public void setCoPay(Integer coPay) {
		this.coPay = coPay;
	}

	public String getEmployerAddress() {
		return this.employerAddress;
	}

	public void setEmployerAddress(String employerAddress) {
		this.employerAddress = employerAddress;
	}

	public String getEmployerCity() {
		return this.employerCity;
	}

	public void setEmployerCity(String employerCity) {
		this.employerCity = employerCity;
	}

	public String getEmployerCountry() {
		return this.employerCountry;
	}

	public void setEmployerCountry(String employerCountry) {
		this.employerCountry = employerCountry;
	}

	public String getEmployerName() {
		return this.employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getEmployerState() {
		return this.employerState;
	}

	public void setEmployerState(String employerState) {
		this.employerState = employerState;
	}

	public Integer getEmployerZip() {
		return this.employerZip;
	}

	public void setEmployerZip(Integer employerZip) {
		this.employerZip = employerZip;
	}

	public String getGroupNumber() {
		return this.groupNumber;
	}

	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getInsuranceProviderAddress() {
		return this.insuranceProviderAddress;
	}

	public void setInsuranceProviderAddress(String insuranceProviderAddress) {
		this.insuranceProviderAddress = insuranceProviderAddress;
	}

	public String getInsuranceProviderCity() {
		return this.insuranceProviderCity;
	}

	public void setInsuranceProviderCity(String insuranceProviderCity) {
		this.insuranceProviderCity = insuranceProviderCity;
	}

	public String getInsuranceProviderCountry() {
		return this.insuranceProviderCountry;
	}

	public void setInsuranceProviderCountry(String insuranceProviderCountry) {
		this.insuranceProviderCountry = insuranceProviderCountry;
	}

	public String getInsuranceProviderName() {
		return this.insuranceProviderName;
	}

	public void setInsuranceProviderName(String insuranceProviderName) {
		this.insuranceProviderName = insuranceProviderName;
	}

	public String getInsuranceProviderState() {
		return this.insuranceProviderState;
	}

	public void setInsuranceProviderState(String insuranceProviderState) {
		this.insuranceProviderState = insuranceProviderState;
	}

	public Integer getInsuranceProviderZip() {
		return this.insuranceProviderZip;
	}

	public void setInsuranceProviderZip(Integer insuranceProviderZip) {
		this.insuranceProviderZip = insuranceProviderZip;
	}

	public Date getPolicyEffectiveDate() {
		return this.policyEffectiveDate;
	}

	public void setPolicyEffectiveDate(Date policyEffectiveDate) {
		this.policyEffectiveDate = policyEffectiveDate;
	}

	public String getPolicyHolderAddress() {
		return this.policyHolderAddress;
	}

	public void setPolicyHolderAddress(String policyHolderAddress) {
		this.policyHolderAddress = policyHolderAddress;
	}

	public Date getPolicyHolderDob() {
		return this.policyHolderDob;
	}

	public void setPolicyHolderDob(Date policyHolderDob) {
		this.policyHolderDob = policyHolderDob;
	}

	public String getPolicyHolderName() {
		return this.policyHolderName;
	}

	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}

	public String getPolicyHolderRelation() {
		return this.policyHolderRelation;
	}

	public void setPolicyHolderRelation(String policyHolderRelation) {
		this.policyHolderRelation = policyHolderRelation;
	}

	public Integer getPolicyHolderSsn() {
		return this.policyHolderSsn;
	}

	public void setPolicyHolderSsn(Integer policyHolderSsn) {
		this.policyHolderSsn = policyHolderSsn;
	}

	public String getPolicyHolderState() {
		return this.policyHolderState;
	}

	public void setPolicyHolderState(String policyHolderState) {
		this.policyHolderState = policyHolderState;
	}

	public Integer getPolicyHolderZip() {
		return this.policyHolderZip;
	}

	public void setPolicyHolderZip(Integer policyHolderZip) {
		this.policyHolderZip = policyHolderZip;
	}

	public String getPolicyNumber() {
		return this.policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public String getPolicyStatus() {
		return this.policyStatus;
	}

	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}

	public Date getPolicyTerminationDate() {
		return this.policyTerminationDate;
	}

	public void setPolicyTerminationDate(Date policyTerminationDate) {
		this.policyTerminationDate = policyTerminationDate;
	}

	public List<PatientInsurancePlan> getPatientInsurancePlans() {
      return this.patientInsurancePlans;
  }

  public void setPatientInsurancePlans(List<PatientInsurancePlan> patientInsurancePlans) {
      this.patientInsurancePlans = patientInsurancePlans;
  }

  public PatientInsurancePlan addPatientInsurancePlan(PatientInsurancePlan patientInsurancePlan) {
      getPatientInsurancePlans().add(patientInsurancePlan);
      patientInsurancePlan.setPolicyInformation(this);

      return patientInsurancePlan;
  }

  public PatientInsurancePlan removePatientInsurancePlan(PatientInsurancePlan patientInsurancePlan) {
      getPatientInsurancePlans().remove(patientInsurancePlan);
      patientInsurancePlan.setPolicyInformation(null);

      return patientInsurancePlan;
  }
}