package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PhysicianDto;
import com.fserv.patientportal.dto.helper.DtoHelper;
import com.fserv.patientportal.model.Physician;
import com.fserv.patientportal.repository.BaseRepository;

@Service
@Transactional
public class PhysicianServiceImpl extends BaseServiceImpl<Physician, Integer, PhysicianDto>
    implements PhysicianService {

  @Autowired
  public PhysicianServiceImpl(BaseRepository<Physician, Integer> repository,

      DtoHelper<Physician, PhysicianDto> dtoHelper) {
    super(repository, dtoHelper);
  }
}
