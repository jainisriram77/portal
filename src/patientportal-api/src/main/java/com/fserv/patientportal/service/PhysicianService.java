package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.PhysicianDto;


public interface PhysicianService extends BaseService<Integer, PhysicianDto> {


}
