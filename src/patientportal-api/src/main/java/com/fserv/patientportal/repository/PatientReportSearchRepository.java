package com.fserv.patientportal.repository;

import java.util.List;

import com.fserv.patientportal.dto.PatientReportSearchCriteriaDto;
import com.fserv.patientportal.model.PatientReport;

public interface PatientReportSearchRepository {

  public List<PatientReport> searchptReport(PatientReportSearchCriteriaDto searchcriteria);

}
