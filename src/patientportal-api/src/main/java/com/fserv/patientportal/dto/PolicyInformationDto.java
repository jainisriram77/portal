package com.fserv.patientportal.dto;

import java.util.Date;

public class PolicyInformationDto {

  private int Id;
  private String policyNumber;
  private String groupNumber;
  private int policyholderSsn;
  private Date policyeffectiveDate;
  private Date policyTerminationDate;
  private String policyStatus;
  private int coPay;
  private String insuranceProviderName;
  private String insuranceProviderAddress;
  private Integer insuranceproviderZip;
  private String insuranceProviderState;
  private String policyHolderName;
  private String policyHolderRelation;
  private Date policyHolderDob;
  private String policyHolderAddress;
  private Integer policyHolderZip;
  private String policyHolderState;
  private String employerName;
  private String employerAddress;
  private Integer employerZip;
  private String employerState;
  private String employerCity;
  private String employerCountry;
  private String insuranceProviderCity;
  private String insuranceProviderCountry;


  public int getId() {
    return Id;
  }

  public void setId(int id) {
    Id = id;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public String getGroupNumber() {
    return groupNumber;
  }

  public void setGroupNumber(String groupNumber) {
    this.groupNumber = groupNumber;
  }

  public int getPolicyholderSsn() {
    return policyholderSsn;
  }

  public void setPolicyholderSsn(int policyholderSsn) {
    this.policyholderSsn = policyholderSsn;
  }

  public Date getPolicyeffectiveDate() {
    return policyeffectiveDate;
  }

  public void setPolicyeffectiveDate(Date policyeffectiveDate) {
    this.policyeffectiveDate = policyeffectiveDate;
  }

  public Date getPolicyTerminationDate() {
    return policyTerminationDate;
  }

  public void setPolicyTerminationDate(Date policyTerminationDate) {
    this.policyTerminationDate = policyTerminationDate;
  }

  public String getPolicyStatus() {
    return policyStatus;
  }

  public void setPolicyStatus(String policyStatus) {
    this.policyStatus = policyStatus;
  }

  public int getCoPay() {
    return coPay;
  }

  public void setCoPay(int coPay) {
    this.coPay = coPay;
  }

  public String getInsuranceProviderName() {
    return insuranceProviderName;
  }

  public void setInsuranceProviderName(String insuranceProviderName) {
    this.insuranceProviderName = insuranceProviderName;
  }

  public String getInsuranceProviderAddress() {
    return insuranceProviderAddress;
  }

  public void setInsuranceProviderAddress(String insuranceProviderAddress) {
    this.insuranceProviderAddress = insuranceProviderAddress;
  }

  public String getInsuranceProviderState() {
    return insuranceProviderState;
  }

  public void setInsuranceProviderState(String insuranceProviderState) {
    this.insuranceProviderState = insuranceProviderState;
  }

  public String getPolicyHolderName() {
    return policyHolderName;
  }

  public void setPolicyHolderName(String policyHolderName) {
    this.policyHolderName = policyHolderName;
  }

  public String getPolicyHolderRelation() {
    return policyHolderRelation;
  }

  public void setPolicyHolderRelation(String policyHolderRelation) {
    this.policyHolderRelation = policyHolderRelation;
  }

  public Date getPolicyHolderDob() {
    return policyHolderDob;
  }

  public void setPolicyHolderDob(Date policyHolderDob) {
    this.policyHolderDob = policyHolderDob;
  }

  public String getPolicyHolderAddress() {
    return policyHolderAddress;
  }

  public void setPolicyHolderAddress(String policyHolderAddress) {
    this.policyHolderAddress = policyHolderAddress;
  }

  public String getPolicyHolderState() {
    return policyHolderState;
  }

  public void setPolicyHolderState(String policyHolderState) {
    this.policyHolderState = policyHolderState;
  }

  public String getEmployerName() {
    return employerName;
  }

  public void setEmployerName(String employerName) {
    this.employerName = employerName;
  }

  public String getEmployerAddress() {
    return employerAddress;
  }

  public void setEmployerAddress(String employerAddress) {
    this.employerAddress = employerAddress;
  }

  public Integer getInsuranceproviderZip() {
    return insuranceproviderZip;
  }

  public void setInsuranceproviderZip(Integer insuranceproviderZip) {
    this.insuranceproviderZip = insuranceproviderZip;
  }

  public Integer getPolicyHolderZip() {
    return policyHolderZip;
  }

  public void setPolicyHolderZip(Integer policyHolderZip) {
    this.policyHolderZip = policyHolderZip;
  }

  public Integer getEmployerZip() {
    return employerZip;
  }

  public void setEmployerZip(Integer employerZip) {
    this.employerZip = employerZip;
  }

  public String getEmployerState() {
    return employerState;
  }

  public void setEmployerState(String employerState) {
    this.employerState = employerState;
  }

  public String getEmployerCity() {
    return employerCity;
  }

  public void setEmployerCity(String employerCity) {
    this.employerCity = employerCity;
  }

  public String getEmployerCountry() {
    return employerCountry;
  }

  public void setEmployerCountry(String employerCountry) {
    this.employerCountry = employerCountry;
  }

  public String getInsuranceProviderCity() {
    return insuranceProviderCity;
  }

  public void setInsuranceProviderCity(String insuranceProviderCity) {
    this.insuranceProviderCity = insuranceProviderCity;
  }

  public String getInsuranceProviderCountry() {
    return insuranceProviderCountry;
  }

  public void setInsuranceProviderCountry(String insuranceProviderCountry) {
    this.insuranceProviderCountry = insuranceProviderCountry;
  }

}
