package com.fserv.patientportal.dto.helper;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;


import com.fserv.patientportal.dto.ComposeEmailDto;

import com.fserv.patientportal.model.ComposeEmail;
@Component
public class ComposeEmailDtoHelper implements DtoHelper<ComposeEmail, ComposeEmailDto>  {

	@Override
	  public List<ComposeEmailDto> buildDto(Iterable<ComposeEmail> models) {

	    List<ComposeEmailDto> composeEmailList = new ArrayList<ComposeEmailDto>();
	    for (ComposeEmail composeEmail : models) {
	    	 ComposeEmailDto dto = new  ComposeEmailDto();
	     
//	      dto.setActivityType(ActivityType.create(activity.getActivityType()));
	      dto.setToEmail(composeEmail.getToEmail());
	      dto.setSubject(composeEmail.getSubject());
	      dto.setBody(composeEmail.getBody());
	      composeEmailList.add(dto);
	    }
	    return composeEmailList;
	  }
	
	@Override
	  public ComposeEmailDto buildDto(ComposeEmail model) {
		ComposeEmailDto dto = new ComposeEmailDto();
	   
	    dto.setToEmail(model.getToEmail());
	    dto.setSubject(model.getSubject());
	    dto.setBody(model.getBody());
	
	    return dto;
	  }

	@Override
	  public ComposeEmail build(ComposeEmailDto dto) {
		ComposeEmail composeEmail = new ComposeEmail();
		
		composeEmail.setToEmail(dto.getToEmail());
		composeEmail.setSubject(dto.getSubject());
		composeEmail.setBody(dto.getBody());
	    
	    return composeEmail;
	  }

	
	@Override
	  public List<ComposeEmail> build(Iterable<ComposeEmailDto> dtos) {

	    return null;
	  }
	
	
	
	
	
}
