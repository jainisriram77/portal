package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.PatientDto;

public interface PatientService extends BaseService<Integer, PatientDto> {

}
