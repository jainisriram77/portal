package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.PatientReportDto;
import com.fserv.patientportal.dto.PatientReportSearchCriteriaDto;
import com.fserv.patientportal.model.PatientReportPK;

public interface PatientReportService extends BaseService<PatientReportPK, PatientReportDto> {

  List<PatientReportDto> searchPatientReport(PatientReportSearchCriteriaDto searchcriteria);

  List<PatientReportDto> findPtreportByDocsIdAndpId(int patientId, String ptDocsId);

  // PatientReportDto findPtreportByptresultIdAndpId(int patientId, int ptDocsId);

}
