package com.fserv.patientportal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fserv.patientportal.model.PatientAllergy;

@Repository
@Transactional
public interface PatientAllergyRepository extends BaseRepository<PatientAllergy, Integer> {

  @Query("SELECT pa FROM PatientAllergy pa WHERE pa.patient.id = :patientId")
  public List<PatientAllergy> findAllergiesbypId(@Param("patientId") int patientId);

}
