package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientContactInformationDto;
import com.fserv.patientportal.dto.helper.DtoHelper;
import com.fserv.patientportal.model.PatientContactInformation;
import com.fserv.patientportal.repository.BaseRepository;

@Service
@Transactional
public class PatientContactInformationServiceImpl 
	extends BaseServiceImpl<PatientContactInformation, Integer, PatientContactInformationDto>
	implements PatientContactInformationService{

	@Autowired
	public PatientContactInformationServiceImpl(
			BaseRepository<PatientContactInformation, Integer> repository,
			DtoHelper<PatientContactInformation, PatientContactInformationDto> dtoHelper) {
		super(repository, dtoHelper);
		// TODO Auto-generated constructor stub
	}

}
