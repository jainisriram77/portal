package com.fserv.patientportal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;





import com.fserv.patientportal.model.ComposeEmail;
@Repository
@Transactional
public interface ComposeEmailRepository  extends BaseRepository<ComposeEmail, Integer>  {
	 
	
	
	@Query("select c from ComposeEmail c where c.id = :Id")
	public List<ComposeEmail>  findComposeEmailbyId(@Param("Id") int Id);

}
