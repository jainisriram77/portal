package com.fserv.patientportal.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.fserv.patientportal.dto.PatientReportSearchCriteriaDto;
import com.fserv.patientportal.model.PatientReport;

@Transactional
@Repository
public class PatientReportSearchRepoImpl implements PatientReportSearchRepository {

  @PersistenceContext
  private EntityManager entityMgr;

  @SuppressWarnings("unchecked")
  
  @Override
  public List<PatientReport> searchptReport(PatientReportSearchCriteriaDto searchcriteria) {
  
    StringBuilder query = new StringBuilder();

    query.append("SELECT ptrep FROM PatientReport AS ptrep ");
    query.append("LEFT JOIN FETCH ptrep.document AS docs ");
    query.append("LEFT JOIN FETCH ptrep.patient AS pt ");
    query.append("LEFT JOIN FETCH ptrep.appointment AS app ");
    query.append("WHERE 1=1 ");
    // query.append("AND ac.id = usac.activity.id ");
    // query.append("AND usr.id = usac.user.id ");

    if (searchcriteria.getAppointmentId() !=0) {
      query.append("AND app.id=:appointmentid ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getDocumentDate()))) {
      query.append("AND docs.documentDate=:documentDate ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getDocumentId()))) {
      query.append("AND docs.id=:documentId ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getDocumentType()))) {
      query.append("AND docs.documentType=:documentType ");
    }
    if (searchcriteria.getPatientId()!=0) {
      query.append("AND pt.id=:patientId ");
    }
    // String s = "SELECT usac.activity FROM UserActivity AS usac LEFT JOIN
    // usac.activity AS ac LEFT JOIN usac.user AS usr WHERE 1=1 ";

    Query query1 = entityMgr.createQuery(query.toString());

    if (searchcriteria.getAppointmentId() != 0)
      query1.setParameter("appointmentid", searchcriteria.getAppointmentId());

    if ((!StringUtils.isEmpty(searchcriteria.getDocumentDate())))
      query1.setParameter("documentDate", searchcriteria.getDocumentDate());

    if ((!StringUtils.isEmpty(searchcriteria.getDocumentType())))
      query1.setParameter("documentType", searchcriteria.getDocumentType());

    if ((!StringUtils.isEmpty(searchcriteria.getDocumentId())))
      query1.setParameter("documentId", searchcriteria.getDocumentId());

    if (searchcriteria.getPatientId() != 0)
      query1.setParameter("patientId", searchcriteria.getPatientId());

    List<PatientReport> ptReport = query1.getResultList();
    return ptReport;
}
}
