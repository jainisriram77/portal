package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.DocumentDto;
import com.fserv.patientportal.model.Document;

@Component
public class DocumentDtoHelper implements DtoHelper<Document, DocumentDto> {

  @Override
  public List<DocumentDto> buildDto(Iterable<Document> models) {
    List<DocumentDto> ptDocsDtoList = new ArrayList<DocumentDto>();
    for (Document document : models) {
      DocumentDto dto = new DocumentDto();
      dto.setCreatedBy(document.getCreatedBy());
      dto.setCreationDate(document.getCreationDate());
      dto.setDocumentDetail(document.getDocumentDetail());
      dto.setDocumentName(document.getDocumentName());
      dto.setDocumentS3URL(document.getDocumentS3Url());
      dto.setDocumentType(document.getDocumentType());
      dto.setDocumentURL(document.getDocumentUrl());
      dto.setId(document.getId());
      dto.setModifiedDate(document.getModifiedDate());
      dto.setSource(document.getSource());
      ptDocsDtoList.add(dto);
    }
    return ptDocsDtoList;
  }

  @Override
  public DocumentDto buildDto(Document model) {
    DocumentDto docu = new DocumentDto();
    docu.setCreatedBy(model.getCreatedBy());
    docu.setCreationDate(model.getCreationDate());
    docu.setDocumentDetail(model.getDocumentDetail());
    docu.setDocumentName(model.getDocumentName());
    docu.setDocumentS3URL(model.getDocumentS3Url());
    docu.setDocumentType(model.getDocumentType());
    docu.setDocumentURL(model.getDocumentUrl());
    docu.setId(model.getId());
    docu.setModifiedDate(model.getModifiedDate());
    docu.setSource(model.getSource());
    return docu;
  }

  @Override
  public Document build(DocumentDto dto) {
    Document docs = new Document();
    docs.setCreatedBy(dto.getCreatedBy());
    docs.setCreationDate(dto.getCreationDate());
    docs.setDocumentDetail(dto.getDocumentDetail());
    docs.setDocumentName(dto.getDocumentName());
    docs.setDocumentS3Url(dto.getDocumentS3URL());
    docs.setDocumentType(dto.getDocumentType());
    docs.setDocumentUrl(dto.getDocumentURL());
    docs.setId(dto.getId());
    docs.setModifiedDate(dto.getModifiedDate());
    docs.setSource(dto.getSource());
    return docs;
  }

  @Override
  public List<Document> build(Iterable<DocumentDto> dtos) {
    // TODO Auto-generated method stub
    return null;
  }

}
