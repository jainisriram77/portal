package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientVitalDto;
import com.fserv.patientportal.model.Patient;
import com.fserv.patientportal.model.PatientVital;

@Component
public class PatientVitalDtoHelper implements DtoHelper<PatientVital, PatientVitalDto> {
	
	@Autowired
	PatientDtoHelper phelper;
	
	@Autowired
	AppointmentDtoHelper ahelper;
	
  @Override
  public List<PatientVitalDto> buildDto(Iterable<PatientVital> models) {
    List<PatientVitalDto> userDtoList = null;
    if (models != null) {
      userDtoList = new ArrayList<PatientVitalDto>();
      for (PatientVital usr : models) {
        userDtoList.add(buildDto(usr));
      }
    }
    return userDtoList;
  }

  @Override
  public PatientVitalDto buildDto(PatientVital model) {
    PatientVitalDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new PatientVitalDto();
      dtoToReturn.setId(model.getId());
      dtoToReturn.setAppointmentDto(ahelper.buildDto(model.getAppointment()));
      dtoToReturn.setPatientDto(phelper.buildDto(model.getPatient()));
      dtoToReturn.setVitalDate(model.getVitalDate());
      dtoToReturn.setVitalName(model.getVitalName());
      dtoToReturn.setVitalValue(model.getVitalValue());
      dtoToReturn.setSource(model.getSource());
    }
    return dtoToReturn;
  }

  @Override
  public PatientVital build(PatientVitalDto dto) {
    PatientVital model = null;
    if (dto != null) {
      model = new PatientVital();
      model.setId(dto.getId());
      model.setAppointment(ahelper.build(dto.getAppointmentDto()));
      model.setSource(dto.getSource());
      model.setVitalDate(dto.getVitalDate());
      model.setVitalName(dto.getVitalName());
      model.setVitalValue(dto.getVitalValue());
      model.setPatient(phelper.build(dto.getPatientDto()));
    }
    return model;
  }

  @Override
  public List<PatientVital> build(Iterable<PatientVitalDto> dtos) {
    List<PatientVital> models = null;
    if (dtos != null) {
      models = new ArrayList<PatientVital>();
      for (PatientVitalDto usrdto : dtos) {
        models.add(build(usrdto));
      }
    }
    return models;
  }


}
