package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fserv.patientportal.dto.HealthProviderDto;
import com.fserv.patientportal.dto.helper.HealthProviderDtoHelper;
import com.fserv.patientportal.model.HealthProvider;
import com.fserv.patientportal.repository.HealthProviderRepository;

@Service
@Transactional
public class HealthProviderServiceImpl extends BaseServiceImpl<HealthProvider, Integer, HealthProviderDto>
implements HealthProviderService{

	@Autowired
	public HealthProviderServiceImpl(HealthProviderRepository healthproviderrepository,
			HealthProviderDtoHelper dtoHelper) {
		super(healthproviderrepository, dtoHelper);
		// TODO Auto-generated constructor stub
	}

}
