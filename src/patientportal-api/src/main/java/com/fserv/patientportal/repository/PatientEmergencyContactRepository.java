package com.fserv.patientportal.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.PatientEmergencyContact;

@Repository
@Transactional

public interface PatientEmergencyContactRepository
    extends BaseRepository<PatientEmergencyContact, Integer> {

}
