package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the user_activity database table.
 * 
 */
@Embeddable
public class UserActivityPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="activity_id", insertable=false, updatable=false)
	private Integer activityId;

	@Column(name="user_id")
	private Integer userId;
//
//	@ManyToOne
//    @JoinColumns({
//        })
//    private Activity activity;
//	
//	@ManyToOne
//    @JoinColumns({
//        })
//    private User user;
//	
	public UserActivityPK() {
	}
	public Integer getActivityId() {
		return this.activityId;
	}
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}
	public Integer getUserId() {
		return this.userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
//	public Activity getActivity() {
//      return this.activity;
//  }
//
//  public void setActivity(Activity activity) {
//      this.activity = activity;
//  }
//  
//  public User getUser(){
//    return this.user;
//  }
//
//  public void setUser(User user){
//    this.user = user;
//  }
  
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserActivityPK)) {
			return false;
		}
		UserActivityPK castOther = (UserActivityPK)other;
		return 
			this.activityId.equals(castOther.activityId)
			&& this.userId.equals(castOther.userId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.activityId.hashCode();
		hash = hash * prime + this.userId.hashCode();
		
		return hash;
	}
}