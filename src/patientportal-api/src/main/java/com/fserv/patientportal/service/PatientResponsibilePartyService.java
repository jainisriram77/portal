package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.PatientResponsibilePartyDto;
import com.fserv.patientportal.service.BaseService;



public interface PatientResponsibilePartyService
    extends BaseService<Integer, PatientResponsibilePartyDto> {

}
