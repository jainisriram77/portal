package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the patient_medication database table.
 * 
 */
@Entity
@Table(name="patient_medication", schema = "patientportal")
@NamedQuery(name="PatientMedication.findAll", query="SELECT p FROM PatientMedication p")
public class PatientMedication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="medication_description")
	private String medicationDescription;

	@Column(name="medication_directions")
	private String medicationDirections;

	@Temporal(TemporalType.DATE)
	@Column(name="medication_end_date")
	private Date medicationEndDate;

	@Column(name="medication_name")
	private String medicationName;

	@Temporal(TemporalType.DATE)
	@Column(name="medication_start_date")
	private Date medicationStartDate;

	@Column(name="medication_status")
	private String medicationStatus;

	@Column(name="medication_type")
	@Enumerated(EnumType.STRING)
	private MedicationType medicationType;

	@Column(name="patient_id")
	private Integer patientId;

	@Temporal(TemporalType.DATE)
	@Column(name="prescribed_date")
	private Date prescribedDate;

	@Column(name="prescribed_physician_id")
	private Integer prescribedPhysicianId;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id",insertable = false, updatable = false)
	private Patient patient;

	//bi-directional many-to-one association to Physician
	@ManyToOne
	@JoinColumn(name = "prescribed_physician_id",insertable = false, updatable = false)
	private Physician physician;

	public PatientMedication() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMedicationDescription() {
		return this.medicationDescription;
	}

	public void setMedicationDescription(String medicationDescription) {
		this.medicationDescription = medicationDescription;
	}

	public String getMedicationDirections() {
		return this.medicationDirections;
	}

	public void setMedicationDirections(String medicationDirections) {
		this.medicationDirections = medicationDirections;
	}

	public Date getMedicationEndDate() {
		return this.medicationEndDate;
	}

	public void setMedicationEndDate(Date medicationEndDate) {
		this.medicationEndDate = medicationEndDate;
	}

	public String getMedicationName() {
		return this.medicationName;
	}

	public void setMedicationName(String medicationName) {
		this.medicationName = medicationName;
	}

	public Date getMedicationStartDate() {
		return this.medicationStartDate;
	}

	public void setMedicationStartDate(Date medicationStartDate) {
		this.medicationStartDate = medicationStartDate;
	}

	public String getMedicationStatus() {
		return this.medicationStatus;
	}

	public void setMedicationStatus(String medicationStatus) {
		this.medicationStatus = medicationStatus;
	}

	public MedicationType getMedicationType() {
    return medicationType;
  }

  public void setMedicationType(MedicationType medicationType) {
    this.medicationType = medicationType;
  }

  public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Date getPrescribedDate() {
		return this.prescribedDate;
	}

	public void setPrescribedDate(Date prescribedDate) {
		this.prescribedDate = prescribedDate;
	}

	public Integer getPrescribedPhysicianId() {
		return this.prescribedPhysicianId;
	}

	public void setPrescribedPhysicianId(Integer prescribedPhysicianId) {
		this.prescribedPhysicianId = prescribedPhysicianId;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Physician getPhysician() {
		return this.physician;
	}

	public void setPhysician(Physician physician) {
		this.physician = physician;
	}

}