package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.ComposeEmailDto;




public interface ComposeEmailService extends BaseService<Integer, ComposeEmailDto>  {

	 public List<ComposeEmailDto> findComposeEmailbyId(int composeEmailId);
	
}
