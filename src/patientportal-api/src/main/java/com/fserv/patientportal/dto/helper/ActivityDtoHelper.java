 package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.ActivityDto;
import com.fserv.patientportal.model.Activity;

@Component
public class ActivityDtoHelper implements DtoHelper<Activity, ActivityDto> {

  @Override
  public List<ActivityDto> buildDto(Iterable<Activity> models) {

    List<ActivityDto> ActivityDtoList = new ArrayList<ActivityDto>();
    for (Activity activity : models) {
      ActivityDto dto = new ActivityDto();
      dto.setActivityDate(activity.getActivityDate());
//      dto.setActivityType(ActivityType.create(activity.getActivityType()));
      dto.setFrom(activity.getFrom());
      dto.setId(activity.getId());
      dto.setMessage(activity.getMessage());
      dto.setTo(activity.getTo());
      ActivityDtoList.add(dto);
    }
    return ActivityDtoList;
  }

  @Override
  public ActivityDto buildDto(Activity model) {
    ActivityDto dto = new ActivityDto();
    dto.setActivityDate(model.getActivityDate());
    dto.setActivityType(model.getActivityType());
    dto.setFrom(model.getFrom());
    dto.setId(model.getId());
    dto.setMessage(model.getMessage());
    dto.setTo(model.getTo());
    return dto;
  }

  @Override
  public Activity build(ActivityDto dto) {
    Activity activityy = new Activity();
    activityy.setActivityDate(dto.getActivityDate());
    activityy.setActivityType(dto.getActivityType());
    activityy.setFrom(dto.getFrom());
    activityy.setId(dto.getId());
    activityy.setMessage(dto.getMessage());
    activityy.setTo(dto.getTo());
    return activityy;
  }

  @Override
  public List<Activity> build(Iterable<ActivityDto> dtos) {

    return null;
  }

}
