package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the patient database table.
 * 
 */
@Entity
@Table(name="patient", schema = "patientportal")
@NamedQuery(name="Patient.findAll", query="SELECT p FROM Patient p")
public class Patient implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="dob")
	private Date dob;

	@Column(name="first_name")
	private String firstName;

	@Column(name="gender")
	private String gender;

	@Column(name="last_name")
	private String lastName;

	@Column(name="mrn")
	private String mrn;

	@Column(name="phone_number")
	private Integer phoneNumber;

	@Column(name="user_id")
	private Integer userId;

	//bi-directional OneToMany association to Appointment
	@OneToMany(mappedBy="patient")
	private List<Appointment> appointments;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "user_id",insertable = false, updatable = false)
	private User user;

	//bi-directional OneToMany association to PatientAllergy
	@OneToMany(mappedBy="patient")
	private List<PatientAllergy> patientAllergies;

	//bi-directional OneToMany association to PatientCondition
	@OneToMany(mappedBy="patient")
	private List<PatientCondition> patientConditions;

	//bi-directional OneToMany association to PatientContactInformation
	@OneToMany(mappedBy="patient")
	private List<PatientContactInformation> patientContactInformations;

	//bi-directional OneToMany association to PatientEmergencyContact
	@OneToMany(mappedBy="patient")
	private List<PatientEmergencyContact> patientEmergencyContact;

	//bi-directional OneToMany association to PatientInsurancePlan
	@OneToMany(mappedBy="patient")
	private List<PatientInsurancePlan> patientInsurancePlans;

	//bi-directional OneToMany association to PatientMedication
	@OneToMany(mappedBy="patient")
	private List<PatientMedication> patientMedications;

	//bi-directional OneToMany association to PatientReport
	@OneToMany(mappedBy="patient")
	private List<PatientReport> patientReports;

	//bi-directional OneToMany association to PatientResponsibileParty
	@OneToMany(mappedBy="patient")
	private List<PatientResponsibileParty> patientResponsibileParties;

	//bi-directional OneToMany association to PatientResult
	@OneToMany(mappedBy="patient")
	private List<PatientResult> patientResults;

	//bi-directional OneToMany association to PatientVital
	@OneToMany(mappedBy="patient")
	private List<PatientVital> patientVitals;

	//bi-directional OneToMany association to UserPreference
	@OneToMany(mappedBy="patient")
	private List<UserPreference> userPreferences;

	public Patient() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMrn() {
		return this.mrn;
	}

	public void setMrn(String mrn) {
		this.mrn = mrn;
	}

	public Integer getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setPatient(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setPatient(null);

		return appointment;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<PatientAllergy> getPatientAllergies() {
		return this.patientAllergies;
	}

	public void setPatientAllergies(List<PatientAllergy> patientAllergies) {
		this.patientAllergies = patientAllergies;
	}

	public PatientAllergy addPatientAllergies(PatientAllergy patientAllergies) {
		getPatientAllergies().add(patientAllergies);
		patientAllergies.setPatient(this);

		return patientAllergies;
	}

	public PatientAllergy removePatientAllergies(PatientAllergy patientAllergies) {
		getPatientAllergies().remove(patientAllergies);
		patientAllergies.setPatient(null);

		return patientAllergies;
	}

	public List<PatientCondition> getPatientConditions() {
		return this.patientConditions;
	}

	public void setPatientConditions(List<PatientCondition> patientConditions) {
		this.patientConditions = patientConditions;
	}

	public PatientCondition addPatientCondition(PatientCondition patientCondition) {
		getPatientConditions().add(patientCondition);
		patientCondition.setPatient(this);

		return patientCondition;
	}

	public PatientCondition removePatientCondition(PatientCondition patientCondition) {
		getPatientConditions().remove(patientCondition);
		patientCondition.setPatient(null);

		return patientCondition;
	}

	public List<PatientContactInformation> getPatientContactInformations() {
		return this.patientContactInformations;
	}

	public void setPatientContactInformations(List<PatientContactInformation> patientContactInformations) {
		this.patientContactInformations = patientContactInformations;
	}

	public PatientContactInformation addPatientContactInformation(PatientContactInformation patientContactInformation) {
		getPatientContactInformations().add(patientContactInformation);
		patientContactInformation.setPatient(this);

		return patientContactInformation;
	}

	public PatientContactInformation removePatientContactInformation(PatientContactInformation patientContactInformation) {
		getPatientContactInformations().remove(patientContactInformation);
		patientContactInformation.setPatient(null);

		return patientContactInformation;
	}

	public List<PatientEmergencyContact> getPatientEmergencyContact() {
		return this.patientEmergencyContact;
	}

	public void setPatientEmergencyContact(List<PatientEmergencyContact> patientEmergencyContact) {
		this.patientEmergencyContact = patientEmergencyContact;
	}

	public PatientEmergencyContact addPatientEmergencyContact(PatientEmergencyContact patientEmergencyContact) {
		getPatientEmergencyContact().add(patientEmergencyContact);
		patientEmergencyContact.setPatient(this);

		return patientEmergencyContact;
	}

	public PatientEmergencyContact removePatientEmergencyContact(PatientEmergencyContact patientEmergencyContact) {
		getPatientEmergencyContact().remove(patientEmergencyContact);
		patientEmergencyContact.setPatient(null);

		return patientEmergencyContact;
	}

	public List<PatientInsurancePlan> getPatientInsurancePlans() {
		return this.patientInsurancePlans;
	}

	public void setPatientInsurancePlans(List<PatientInsurancePlan> patientInsurancePlans) {
		this.patientInsurancePlans = patientInsurancePlans;
	}

	public PatientInsurancePlan addPatientInsurancePlan(PatientInsurancePlan patientInsurancePlan) {
		getPatientInsurancePlans().add(patientInsurancePlan);
		patientInsurancePlan.setPatient(this);

		return patientInsurancePlan;
	}

	public PatientInsurancePlan removePatientInsurancePlan(PatientInsurancePlan patientInsurancePlan) {
		getPatientInsurancePlans().remove(patientInsurancePlan);
		patientInsurancePlan.setPatient(null);

		return patientInsurancePlan;
	}

	public List<PatientMedication> getPatientMedications() {
		return this.patientMedications;
	}

	public void setPatientMedications(List<PatientMedication> patientMedications) {
		this.patientMedications = patientMedications;
	}

	public PatientMedication addPatientMedication(PatientMedication patientMedication) {
		getPatientMedications().add(patientMedication);
		patientMedication.setPatient(this);

		return patientMedication;
	}

	public PatientMedication removePatientMedication(PatientMedication patientMedication) {
		getPatientMedications().remove(patientMedication);
		patientMedication.setPatient(null);

		return patientMedication;
	}

	public List<PatientReport> getPatientReports() {
		return this.patientReports;
	}

	public void setPatientReports(List<PatientReport> patientReports) {
		this.patientReports = patientReports;
	}

	public PatientReport addPatientReport(PatientReport patientReport) {
		getPatientReports().add(patientReport);
		patientReport.setPatient(this);

		return patientReport;
	}

	public PatientReport removePatientReport(PatientReport patientReport) {
		getPatientReports().remove(patientReport);
		patientReport.setPatient(null);

		return patientReport;
	}

	public List<PatientResponsibileParty> getPatientResponsibileParties() {
		return this.patientResponsibileParties;
	}

	public void setPatientResponsibileParties(List<PatientResponsibileParty> patientResponsibileParties) {
		this.patientResponsibileParties = patientResponsibileParties;
	}

	public PatientResponsibileParty addPatientResponsibileParty(PatientResponsibileParty patientResponsibileParty) {
		getPatientResponsibileParties().add(patientResponsibileParty);
		patientResponsibileParty.setPatient(this);

		return patientResponsibileParty;
	}

	public PatientResponsibileParty removePatientResponsibileParty(PatientResponsibileParty patientResponsibileParty) {
		getPatientResponsibileParties().remove(patientResponsibileParty);
		patientResponsibileParty.setPatient(null);

		return patientResponsibileParty;
	}

	public List<PatientResult> getPatientResults() {
		return this.patientResults;
	}

	public void setPatientResults(List<PatientResult> patientResults) {
		this.patientResults = patientResults;
	}

	public PatientResult addPatientResult(PatientResult patientResult) {
		getPatientResults().add(patientResult);
		patientResult.setPatient(this);

		return patientResult;
	}

	public PatientResult removePatientResult(PatientResult patientResult) {
		getPatientResults().remove(patientResult);
		patientResult.setPatient(null);

		return patientResult;
	}

	public List<PatientVital> getPatientVitals() {
		return this.patientVitals;
	}

	public void setPatientVitals(List<PatientVital> patientVitals) {
		this.patientVitals = patientVitals;
	}

//	public PatientVital addPatientVital(PatientVital patientVital) {
//		getPatientVitals().add(patientVital);
//		patientVital.setPatient(this);
//
//		return patientVital;
//	}
//
//	public PatientVital removePatientVital(PatientVital patientVital) {
//		getPatientVitals().remove(patientVital);
//		patientVital.setPatient(null);
//
//		return patientVital;
//	}

	public List<UserPreference> getUserPreferences() {
		return this.userPreferences;
	}

	public void setUserPreferences(List<UserPreference> userPreferences) {
		this.userPreferences = userPreferences;
	}

//	public UserPreference addUserPreference(UserPreference userPreference) {
//		getUserPreferences().add(userPreference);
//		userPreference.setPatient(this);
//
//		return userPreference;
//	}
//
//	public UserPreference removeUserPreference(UserPreference userPreference) {
//		getUserPreferences().remove(userPreference);
//		userPreference.setPatient(null);
//
//		return userPreference;
//	}

}