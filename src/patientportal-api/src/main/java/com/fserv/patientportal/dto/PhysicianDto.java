package com.fserv.patientportal.dto;

public class PhysicianDto {

  private int id;
  private String firstName;
  private String lastName;
  private int age;
  private int cellPhoneNumber;
  private int homePhoneNumber;
  private int workPhoneNumber;
  private String email;
  private String gender;
  private String specialization;
  private int physicianNpid;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getCellPhoneNumber() {
    return cellPhoneNumber;
  }

  public void setCellPhoneNumber(int cellPhoneNumber) {
    this.cellPhoneNumber = cellPhoneNumber;
  }

  public int getHomePhoneNumber() {
    return homePhoneNumber;
  }

  public void setHomePhoneNumber(int homePhoneNumber) {
    this.homePhoneNumber = homePhoneNumber;
  }

  public int getWorkPhoneNumber() {
    return workPhoneNumber;
  }

  public void setWorkPhoneNumber(int workPhoneNumber) {
    this.workPhoneNumber = workPhoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getSpecialization() {
    return specialization;
  }

  public void setSpecialization(String specialization) {
    this.specialization = specialization;
  }

  public int getPhysicianNpid() {
    return physicianNpid;
  }

  public void setPhysicianNpid(int physicianNpid) {
    this.physicianNpid = physicianNpid;
  }

}
