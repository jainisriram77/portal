package com.fserv.patientportal.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fserv.patientportal.model.User;

@Repository
@Transactional
public interface UserRepository extends BaseRepository<User, Integer> {
	@Query("SELECT u FROM User u WHERE u.userName=:userName and u.password=:password")
	public User findByUsername(@Param("userName") String username,@Param("password") String password);
	
	
	

}
