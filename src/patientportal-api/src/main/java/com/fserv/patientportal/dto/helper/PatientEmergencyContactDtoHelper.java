package com.fserv.patientportal.dto.helper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientEmergencyContactDto;
import com.fserv.patientportal.model.Patient;
import com.fserv.patientportal.model.PatientEmergencyContact;

@Component
public class PatientEmergencyContactDtoHelper
    implements DtoHelper<PatientEmergencyContact, PatientEmergencyContactDto> {

  @Override
  public List<PatientEmergencyContactDto> buildDto(Iterable<PatientEmergencyContact> models) {
    return null;
  }

  @Override
  public PatientEmergencyContactDto buildDto(PatientEmergencyContact model) {
    PatientEmergencyContactDto patientEmerContDto = new PatientEmergencyContactDto();
    patientEmerContDto.setEmergencyAddress(model.getEmergencyAddress());
    patientEmerContDto.setEmergencyContactName(model.getEmergencyContactName());
    patientEmerContDto.setEmergencyContactRelation(model.getEmergencyContactRelation());
    patientEmerContDto.setEmergencyEmail(model.getEmergencyEmail());
    patientEmerContDto.setId(model.getId());
    patientEmerContDto.setPatientId(model.getPatientId());
    patientEmerContDto.setEmergencyContactNumber(model.getEmergencyContactNumber());
    return patientEmerContDto;
  }

  @Override
  public PatientEmergencyContact build(PatientEmergencyContactDto dto) {
    PatientEmergencyContact patientEmerCont = new PatientEmergencyContact();
    patientEmerCont.setEmergencyAddress(dto.getEmergencyAddress());
    patientEmerCont.setEmergencyContactName(dto.getEmergencyContactName());
    patientEmerCont.setEmergencyContactNumber(dto.getEmergencyContactNumber());
    patientEmerCont.setEmergencyContactRelation(dto.getEmergencyContactRelation());
    patientEmerCont.setEmergencyEmail(dto.getEmergencyEmail());
    patientEmerCont.setPatientId(dto.getPatientId());
    patientEmerCont.setId(dto.getId());
    return patientEmerCont;
  }

  @Override
  public List<PatientEmergencyContact> build(Iterable<PatientEmergencyContactDto> dtos) {
    return null;
  }

}
