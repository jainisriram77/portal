package com.fserv.patientportal.dto;

import java.util.Date;
import java.util.List;

import com.fserv.patientportal.model.ActivityType;

public class ActivityDto {

  private int id;
  private Date activityDate;
  private ActivityType activityType;
  private String from;
  private String message;
  private String subject;
  private String to;
  private List<PatientDto> patients;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getActivityDate() {
    return activityDate;
  }

  public void setActivityDate(Date activityDate) {
    this.activityDate = activityDate;
  }

  public String getFrom() {
    return from;
  }

  public ActivityType getActivityType() {
    return activityType;
  }

  public void setActivityType(ActivityType activityType) {
    this.activityType = activityType;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getTo() {
    return to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public List<PatientDto> getPatients() {
    return patients;
  }

  public void setPatients(List<PatientDto> patients) {
    this.patients = patients;
  }

}
