package com.fserv.patientportal.dto;

import java.util.List;

import com.fserv.patientportal.model.PatientResult;

public class ResultMetricDto {

  private int id;
  private String highValue;
  private String lowValue;
  private String normalValue;
  private String resultMetricCode;
  private String resultMetricName;
  private List<PatientResultDto> patientResultsDto;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getHighValue() {
    return highValue;
  }

  public void setHighValue(String highValue) {
    this.highValue = highValue;
  }

  public String getLowValue() {
    return lowValue;
  }

  public void setLowValue(String lowValue) {
    this.lowValue = lowValue;
  }

  public String getNormalValue() {
    return normalValue;
  }

  public void setNormalValue(String normalValue) {
    this.normalValue = normalValue;
  }

  public String getResultMetricCode() {
    return resultMetricCode;
  }

  public void setResultMetricCode(String resultMetricCode) {
    this.resultMetricCode = resultMetricCode;
  }

  public String getResultMetricName() {
    return resultMetricName;
  }

  public void setResultMetricName(String resultMetricName) {
    this.resultMetricName = resultMetricName;
  }

  public List<PatientResultDto> getPatientResultsDto() {
    return patientResultsDto;
  }

  public void setPatientResultsDto(List<PatientResultDto> patientResultsDto) {
    this.patientResultsDto = patientResultsDto;
  }



}
