package com.fserv.patientportal.dto.helper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientContactInformationDto;
import com.fserv.patientportal.model.PatientContactInformation;


@Component
public class PatientContactInformationDtoHelper 
	implements DtoHelper<PatientContactInformation, PatientContactInformationDto>{

	@Override
	public List<PatientContactInformationDto> buildDto(
			Iterable<PatientContactInformation> models) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PatientContactInformationDto buildDto(PatientContactInformation model) {
		PatientContactInformationDto patientconinfoDto = new PatientContactInformationDto();
		patientconinfoDto.setAddress1(model.getAddress1());
		patientconinfoDto.setAddress2(model.getAddress2());
		patientconinfoDto.setCity(model.getCity());
		patientconinfoDto.setCountry(model.getCountry());
		patientconinfoDto.setContactType(model.getContactType());
		patientconinfoDto.setEmail(model.getEmail());
		patientconinfoDto.setState(model.getState());
		patientconinfoDto.setId(model.getId());
		patientconinfoDto.setZip(model.getZip());
		patientconinfoDto.setPhoneNumber(model.getPhoneNumber());
		return patientconinfoDto;
	}

	@Override
	public PatientContactInformation build(PatientContactInformationDto dto) {
		PatientContactInformation patientconinfo = new PatientContactInformation();
		patientconinfo.setAddress1(dto.getAddress1());
		patientconinfo.setAddress2(dto.getAddress2());
		patientconinfo.setCity(dto.getCity());
		patientconinfo.setCountry(dto.getCountry());
		patientconinfo.setContactType(dto.getContactType());
		patientconinfo.setEmail(dto.getEmail());
		patientconinfo.setState(dto.getState());
		patientconinfo.setId(dto.getId());
		patientconinfo.setZip(dto.getZip());
		patientconinfo.setPhoneNumber(dto.getPhoneNumber());
		return patientconinfo;
	}

	@Override
	public List<PatientContactInformation> build(
			Iterable<PatientContactInformationDto> dtos) {
		// TODO Auto-generated method stub
		return null;
	}

}
