package com.fserv.patientportal.dto.helper;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PhysicianDto;
import com.fserv.patientportal.model.Physician;

@Component
public class PhysicianDtoHelper implements DtoHelper<Physician, PhysicianDto> {

  @Override
  public List<PhysicianDto> buildDto(Iterable<Physician> models) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public PhysicianDto buildDto(Physician model) {
    PhysicianDto physicianDto = null;
    if (model != null) {
      physicianDto = new PhysicianDto();
      physicianDto.setAge(model.getAge());
      physicianDto.setCellPhoneNumber(model.getCellPhoneNumber());
      physicianDto.setEmail(model.getEmail());
      physicianDto.setFirstName(model.getFirstName());
      physicianDto.setLastName(model.getLastName());
      physicianDto.setGender(model.getGender());
      physicianDto.setHomePhoneNumber(model.getHomePhoneNumber());
      physicianDto.setId(model.getPhysicianId());
      physicianDto.setPhysicianNpid(model.getPhysicianNpid());
      physicianDto.setSpecialization(model.getSpecialization());
      physicianDto.setWorkPhoneNumber(model.getWorkPhoneNumber());
    }
    return physicianDto;
  }

  @Override
  public Physician build(PhysicianDto dto) {
    Physician physic = null;
    if (dto != null) {
      physic = new Physician();
      physic.setAge(dto.getAge());
      physic.setCellPhoneNumber(dto.getCellPhoneNumber());
      physic.setEmail(dto.getEmail());
      physic.setFirstName(dto.getFirstName());
      physic.setLastName(dto.getLastName());
      physic.setGender(dto.getGender());
      physic.setHomePhoneNumber(dto.getHomePhoneNumber());
      physic.setPhysicianNpid(dto.getPhysicianNpid());
      physic.setSpecialization(dto.getSpecialization());
      physic.setPhysicianId(dto.getId());
      physic.setWorkPhoneNumber(dto.getWorkPhoneNumber());
    }
    return physic;
  }

  @Override
  public List<Physician> build(Iterable<PhysicianDto> dtos) {
    // TODO Auto-generated method stub
    return null;
  }

}
