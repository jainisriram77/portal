package com.fserv.patientportal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.dto.PatientReportDto;
import com.fserv.patientportal.dto.PatientReportSearchCriteriaDto;
import com.fserv.patientportal.dto.helper.PatientReportDtoHelper;
import com.fserv.patientportal.model.PatientReport;
import com.fserv.patientportal.model.PatientReportPK;
import com.fserv.patientportal.repository.PatientReportRepository;
import com.fserv.patientportal.repository.PatientReportSearchRepository;

@Service
@Transactional
public class PatientReportServiceImpl extends
    BaseServiceImpl<PatientReport, PatientReportPK, PatientReportDto> implements PatientReportService {

  @Autowired
  private PatientReportDtoHelper ptReportDtoHelper;
  
  @Autowired
  private PatientReportSearchRepository ptReportSearchRepo;
  
  @Autowired
  private PatientReportRepository ptReportRepo;

  @Autowired
  public PatientReportServiceImpl(PatientReportRepository repository,
      PatientReportDtoHelper dtoHelper) {
    super(repository, dtoHelper);
  }
  
  @Override
  public List<PatientReportDto> searchPatientReport(PatientReportSearchCriteriaDto searchcriteria) {
    List<PatientReport> ptReports = ptReportSearchRepo.searchptReport(searchcriteria);
    return ptReportDtoHelper.buildDto(ptReports);
  }

  @Override
  public List<PatientReportDto> findPtreportByDocsIdAndpId(int patientId, String ptDocsId) {
    List<PatientReport> ptReports= ptReportRepo.findptReports(patientId,ptDocsId);
    return ptReportDtoHelper.buildDto(ptReports);
  }

}
