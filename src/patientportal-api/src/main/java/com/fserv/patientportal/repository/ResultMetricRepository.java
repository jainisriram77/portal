package com.fserv.patientportal.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.ResultMetric;

@Repository
@Transactional
public interface ResultMetricRepository extends BaseRepository<ResultMetric, Integer> {

}
