package com.fserv.patientportal.dto;

import java.sql.Time;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fserv.patientportal.model.AppointmentStatus;
import com.fserv.patientportal.model.AppointmentType;

public class AppointmentDto {

  private int id;

  @JsonFormat(pattern = "yyyy-mm-dd")
  private Date appointmentDate;
  private String appointmentReason;
  private AppointmentStatus appointmentStatus;

  @JsonFormat(pattern = "hh:mm")
  private Time appointmentTime;
  private AppointmentType appointmentType;
  // Change all these related dependencies to dtos
  private PatientDto patientDto;
  private PhysicianDto physicianDto;
  private HealthProviderDto healthProviderDto;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getAppointmentDate() {
    return appointmentDate;
  }

  public void setAppointmentDate(Date appointmentDate) {
    this.appointmentDate = appointmentDate;
  }

  public Time getAppointmentTime() {
    return appointmentTime;
  }

  public void setAppointmentTime(Time appointmentTime) {
    this.appointmentTime = appointmentTime;
  }

  public String getAppointmentReason() {
    return appointmentReason;
  }

  public void setAppointmentReason(String appointmentReason) {
    this.appointmentReason = appointmentReason;
  }



  public AppointmentStatus getAppointmentStatus() {
    return appointmentStatus;
  }

  public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
    this.appointmentStatus = appointmentStatus;
  }

  public AppointmentType getAppointmentType() {
    return appointmentType;
  }

  public void setAppointmentType(AppointmentType appointmentType) {
    this.appointmentType = appointmentType;
  }


  public PatientDto getPatientDto() {
    return patientDto;
  }

  public void setPatientDto(PatientDto patientDto) {
    this.patientDto = patientDto;
  }

  public PhysicianDto getPhysicianDto() {
    return physicianDto;
  }

  public void setPhysicianDto(PhysicianDto physicianDto) {
    this.physicianDto = physicianDto;
  }

  public HealthProviderDto getHealthProviderDto() {
    return healthProviderDto;
  }

  public void setHealthProviderDto(HealthProviderDto healthProviderDto) {
    this.healthProviderDto = healthProviderDto;
  }


}
