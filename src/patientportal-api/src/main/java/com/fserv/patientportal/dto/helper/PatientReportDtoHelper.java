package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.AppointmentDto;
import com.fserv.patientportal.dto.DocumentDto;
import com.fserv.patientportal.dto.PatientReportDto;
import com.fserv.patientportal.model.Appointment;
import com.fserv.patientportal.model.Document;
import com.fserv.patientportal.model.PatientReport;
import com.fserv.patientportal.model.PatientReportPK;

@Component
public class PatientReportDtoHelper implements DtoHelper<PatientReport, PatientReportDto> {

  @Override
  public List<PatientReportDto> buildDto(Iterable<PatientReport> models) {
    List<PatientReportDto> ptReportDtoList = new ArrayList<PatientReportDto>();
    for (PatientReport ptReports : models) {
      PatientReportDto dto = new PatientReportDto();
      dto.setAppointmentId(ptReports.getAppointment().getId());
      dto.setCapturedDate(ptReports.getCapturedDate());
      dto.setReportType(ptReports.getReportType());
      dto.setDocumentId(ptReports.getDocument().getId());
      dto.setPatientId(ptReports.getPatient().getId());
      AppointmentDto apt = new AppointmentDto();
      Appointment appoint = ptReports.getAppointment();
      apt.setAppointmentStatus(appoint.getAppointmentStatus());
      apt.setAppointmentTime(appoint.getAppointmentTime());
      apt.setAppointmentDate(appoint.getAppointmentDate());
      apt.setAppointmentReason(appoint.getAppointmentReason());
      DocumentDto docsdto = new DocumentDto();
      Document docs = ptReports.getDocument();
      docsdto.setCreatedBy(docs.getCreatedBy());
      docsdto.setCreationDate(docs.getCreationDate());
      docsdto.setDocumentDetail(docs.getDocumentDetail());
      docsdto.setDocumentName(docs.getDocumentName());
      docsdto.setDocumentType(docs.getDocumentType());
      docsdto.setSource(docs.getSource());
      docsdto.setModifiedDate(docs.getModifiedDate());
      docsdto.setId(docs.getId());
      docsdto.setDocumentURL(docs.getDocumentUrl());
      ptReportDtoList.add(dto);
    }
    return ptReportDtoList;
  }
  
  @Override
  public PatientReportDto buildDto(PatientReport model) {
    return null;
  }

  @Override
  public PatientReport build(PatientReportDto dto) {
    PatientReport ptReport = new PatientReport();
    PatientReportPK pk = new PatientReportPK();
    pk.setDocumentId(dto.getDocumentId());
    pk.setPatientId(dto.getPatientId());
    ptReport.setId(pk);
    return ptReport;
  }
  

  @Override
  public List<PatientReport> build(Iterable<PatientReportDto> dtos) {
    return null;
  }
}

// @Override
// public List<PatientReportDto> buildDto(Iterable<PatientReport> models) {
// List<PatientReportDto> dtoList = new ArrayList<PatientReportDto>();
// for(PatientReport pat:models){
// PatientReportDto dto = new PatientReportDto();
// dto.setCapturedDate(pat.getCapturedDate());
// dto.setId(pat.getId());
// Appointment p = new Appointment();
// p=pat.getPatientAppointment();
// dto.setPatientAppointmentId(p.getId());
// Document d = new Document();
// d=pat.getDocument();
// dto.setDocumentId(d.getId());
// dto.setReportType(pat.getReportType());
// dtoList.add(dto);
// }
// return dtoList;
// }
//
// @Override
// public PatientReportDto buildDto(PatientReport model) {
// PatientReportDto dto = null;
// if(model!=null){
//// dto = new PatientReportDto();
//// dto.setCapturedDate(model.getCapturedDate());
//// dto.setId(model.getId());
//// Appointment p = new Appointment();
//// p=model.getPatientAppointment();
//// dto.setPatientAppointmentId(p.getId());
//// Document d = new Document();
//// d=model.getDocument();
//// dto.setDocumentId(d.getId());
//// dto.setReportType(model.getReportType());
//// }
// return dto;
// }
//
// @Override
// public PatientReport build(PatientReportDto dto) {
// PatientReport model = null;
// if(dto!=null){
// model = new PatientReport();
// model.setCapturedDate(dto.getCapturedDate());
// model.setId(dto.getId());
// Appointment p = new Appointment();
// p.setId(dto.getPatientAppointmentId());
// model.setPatientAppointment(p);
// Document d = new Document();
// d.setId(dto.getDocumentId());
// model.setDocument(d);
// model.setReportType(dto.getReportType());
// }
// return model;
// }
//
// @Override
// public List<PatientReport> build(Iterable<PatientReportDto> dtos) {
// List<PatientReport> modelList = new ArrayList<PatientReport>();
// for(PatientReportDto pat:dtos){
// PatientReport model = new PatientReport();
// model.setCapturedDate(pat.getCapturedDate());
// model.setId(pat.getId());
// Appointment p = new Appointment();
// p.setId(pat.getPatientAppointmentId());
// model.setPatientAppointment(p);
// Document d = new Document();
// d.setId(pat.getDocumentId());
// model.setDocument(d);
// model.setReportType(pat.getReportType());
// modelList.add(model);
//
// }
// return modelList;
// }
//
// }
