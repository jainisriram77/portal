package com.fserv.patientportal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.dto.PatientMedicationDto;
import com.fserv.patientportal.dto.helper.PatientMedicationDtoHelper;
import com.fserv.patientportal.model.MedicationType;
import com.fserv.patientportal.model.PatientMedication;
import com.fserv.patientportal.repository.PatientMedicationRepository;

@Service
@Transactional
public class PatientMedicationServiceImpl
    extends BaseServiceImpl<PatientMedication, Integer, PatientMedicationDto>
    implements PatientMedicationService {

  @Autowired
  PatientMedicationDtoHelper dtoHelper;

  @Autowired
  public PatientMedicationServiceImpl(PatientMedicationRepository repository,
      PatientMedicationDtoHelper dtoHelper) {
    super(repository, dtoHelper);
  }

  @Override
  public List<PatientMedicationDto> findPtMedicationsByPtIdAndMedicationType(int patientId,
      MedicationType medicationType) {
    List<PatientMedication> ptMedications = ((PatientMedicationRepository) repository)
        .findPtMedicationsByIdAndType(patientId, medicationType);
    return dtoHelper.buildDto(ptMedications);
  }

}
