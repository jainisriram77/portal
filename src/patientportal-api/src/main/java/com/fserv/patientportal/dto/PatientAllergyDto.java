package com.fserv.patientportal.dto;

public class PatientAllergyDto {

  private int id;
  private String allergyDescription;
  private String allergyRemarks;
  private int patientId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getAllergyDescription() {
    return allergyDescription;
  }

  public void setAllergyDescription(String allergyDescription) {
    this.allergyDescription = allergyDescription;
  }

  public String getAllergyRemarks() {
    return allergyRemarks;
  }

  public void setAllergyRemarks(String allergyRemarks) {
    this.allergyRemarks = allergyRemarks;
  }

  public int getPatientId() {
    return patientId;
  }

  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }
}
