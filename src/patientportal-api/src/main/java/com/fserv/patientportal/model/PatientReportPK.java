package com.fserv.patientportal.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the patient_reports database table.
 * 
 */
@Embeddable
public class PatientReportPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="document_id", insertable=false, updatable=false)
	private String documentId;

	@Column(name="patient_id", insertable=false, updatable=false)
	private Integer patientId;

	public PatientReportPK() {
	}
	public String getDocumentId() {
		return this.documentId;
	}
	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	public Integer getPatientId() {
		return this.patientId;
	}
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PatientReportPK)) {
			return false;
		}
		PatientReportPK castOther = (PatientReportPK)other;
		return 
			this.documentId.equals(castOther.documentId)
			&& this.patientId.equals(castOther.patientId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.documentId.hashCode();
		hash = hash * prime + this.patientId.hashCode();
		
		return hash;
	}
}