package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientResponsibilePartyDto;
import com.fserv.patientportal.model.PatientResponsibileParty;

@Component
public class PatientResponsibilePartyDtoHelper
    implements DtoHelper<PatientResponsibileParty, PatientResponsibilePartyDto> {

  @Override
  public List<PatientResponsibilePartyDto> buildDto(Iterable<PatientResponsibileParty> models) {
    List<PatientResponsibilePartyDto> patientResponsibilePartyDtoList = null;
    if (models != null) {
      patientResponsibilePartyDtoList = new ArrayList<PatientResponsibilePartyDto>();
      for (PatientResponsibileParty prp : models) {
        patientResponsibilePartyDtoList.add(buildDto(prp));
      }
    }
    return patientResponsibilePartyDtoList;
  }

  @Override
  public PatientResponsibilePartyDto buildDto(PatientResponsibileParty model) {
    PatientResponsibilePartyDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new PatientResponsibilePartyDto();
      dtoToReturn.setPatient(model.getPatient());
      dtoToReturn.setPatientResponsibilePartyId(model.getId());
      dtoToReturn.setResponsibilePartyAddress(model.getResponsibilePartyAddress());
      dtoToReturn.setResponsibilePartyEmail(model.getResponsibilePartyEmail());
      dtoToReturn.setResponsibilePartyName(model.getResponsibilePartyName());
      dtoToReturn.setResponsibilePartyNumber(model.getResponsibilePartyNumber());
    }
    return dtoToReturn;
  }

  @Override
  public PatientResponsibileParty build(PatientResponsibilePartyDto dto) {
    PatientResponsibileParty model = null;
    if (dto != null) {
      model = new PatientResponsibileParty();
      model.setPatient(dto.getPatient());
      model.setId(dto.getPatientResponsibilePartyId());
      model.setResponsibilePartyAddress(dto.getResponsibilePartyAddress());
      model.setResponsibilePartyEmail(dto.getResponsibilePartyEmail());
      model.setResponsibilePartyName(dto.getResponsibilePartyName());
      model.setResponsibilePartyNumber(dto.getResponsibilePartyNumber());
    }
    return model;
  }

  @Override
  public List<PatientResponsibileParty> build(Iterable<PatientResponsibilePartyDto> dtos) {
    List<PatientResponsibileParty> models = null;
    if (dtos != null) {
      models = new ArrayList<PatientResponsibileParty>();
      for (PatientResponsibilePartyDto prpdto : dtos) {
        models.add(build(prpdto));
      }
    }
    return models;
  }

}
