package com.fserv.patientportal.service;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.fserv.patientportal.constants.AppProperties;
import com.fserv.patientportal.model.ComposeEmail;
import com.fserv.patientportal.repository.ComposeEmailRepository;

@Service
public class AmazonSESService {

	static final String BODY = "hi";
	static final String SUBJECT = "Amazon SES test (AWS SDK for Java)";

	private static final Logger log = Logger.getLogger(AmazonSESService.class);

	@Autowired
//	private  ComposeEmail email;
	private AppProperties appProperties;
	private AmazonSimpleEmailServiceClient client;
	
	@Autowired
	private ComposeEmailRepository composeEmailRepository;

	public AmazonSESService() {
		super();
	}

	@PostConstruct
	public void init() {
		AWSCredentials credentials = new BasicAWSCredentials(
				appProperties.getSesAccessKey(),
				appProperties.getSesSecretKey());
		init(credentials);
	}

	@PreDestroy
	public void cleanUp() throws Exception {
		log.info("Cleaning up amazon SES client");
		client.shutdown();
	}

	public void init(AWSCredentials credentials) {
		try {
			client = new AmazonSimpleEmailServiceClient(credentials);
			client.setRegion(Region.getRegion(Regions.US_EAST_1));
		} catch (Exception e) {
			throw new AmazonClientException(
					"Cannot load the credentials from the credential profiles file. "
							+ "Please make sure that your credentials file is at the correct "
							+ "location (~/.aws/credentials), and is in valid format.",
					e);
		}
	}
//	public void save(String subjectStr, String bodyStr,
//			String... toAddress){
//		Destination destination = new Destination().withToAddresses(email.);
//		Content subject = new Content().withData(subjectStr);
//		Content textBody = new Content().withData(bodyStr);
//		ComposeEmail email = new ComposeEmail()
//	}
	
	public void sendEmail(String subjectStr, String bodyStr,
			String toAddress) throws IOException {
		// Construct an object to contain the recipient address.
		Destination destination = new Destination().withToAddresses(toAddress);

		// Create the subject and body of the message.
		Content subject = new Content().withData(subjectStr);
		Content textBody = new Content().withData(bodyStr);
		Body body = new Body().withText(textBody);

		// Create a message with the specified subject and body.
		Message message = new Message().withSubject(subject).withBody(body);

		// Assemble the email.
		SendEmailRequest request = new SendEmailRequest()
				.withSource(appProperties.getSesFromEmail())
				.withDestination(destination).withMessage(message);
		ComposeEmail email = new ComposeEmail();

		email.setBody(bodyStr);
		email.setSubject(subjectStr);
		email.setToEmail(toAddress);
		composeEmailRepository.save(email);
		
		try {
			System.out
					.println("Attempting to send an email through Amazon SES by using the AWS SDK for Java...");
			// Send the email.
			client.sendEmail(request);
			
			System.out.println("Email sent!");
		} catch (Exception ex) {
			System.out.println("The email was not sent.");
			System.out.println("Error message: " + ex.getMessage());
		}
	}
	
	
	public static void main(String args[]) {
		// AWSCredentials credentials =
		// new BasicAWSCredentials("AKIAIHLRTJR7GTAQUQKQ",
		// "AvR9Qwb7jAAwv5bHMf29LYc5w/j6WPNPtjmZ+RTwabdX");
		AmazonSESService service = new AmazonSESService();
		AppProperties appProps = new AppProperties();
		appProps.setSesAccessKey("AKIAI6BDR5N2ZXSR732Q");
		appProps.setSesSecretKey("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");
		appProps.setSesFromEmail("admin@ephaseglobal.com");

		service.appProperties = appProps;
		service.init();
		// service.setFrom("admin@ephaseglobal.com");
		try {
			service.sendEmail(SUBJECT, BODY, "jainisriram77@gmail.com");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}