package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.UserPreferenceDto;
import com.fserv.patientportal.model.User;
import com.fserv.patientportal.model.UserPreference;

@Component
public class UserPreferenceDtoHelper implements DtoHelper<UserPreference, UserPreferenceDto> {
  @Override
  public List<UserPreferenceDto> buildDto(Iterable<UserPreference> models) {
    List<UserPreferenceDto> userDtoList = null;
    if (models != null) {
      userDtoList = new ArrayList<UserPreferenceDto>();
      for (UserPreference usr : models) {
        userDtoList.add(buildDto(usr));
      }
    }
    return userDtoList;
  }

  @Override
  public UserPreferenceDto buildDto(UserPreference model) {
    UserPreferenceDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new UserPreferenceDto();
      dtoToReturn.setId(model.getId());
      dtoToReturn.setPreferenceName(model.getPreferenceName());
      dtoToReturn.setPreferenceValue(model.getPreferenceName());
      dtoToReturn.setUserId(model.getUserId());
    }
    return dtoToReturn;
  }

  @Override
  public UserPreference build(UserPreferenceDto dto) {
    UserPreference model = null;
    if (dto != null) {
      model = new UserPreference();
      model.setId(dto.getId());
      model.setPreferenceName(dto.getPreferenceName());
      model.setPreferenceValue(dto.getPreferenceValue());
      User user = new User();
      user.setId(dto.getUserId());
      model.setUserId(null);
      }
    return model;
  }

  @Override
  public List<UserPreference> build(Iterable<UserPreferenceDto> dtos) {
    List<UserPreference> models = null;
    if (dtos != null) {
      models = new ArrayList<UserPreference>();
      for (UserPreferenceDto usrdto : dtos) {
        models.add(build(usrdto));
      }
    }
    return models;
  }
}
