package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.PatientResultDto;

public interface PatientResultService extends BaseService<Integer, PatientResultDto> {

  List<PatientResultDto> findPatientResults(int patientId);



}
