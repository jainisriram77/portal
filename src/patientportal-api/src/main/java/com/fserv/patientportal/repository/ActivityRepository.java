package com.fserv.patientportal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.Activity;

@Repository
@Transactional
public interface ActivityRepository extends BaseRepository<Activity, Integer> {

  @Query("select activity from Activity activity where activity.id in :activityIds")
  public List<Activity> findActivitybyIds(@Param("activityIds") List<Integer> activityIds);

}
