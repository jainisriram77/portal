package com.fserv.patientportal.service;

import java.util.List;

import com.fserv.patientportal.dto.AppointmentDto;
import com.fserv.patientportal.dto.AppointmentSearchDto;

public interface AppointmentService extends BaseService<Integer, AppointmentDto> {



  public List<AppointmentDto> findAllwithpatientId(int patientId);

  public List<AppointmentDto> findAppointmentBypId(int physicianId);

  public List<AppointmentDto> searchcriteria(AppointmentSearchDto dto);

}
