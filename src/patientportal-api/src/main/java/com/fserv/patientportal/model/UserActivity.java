package com.fserv.patientportal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the user_activity database table.
 * 
 */
@Entity
@Table(name="user_activity", schema = "patientportal")
@NamedQuery(name="UserActivity.findAll", query="SELECT u FROM UserActivity u")
public class UserActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserActivityPK id;

	@Column(name="is_user_read")
	private String isUserRead;


    @ManyToOne
    @JoinColumn(name = "activity_id",insertable = false, updatable = false)
    private Activity activity;
    
    @ManyToOne
    @JoinColumn(name = "user_id",insertable = false, updatable = false)
    private User user;
    
    
	public UserActivity() {
	}

	public UserActivityPK getId() {
		return this.id;
	}

	public void setId(UserActivityPK id) {
		this.id = id;
	}

	public String getIsUserRead() {
		return this.isUserRead;
	}

	public void setIsUserRead(String isUserRead) {
		this.isUserRead = isUserRead;
	}

	   public Activity getActivity() {
	      return this.activity;
	  }

	  public void setActivity(Activity activity) {
	      this.activity = activity;
	  }
	  
	  public User getUser(){
	    return this.user;
	  }

	  public void setUser(User user){
	    this.user = user;
	  }
	  
}