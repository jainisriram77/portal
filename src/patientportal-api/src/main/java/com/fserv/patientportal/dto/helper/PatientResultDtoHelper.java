package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.PatientResultDto;
import com.fserv.patientportal.model.PatientResult;

@Component
public class PatientResultDtoHelper implements DtoHelper<PatientResult, PatientResultDto> {

  @Override
  public List<PatientResultDto> buildDto(Iterable<PatientResult> models) {
    List<PatientResultDto> resultDtoList = null;
    if (models != null) {
      resultDtoList = new ArrayList<PatientResultDto>();
      for (PatientResult result : models) {
        resultDtoList.add(buildDto(result));
      }
    }
    return resultDtoList;
  }

  @Override
  public PatientResultDto buildDto(PatientResult model) {
    PatientResultDto dtoToReturn = null;
    if (model != null) {
      dtoToReturn = new PatientResultDto();
      dtoToReturn.setId(model.getId());
      dtoToReturn.setPatientAppointmentId(model.getAppointment().getId());
      dtoToReturn.setPatientId(model.getPatient().getId());
      dtoToReturn.setResultDate(model.getResultDate());
      dtoToReturn.setResultDetails(model.getResultDetails());
      dtoToReturn.setResultMetricId(model.getResultMetric().getId());
      dtoToReturn.setResultProvider(model.getResultProvider());
      dtoToReturn.setResultValue(model.getResultValue());
    }
    return dtoToReturn;
  }

  @Override
  public PatientResult build(PatientResultDto dto) {
    PatientResult model = null;
    if (dto != null) {
      model = new PatientResult();
      model.setId(dto.getId());
      // model.setPatient(dto.getPatientId());;
      // model.setPatientAppointment(dto.getPatientAppointmentId());
      model.setResultDate(dto.getResultDate());
      model.setResultDetails(dto.getResultDetails());
      // model.setResultMetric(dto.getResultMetricId());
      model.setResultProvider(dto.getResultProvider());
      model.setResultValue(dto.getResultValue());
    }
    return model;
  }

  @Override
  public List<PatientResult> build(Iterable<PatientResultDto> dtos) {
    List<PatientResult> models = null;
    if (dtos != null) {
      models = new ArrayList<PatientResult>();
      for (PatientResultDto resultdto : dtos) {
        models.add(build(resultdto));
      }
    }
    return models;
  }
}
