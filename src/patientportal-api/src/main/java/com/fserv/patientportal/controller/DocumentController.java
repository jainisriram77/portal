package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.DocumentDto;
import com.fserv.patientportal.dto.DocumentSearchCriteriaDto;
import com.fserv.patientportal.service.DocumentService;

@Controller
@RequestMapping(value = "/patientDocs/")
public class DocumentController extends BaseControllerImpl<Integer, DocumentDto> {

  @Autowired
  private DocumentService documentservice;

  @Autowired(required = true)
  public DocumentController(DocumentService resourceService) {
    super(resourceService);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/search/", produces = "application/json")
  public @ResponseBody List<DocumentDto> searchptDocu(
      @RequestBody DocumentSearchCriteriaDto searchcriteria) {
    List<DocumentDto> ptDocuDto = documentservice.searchPtDocu(searchcriteria);
    return ptDocuDto;
  }
}
