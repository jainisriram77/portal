package com.fserv.patientportal.dto;

import java.util.Date;

public class PatientReportSearchCriteriaDto {
  
  private int patientId;
  private int appointmentId;
  private String documentId;
  private String documentType;
  private Date documentDate;
  
  public int getPatientId() {
    return patientId;
  }
  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }
  public int getAppointmentId() {
    return appointmentId;
  }
  public void setAppointmentId(int appointmentId) {
    this.appointmentId = appointmentId;
  }
  public String getDocumentId() {
    return documentId;
  }
  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }
  public String getDocumentType() {
    return documentType;
  }
  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }
  public Date getDocumentDate() {
    return documentDate;
  }
  public void setDocumentDate(Date documentDate) {
    this.documentDate = documentDate;
  }

}
