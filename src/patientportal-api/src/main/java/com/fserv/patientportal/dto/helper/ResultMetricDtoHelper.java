package com.fserv.patientportal.dto.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fserv.patientportal.dto.ResultMetricDto;
import com.fserv.patientportal.model.ResultMetric;

@Component
public class ResultMetricDtoHelper implements DtoHelper<ResultMetric, ResultMetricDto> {

  @Override
  public List<ResultMetricDto> buildDto(Iterable<ResultMetric> models) {
    List<ResultMetricDto> dtoList = new ArrayList<ResultMetricDto>();
    for (ResultMetric resultMetric : models) {
      ResultMetricDto dto = new ResultMetricDto();
      dto.setHighValue(resultMetric.getHighValue());
      dto.setId(resultMetric.getId());
      dto.setLowValue(resultMetric.getLowValue());
      dto.setNormalValue(resultMetric.getNormalValue());
      // dto.setPatientResultsDto(resultMetric.getPatientResults());
      dto.setResultMetricCode(resultMetric.getResultMetricCode());
      dto.setResultMetricName(resultMetric.getResultMetricName());
      dtoList.add(dto);
    }
    return dtoList;
  }

  @Override
  public ResultMetricDto buildDto(ResultMetric model) {
    ResultMetricDto dto = null;
    if (model != null) {
      dto = new ResultMetricDto();
      dto.setHighValue(model.getHighValue());
      dto.setId(model.getId());
      dto.setLowValue(model.getLowValue());
      dto.setNormalValue(model.getNormalValue());
      // dto.setPatientResults(model.getPatientResults());
      dto.setResultMetricCode(model.getResultMetricCode());
      dto.setResultMetricName(model.getResultMetricName());
    }
    return dto;
  }

  @Override
  public ResultMetric build(ResultMetricDto dto) {
    ResultMetric model = null;
    if (dto != null) {
      model = new ResultMetric();
      dto.setHighValue(dto.getHighValue());
      model.setId(dto.getId());
      model.setLowValue(dto.getLowValue());
      model.setNormalValue(dto.getNormalValue());
      // model.setPatientResults(dto.getPatientResults());
      model.setResultMetricCode(dto.getResultMetricCode());
      model.setResultMetricName(dto.getResultMetricName());
    }
    return model;
  }

  @Override
  public List<ResultMetric> build(Iterable<ResultMetricDto> dtos) {
    // TODO Auto-generated method stub
    return null;
  }

}
