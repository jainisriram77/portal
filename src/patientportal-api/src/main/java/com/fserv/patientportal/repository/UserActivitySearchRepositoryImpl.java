package com.fserv.patientportal.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fserv.patientportal.dto.UserActivitySearchCriteriaDto;
import com.fserv.patientportal.model.UserActivity;

@Repository
@Transactional
public class UserActivitySearchRepositoryImpl implements UserActivitySearchRepository {

  @PersistenceContext
  private EntityManager entityMgr;

  @SuppressWarnings("unchecked")
  @Override
  public List<UserActivity> searchUserActivities(
      UserActivitySearchCriteriaDto usrActivitySearchCriteria) {
   
    StringBuilder query = new StringBuilder();
    // query.append("SELECT ac FROM Activity AS ac left join ac.users as usr
    // ");

    query.append("SELECT usac FROM UserActivity AS usac ");
    query.append("LEFT JOIN FETCH usac.activity AS ac ");
    query.append("LEFT JOIN FETCH usac.user AS usr ");
    query.append("WHERE 1=1 ");
    // query.append("AND ac.id = usac.activity.id ");
    // query.append("AND usr.id = usac.user.id ");

    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getUserId()))) {
      query.append("AND usr.id=:userid ");
    }
    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getActivityDate()))) {
      query.append("AND ac.activityDate=:activityDate ");
    }
    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getActivityType()))) {
      query.append("AND ac.activityType=:activityType ");
    }
    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getFrom()))) {
      query.append("AND ac.from=:from ");
    }
    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getMessage()))) {
      query.append("AND ac.message=:message ");
    }
    // String s = "SELECT usac.activity FROM UserActivity AS usac LEFT JOIN
    // usac.activity AS ac LEFT JOIN usac.user AS usr WHERE 1=1 ";

    Query query1 = entityMgr.createQuery(query.toString());

    if (usrActivitySearchCriteria.getUserId() != 0)
      query1.setParameter("userid", usrActivitySearchCriteria.getUserId());

    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getActivityDate())))
      query1.setParameter("activityDate", usrActivitySearchCriteria.getActivityDate());

    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getActivityType())))
      query1.setParameter("activityType", usrActivitySearchCriteria.getActivityType().getValue());

    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getFrom())))
      query1.setParameter("from", usrActivitySearchCriteria.getFrom());

    if ((!StringUtils.isEmpty(usrActivitySearchCriteria.getMessage())))
      query1.setParameter("message", usrActivitySearchCriteria.getMessage());

    List<UserActivity> resultList = query1.getResultList();
    return resultList;

  }

}
