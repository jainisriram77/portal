package com.fserv.patientportal.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fserv.patientportal.dto.PatientEmergencyContactDto;
import com.fserv.patientportal.dto.helper.DtoHelper;
import com.fserv.patientportal.model.PatientEmergencyContact;
import com.fserv.patientportal.repository.BaseRepository;

@Service
@Transactional
public class PatientEmergencyContactServiceImpl
    extends BaseServiceImpl<PatientEmergencyContact, Integer, PatientEmergencyContactDto>
    implements PatientEmergencyContactService {

  @Autowired
  public PatientEmergencyContactServiceImpl(
      BaseRepository<PatientEmergencyContact, Integer> repository,

      DtoHelper<PatientEmergencyContact, PatientEmergencyContactDto> dtoHelper) {
    super(repository, dtoHelper);
  }

}
