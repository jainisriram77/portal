package com.fserv.patientportal.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class PatientSearchRepoImpl implements PatientSearchRepository {

  @PersistenceContext
  private EntityManager entityMgr;

  @Override
  public void search() {

  }

}
