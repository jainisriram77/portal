package com.fserv.patientportal.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fserv.patientportal.dto.DocumentSearchCriteriaDto;
import com.fserv.patientportal.model.Document;

@Repository
@Transactional
public class DocumentSearchRepoImpl implements DocumentSearchRepository {

  @PersistenceContext
  private EntityManager entityMgr;

  @SuppressWarnings("unchecked")
  @Override
  public List<Document> searchptDocu(DocumentSearchCriteriaDto searchcriteria) {

    StringBuilder query = new StringBuilder();

    query.append("SELECT docs FROM Document AS docs ");
    query.append("WHERE 1=1 ");

    if ((!StringUtils.isEmpty(searchcriteria.getCreationDate()))) {
      query.append("AND docs.creationDate=:CreationDate ");
    }
    // if ((!StringUtils.isEmpty(searchcriteria.getDocumentId()))) {
    // query.append("AND docs.id=: ");
    // }
    if ((!StringUtils.isEmpty(searchcriteria.getDocumentType()))) {
      query.append("AND docs.documentType=:documentType ");
    }
    if ((!StringUtils.isEmpty(searchcriteria.getDocumentName()))) {
      query.append("AND docs.documentName=:documentName ");
    }

    Query query1 = entityMgr.createQuery(query.toString());

    if ((!StringUtils.isEmpty(searchcriteria.getCreationDate())))
      query1.setParameter("creationDate", searchcriteria.getCreationDate());

    if ((!StringUtils.isEmpty(searchcriteria.getDocumentType())))
      query1.setParameter("documentType", searchcriteria.getDocumentType());

    if ((!StringUtils.isEmpty(searchcriteria.getDocumentName())))
      query1.setParameter("documentName", searchcriteria.getDocumentName());

    List<Document> ptDocsList = query1.getResultList();
    return ptDocsList;
  }

}
