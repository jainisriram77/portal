package com.fserv.patientportal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.PatientInsurancePlan;

@Repository
@Transactional
public interface PatientInsurancePlanRepository
    extends BaseRepository<PatientInsurancePlan, Integer> {

  @Query("SELECT ptinsplan FROM PatientInsurancePlan ptinsplan LEFT JOIN FETCH ptinsplan.patient WHERE ptinsplan.patient.id = :patientId")
  public List<PatientInsurancePlan> findPtInsurancePlans(@Param("patientId") int patientId);


  // public List<PatientInsurancePlan> findallPatientInsuplan(int patientId);

}
