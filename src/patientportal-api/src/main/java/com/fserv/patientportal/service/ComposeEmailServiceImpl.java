package com.fserv.patientportal.service;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.fserv.patientportal.dto.ComposeEmailDto;
import com.fserv.patientportal.dto.helper.ComposeEmailDtoHelper;

import com.fserv.patientportal.model.ComposeEmail;
import com.fserv.patientportal.repository.ComposeEmailRepository;


@Service
@Transactional
public  class ComposeEmailServiceImpl extends
BaseServiceImpl<ComposeEmail, Integer, ComposeEmailDto> implements ComposeEmailService  {
	 @Autowired
	 ComposeEmailDtoHelper composeEmailDtoHelper;
	 @Autowired
	
	 ComposeEmailRepository composeEmailRepository;
	 
	 @Autowired
	  public ComposeEmailServiceImpl(ComposeEmailRepository composeEmailRepository,
			  ComposeEmailDtoHelper composeEmailDtoHelper) {
	    super( composeEmailRepository, composeEmailDtoHelper);
	    
	    
}

	@Override
	public List<ComposeEmailDto> findComposeEmailbyId(int Id) {
		Iterable<ComposeEmail> composeEmail= composeEmailRepository.findComposeEmailbyId(Id);
		return composeEmailDtoHelper.buildDto(composeEmail);
	}

	

//	 @Override
//	  public List<ComposeEmailDto>findAll(int Id) {
//		 Iterable<ComposeEmail> composeEmail= repository.findAll(Id);
//	    return composeEmailDtoHelper.buildDto(composeEmail);
//
//	  }
//	 @Override
//	  public List<ComposeEmailDto> findComposeEmailbyId(int composeEmailId) {
//	    List<ComposeEmail> composeEmail =composeEmailRepository.findComposeEmailbyId(composeEmailId);
//	    return composeEmailDtoHelper.buildDto(composeEmail);
//	  }
}
