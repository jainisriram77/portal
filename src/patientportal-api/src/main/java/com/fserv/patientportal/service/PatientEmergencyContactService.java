package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.PatientEmergencyContactDto;

public interface PatientEmergencyContactService
    extends BaseService<Integer, PatientEmergencyContactDto> {

}
