package com.fserv.patientportal.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fserv.patientportal.model.ConditionType;
import com.fserv.patientportal.model.PatientCondition;

@Repository
@Transactional
public interface PatientConditionRepository extends BaseRepository<PatientCondition, Integer> {

  @Query("SELECT pc FROM PatientCondition pc WHERE pc.patient.id = :patientId AND pc.conditionType = :conditionType")
  public List<PatientCondition> findPtConditionsByPtIdAndType(@Param("patientId") int patientId,
      @Param("conditionType") ConditionType conditionType);

  @Query("SELECT pc FROM PatientCondition pc WHERE pc.patient.id = :patientId")
  public List<PatientCondition> findPtConditionsByPtId(@Param("patientId") int patientId);

}
