package com.fserv.patientportal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fserv.patientportal.model.MedicationType;
import com.fserv.patientportal.model.PatientMedication;

@Repository
@Transactional
public interface PatientMedicationRepository extends BaseRepository<PatientMedication, Integer> {

  @Query("SELECT pm FROM PatientMedication pm WHERE pm.patient.id =:patientId AND pm.medicationType =:medicationType")
  public List<PatientMedication> findPtMedicationsByIdAndType(@Param("patientId") int patientId,
      @Param("medicationType") MedicationType medicationType);

}
