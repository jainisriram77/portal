 package com.fserv.patientportal.service;

 import javax.transaction.Transactional;

 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.stereotype.Service;

 import com.fserv.patientportal.dto.PatientDto;
 import com.fserv.patientportal.dto.helper.PatientDtoHelper;
 import com.fserv.patientportal.model.Patient;
 import com.fserv.patientportal.repository.PatientRepository;

 @Service
 @Transactional
 public class PatientServiceImpl extends
 BaseServiceImpl<Patient, Integer, PatientDto> implements PatientService {

 @Autowired
 public PatientServiceImpl(PatientRepository ptRepository,
 PatientDtoHelper patientDtoHelper) {
 super(ptRepository, patientDtoHelper);
 }

 }
