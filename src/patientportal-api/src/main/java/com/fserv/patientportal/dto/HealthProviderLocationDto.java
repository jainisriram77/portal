package com.fserv.patientportal.dto;

import java.util.List;

import com.fserv.patientportal.model.Appointment;

public class HealthProviderLocationDto {

  private int id;

  private String address1;

  private String address2;

  private String city;

  private int code;

  private String email;

  private Boolean isPrimaryLocation;
  private String name;
  private Integer officeNumber;

  private String state;

  private String zip;
  private HealthProviderDto healthProviderDto;
  private List<Appointment> patientAppointments;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean getIsPrimaryLocation() {
    return isPrimaryLocation;
  }

  public void setIsPrimaryLocation(Boolean isPrimaryLocation) {
    this.isPrimaryLocation = isPrimaryLocation;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getOfficeNumber() {
    return officeNumber;
  }

  public void setOfficeNumber(Integer officeNumber) {
    this.officeNumber = officeNumber;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public List<Appointment> getPatientAppointments() {
    return patientAppointments;
  }

  public void setPatientAppointments(List<Appointment> patientAppointments) {
    this.patientAppointments = patientAppointments;
  }

  public HealthProviderDto getHealthProviderDto() {
    return healthProviderDto;
  }

  public void setHealthProviderDto(HealthProviderDto healthProviderDto) {
    this.healthProviderDto = healthProviderDto;
  }


}
