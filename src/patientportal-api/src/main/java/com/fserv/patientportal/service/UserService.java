package com.fserv.patientportal.service;

import com.fserv.patientportal.dto.UserDto;

public interface UserService extends BaseService<Integer, UserDto> {

}
