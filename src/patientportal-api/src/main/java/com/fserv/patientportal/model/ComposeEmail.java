package com.fserv.patientportal.model;
import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the "composeEmail" database table.
 * 
 */
@Entity
@Table(name="\"compose_email\"")
@NamedQuery(name="ComposeEmail.findAll", query="SELECT c FROM ComposeEmail c")
public class ComposeEmail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@SequenceGenerator(name = "ComposeEmail_id", sequenceName = "compose_email_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ComposeEmail_id")
	private Integer id;
    @Column(name ="body")
	private String body;
    @Column(name ="subject")
	private String subject;
@Column(name ="to_email")
	private String toEmail;

	public ComposeEmail() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	

	

	
}

