package com.fserv.patientportal.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fserv.patientportal.dto.AppointmentSearchDto;
import com.fserv.patientportal.model.Appointment;

@Repository
@Transactional
public class AppointmentSearchRepoImpl implements AppointmentSearchRepo {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Appointment> searchCriteria(AppointmentSearchDto dto) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(stringBuilderListQuery()).append(stringBuilderWhereClause(dto));
		Query q = entityManager.createQuery(stringBuilder.toString());
		bindParameters(q, dto);
		List<Appointment> q1 = q.getResultList();
		return q1;
	}

	public String stringBuilderListQuery() {
		StringBuilder select = new StringBuilder();
		select.append("select a from Appointment a LEFT JOIN FETCH a.patient LEFT JOIN FETCH a.physician LEFT JOIN FETCH a.healthProvider ");
		return select.toString();
	}

	public String stringBuilderWhereClause(AppointmentSearchDto dto) {
		StringBuilder where = new StringBuilder();
		where.append(" where 1=1");
		
		if (!StringUtils.isEmpty(dto.getAppointmentDate())) {
			where.append(" and (a.appointmentDate like (:appointmentDate))");
		}

		if (!StringUtils.isEmpty(dto.getAppointmentStatus())) {
			where.append(" and (a.appointmentStatus like(:appointmentStatus))");
		}
		if (!StringUtils.isEmpty(dto.getAppointmentType())) {
			where.append(" and (a.appointmentType like(:appointmentType))");
		}
		if(!StringUtils.isEmpty(dto.getHealthProviderId())){
			where.append(" and (a.healthProviderId =:healthProviderId)");
		}		
		if(!StringUtils.isEmpty(dto.getPatientId())){
			where.append(" and (a.patientId =:patientId)");
		}
		if(!StringUtils.isEmpty(dto.getPhysicianId())){
			where.append(" and (a.physicianId =:physicianId)");
		}
		return where.toString();
	}

	private Query bindParameters(Query query, AppointmentSearchDto dto) {
		if (!StringUtils.isEmpty(dto.getAppointmentDate())) {
			query.setParameter("appointmentDate", dto.getAppointmentDate());
		}
		if (!StringUtils.isEmpty(dto.getAppointmentStatus())) {
			query.setParameter("appointmentStatus", dto.getAppointmentStatus());
		}
		if (!StringUtils.isEmpty(dto.getAppointmentType())) {
			query.setParameter("appointmentType", dto.getAppointmentType());
		}
		if (!StringUtils.isEmpty(dto.getHealthProviderId())) {
			query.setParameter("healthProviderId",dto.getHealthProviderId());
		}
		if (!StringUtils.isEmpty(dto.getPatientId())) {
			query.setParameter("patientId",dto.getPatientId());
		}
		if (!StringUtils.isEmpty(dto.getPhysicianId())) {
			query.setParameter("physicianId",dto.getPhysicianId());
		}
		return query;

	}

}
