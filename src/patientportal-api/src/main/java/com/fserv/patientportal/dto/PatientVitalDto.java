package com.fserv.patientportal.dto;

import java.util.Date;

import com.fserv.patientportal.model.Appointment;

public class PatientVitalDto {
  private int id;
  private Date vitalDate;
  private String vitalName;
  private String vitalValue;
  private AppointmentDto appointmentDto;
  private PatientDto patientDto;
  private String source;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Date getVitalDate() {
    return vitalDate;
  }

  public void setVitalDate(Date vitalDate) {
    this.vitalDate = vitalDate;
  }

  public String getVitalName() {
    return vitalName;
  }

  public void setVitalName(String vitalName) {
    this.vitalName = vitalName;
  }

  public String getVitalValue() {
    return vitalValue;
  }

  public void setVitalValue(String vitalValue) {
    this.vitalValue = vitalValue;
  }


public AppointmentDto getAppointmentDto() {
	return appointmentDto;
}

public void setAppointmentDto(AppointmentDto appointmentDto) {
	this.appointmentDto = appointmentDto;
}

public PatientDto getPatientDto() {
	return patientDto;
}

public void setPatientDto(PatientDto patientDto) {
	this.patientDto = patientDto;
}


}
