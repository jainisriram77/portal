package com.fserv.patientportal.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the appointment database table.
 * 
 */
@Entity
@Table(name="appointment", schema = "patientportal")
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Temporal(TemporalType.DATE)
	@Column(name="appointment_date")
	private Date appointmentDate;

	@Column(name="appointment_reason")
	private String appointmentReason;

	@Column(name="appointment_status")
	@Enumerated(EnumType.STRING)
	private AppointmentStatus appointmentStatus;

	@Column(name="appointment_time")
	private Time appointmentTime;

	@Column(name="appointment_type")
	@Enumerated(EnumType.STRING)
	private AppointmentType appointmentType;

	@Column(name="health_provider_id")
	private Integer healthProviderId;

	@Column(name="patient_id")
	private Integer patientId;

	@Column(name="physician_id")
	private Integer physicianId;

	//bi-directional many-to-one association to HealthProviderLocation
	@ManyToOne
	@JoinColumn(name = "health_provider_id",insertable = false, updatable = false)
	private HealthProvider healthProvider;

	//bi-directional many-to-one association to Patient
	@ManyToOne
	@JoinColumn(name = "patient_id", insertable = false, updatable = false)
	private Patient patient;

	//bi-directional many-to-one association to Physician
	@ManyToOne
	@JoinColumn(name = "physician_id", insertable = false, updatable = false)
	private Physician physician;

	//bi-directional many-to-one association to PatientReport
	@OneToMany(mappedBy="appointment")
	private List<PatientReport> patientReports;

	//bi-directional many-to-one association to PatientResult
	@OneToMany(mappedBy="appointment")
	private List<PatientResult> patientResults;

	//bi-directional many-to-one association to PatientVital
	@OneToMany(mappedBy="appointment")
	private List<PatientVital> patientVitals;

	public Appointment() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getAppointmentDate() {
		return this.appointmentDate;
	}

	public void setAppointmentDate(Date appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getAppointmentReason() {
		return this.appointmentReason;
	}

	public void setAppointmentReason(String appointmentReason) {
		this.appointmentReason = appointmentReason;
	}

	public AppointmentStatus getAppointmentStatus() {
    return appointmentStatus;
  }

  public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
    this.appointmentStatus = appointmentStatus;
  }

  public AppointmentType getAppointmentType() {
    return appointmentType;
  }

  public void setAppointmentType(AppointmentType appointmentType) {
    this.appointmentType = appointmentType;
  }

  public Time getAppointmentTime() {
		return this.appointmentTime;
	}

	public void setAppointmentTime(Time appointmentTime) {
		this.appointmentTime = appointmentTime;
	}

	public Integer getHealthProviderId() {
		return healthProviderId;
	}

	public void setHealthProviderId(Integer healthProviderId) {
		this.healthProviderId = healthProviderId;
	}

	public Integer getPatientId() {
		return this.patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Integer getPhysicianId() {
		return this.physicianId;
	}

	public void setPhysicianId(Integer physicianId) {
		this.physicianId = physicianId;
	}

	public HealthProvider getHealthProvider() {
		return this.healthProvider;
	}

	public void setHealthProvider(HealthProvider healthProvider) {
		this.healthProvider = healthProvider;
	}

	public Patient getPatient() {
		return this.patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Physician getPhysician() {
		return this.physician;
	}

	public void setPhysician(Physician physician) {
		this.physician = physician;
	}

	public List<PatientReport> getPatientReports() {
		return this.patientReports;
	}

	public void setPatientReports(List<PatientReport> patientReports) {
		this.patientReports = patientReports;
	}

	public PatientReport addPatientReport(PatientReport patientReport) {
		getPatientReports().add(patientReport);
		patientReport.setAppointment(this);

		return patientReport;
	}

	public PatientReport removePatientReport(PatientReport patientReport) {
		getPatientReports().remove(patientReport);
		patientReport.setAppointment(null);

		return patientReport;
	}

	public List<PatientResult> getPatientResults() {
		return this.patientResults;
	}

	public void setPatientResults(List<PatientResult> patientResults) {
		this.patientResults = patientResults;
	}

	public PatientResult addPatientResult(PatientResult patientResult) {
		getPatientResults().add(patientResult);
		patientResult.setAppointment(this);

		return patientResult;
	}

	public PatientResult removePatientResult(PatientResult patientResult) {
		getPatientResults().remove(patientResult);
		patientResult.setAppointment(null);

		return patientResult;
	}

	public List<PatientVital> getPatientVitals() {
		return this.patientVitals;
	}

	public void setPatientVitals(List<PatientVital> patientVitals) {
		this.patientVitals = patientVitals;
	}

	public PatientVital addPatientVital(PatientVital patientVital) {
		getPatientVitals().add(patientVital);
		patientVital.setAppointment(this);

		return patientVital;
	}

	public PatientVital removePatientVital(PatientVital patientVital) {
		getPatientVitals().remove(patientVital);
		patientVital.setAppointment(null);

		return patientVital;
	}

}