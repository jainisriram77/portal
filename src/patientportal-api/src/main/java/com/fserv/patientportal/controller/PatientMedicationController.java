package com.fserv.patientportal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fserv.patientportal.dto.PatientMedicationDto;
import com.fserv.patientportal.model.MedicationType;
import com.fserv.patientportal.service.PatientMedicationService;

@Controller
@RequestMapping(value = "/patientmedications")
public class PatientMedicationController extends BaseControllerImpl<Integer, PatientMedicationDto> {

  @Autowired
  PatientMedicationService patientMedicationService;

  @Autowired(required = true)
  public PatientMedicationController(PatientMedicationService baseService) {
    super(baseService);
    // TODO Auto-generated constructor stub
  }

  @RequestMapping(value = "/{patientId}/{medicationType}", method = RequestMethod.GET,
      produces = "application/json")
  public @ResponseBody List<PatientMedicationDto> findPtMedicationsByPtIdAndType(
      @PathVariable("patientId") int patientId,
      @PathVariable("medicationType") MedicationType medicationType) {
    List<PatientMedicationDto> ptMedicationsDto = patientMedicationService
        .findPtMedicationsByPtIdAndMedicationType(patientId, medicationType);
    return ptMedicationsDto;
  }

}
