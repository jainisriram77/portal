

DROP TABLE public."user";
DROP TABLE public.patient;
DROP TABLE public.patient_medication;
DROP TABLE public.patient_vital;
DROP TABLE public.patient_document;
DROP TABLE public.document;
DROP TABLE public.patient_condition;
DROP TABLE public.activity;
DROP TABLE public.patient_activity;
DROP TABLE public.patient_preference;
DROP TABLE public.physician;
DROP TABLE public.patient_appointment;
DROP TABLE public.health_provider;




CREATE TABLE public."user"
(
  user_id integer,
  lastname character varying,
  firstname character varying,
  password character varying,
  email character varying,
  dob date
)
WITH (
  OIDS=FALSE
);



CREATE TABLE public.patient
(
  patient_id integer NOT NULL DEFAULT nextval('patient_pid_seq'::regclass),
  first_name character varying,
  last_name character varying,
  dob date,
  email character varying,
  ssn integer,
  phone integer,
  mrn character varying,
  address1 character varying,
  gender character varying,
  age integer,
  user_id character varying,
  CONSTRAINT pk_pid PRIMARY KEY (patient_id),
  CONSTRAINT uni_ssn UNIQUE (ssn)
)
WITH (
  OIDS=FALSE
);




CREATE TABLE public.physician
(
  first_name character varying,
  last_name character varying,
  email character varying,
  specialization character varying,
  physician_id integer NOT NULL DEFAULT nextval('physician_phy_id_seq'::regclass),
  physician_npid integer NOT NULL,
  age integer,
  gender character varying,
  home_phone_number integer,
  cell_phone_number integer,
  work_phone_number integer,
  CONSTRAINT pk_phy_id PRIMARY KEY (physician_id)
)
WITH (
  OIDS=FALSE
);




CREATE TABLE public.patient_preference
(
  patient_preference_id integer NOT NULL,
  patient_id integer,
  preference_name character varying,
  preference_value character varying,
  CONSTRAINT "PK_patient_preference" PRIMARY KEY (patient_preference_id),
  CONSTRAINT "FK_patient_preference" FOREIGN KEY (patient_id)
      REFERENCES public.patient (patient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


CREATE TABLE public.patient_vital
(
  patient_vital_id integer NOT NULL,
  patient_id integer,
  height character varying,
  weight character varying,
  blood_pressure character varying,
  vital_date date,
  CONSTRAINT "PK_patient_vital" PRIMARY KEY (patient_vital_id),
  CONSTRAINT "FK_patient_vital" FOREIGN KEY (patient_id)
      REFERENCES public.patient (patient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);



CREATE TABLE public.patient_medication
(
  patient_medication_id integer NOT NULL,
  patient_id integer,
  prescribed_date date,
  medication_name character varying,
  medication_description character varying,
  prescribed_physician character varying,
  medication_start_date date,
  medication_end_date date,
  medication_type character varying,
  CONSTRAINT "PK_patient_medication" PRIMARY KEY (patient_medication_id),
  CONSTRAINT "FK_patient_medication" FOREIGN KEY (patient_id)
      REFERENCES public.patient (patient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);



CREATE TABLE public.patient_document
(
  patient_id integer,
  document_id integer
)
WITH (
  OIDS=FALSE
);




CREATE TABLE public.document
(
  document_id integer NOT NULL,
  document_name character varying,
  document_type character varying,
  document_description character varying,
  document_url character varying,
  creation_date date,
  modified_date date,
  CONSTRAINT "PK_document" PRIMARY KEY (document_id)
)
WITH (
  OIDS=FALSE
);




CREATE TABLE public.patient_condition
(
  patient_condition_id integer NOT NULL,
  patient_id integer,
  condition_date date,
  is_pre_condition character varying,
  condition_type character varying,
  condition_name character varying,
  condition_description character varying,
  CONSTRAINT "PK_patient_condition" PRIMARY KEY (patient_condition_id),
  CONSTRAINT "FK_patient_condition" FOREIGN KEY (patient_id)
      REFERENCES public.patient (patient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);



CREATE TABLE public.patient_activity
(
  patient_id integer NOT NULL,
  activity_id integer,
  CONSTRAINT "PK_patient_activity" PRIMARY KEY (patient_id),
  CONSTRAINT "FK_patient_activity" FOREIGN KEY (patient_id)
      REFERENCES public.patient (patient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);






CREATE TABLE public.activity
(
  activity_id integer NOT NULL,
  activity_type character varying,
  activity_date date,
  subject character varying,
  message character varying,
  "from" character varying,
  CONSTRAINT "PK_activity" PRIMARY KEY (activity_id)
)
WITH (
  OIDS=FALSE
);




CREATE TABLE public.patient_appointment
(
  patient_appointment_id integer NOT NULL,
  patient_id integer,
  physician_id integer,
  appointment_date date,
  appointment_time time without time zone,
  appointment_reason character varying,
  appointment_type character varying,
  provider_location_id integer,
  appointment_status character varying,
  CONSTRAINT "PK_patient_appointment" PRIMARY KEY (patient_appointment_id),
  CONSTRAINT "FK1_patient_appointment" FOREIGN KEY (physician_id)
      REFERENCES public.physician (physician_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FK_patient_appointment" FOREIGN KEY (patient_id)
      REFERENCES public.patient (patient_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);







CREATE TABLE public.health_provider
(
  health_provider_id integer NOT NULL,
  health_provider_name character varying,
  CONSTRAINT "PK_health_provider" PRIMARY KEY (health_provider_id)
)
WITH (
  OIDS=FALSE
);
